## [0.0.1] - TODO: Add release date.

* TODO: Describe initial release.



/*
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      final accessToken = await _getAccessToken();
      print("=========================>$accessToken");

      if (accessToken != null) {
        final tokenMap = JwtDecoder.decode(accessToken);
        _log.info("TokenMap: $tokenMap");
        DateTime expirationDate = JwtDecoder.getExpirationDate(accessToken);
        _log.info("ExpirationDate: $expirationDate");
        final refreshToken = await _get(kTicatagRefreshToken);
        if (refreshToken != null) {
          DateTime expirationRefreshDate =
              JwtDecoder.getExpirationDate(refreshToken);
          _log.info("RefreshTokeDate: $expirationRefreshDate");
        } else {
          _log.info("No refresh token found");
        }

        options.headers['Authorization'] = 'Bearer $accessToken';
      }
      return options;
    }, onResponse: (response, handler) {
      return response;
    }, onError: (error, handler) async {
      _log.severe('${error.requestOptions.uri} $error ${error.response}');
      if (error.response?.statusCode == 401) {
        final String errorDescription =
            error.response.data['error_description'];
        if (errorDescription != null &&
            errorDescription.contains('activated')) {
          throw TicatagApiAccountNotActivatedException();
        }
        final refreshToken = await _getRefreshToken();
        if (refreshToken == null) {
          _log.severe('No refresh token found.');
          _storage.delete(key: kTicatagAccessToken);
          _authenticationErrorStreamController
              .add(Exception('No refresh token found'));
        } else {
          RequestOptions options = error.response.requestOptions;
          _dio.interceptors.requestLock.lock();
          _dio.interceptors.responseLock.lock();
          return _tokenDio
              .post(
                  '/token?client_id=$clientId&client_secret=$clientSecret&refresh_token=$refreshToken&grant_type=refresh_token')
              .then((d) async {
            final accessToken = d.data['access_token'];
            final refreshToken = d.data['refresh_token'];
            _log.info(
                "New access and refresh token from refresh step: $accessToken, $refreshToken");
            await _setAccessToken(accessToken);
            await _setRefreshToken(refreshToken);
            options.headers['Authorization'] = 'Bearer $accessToken';
          }).catchError((e) {
            _log.severe(
                "Error getting new accessToken from refreshToken. ${error.response.data['error_description']}");
            _storage.delete(key: kTicatagAccessToken);
            _storage.delete(key: kTicatagRefreshToken);
            _authenticationErrorStreamController
                .add(Exception('${error.response.data['error_description']}'));
          }).whenComplete(() {
            _dio.interceptors.requestLock.unlock();
            _dio.interceptors.responseLock.unlock();
          }).then((e) {
            //return _dio.request(options.path, options: options);
            handler.next(err)
          });
        }
      }
      return error; //continue
    }));
     */
