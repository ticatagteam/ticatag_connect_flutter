import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ticatag_connect/ticatag_connect.dart';

void main() {
  test('Notification toJson', () {
    final notification = Notification((b) => b
      ..isActive = true
      ..deviceId = 'device1'
      ..emails =
          [Email((b) => b..email = 'fvisticot@gmail.com')].build().toBuilder());

    print(notification.toJson());
    expect(notification.toJson(), isNotNull);
  });

  test('Notification fromMap', () {
    final map = {
      "to": [],
      "device_id": "device1",
      "active": true,
      "emails": [
        {"email": "fvisticot@gmail.com"}
      ]
    };
    final notification = Notification.fromMap(map);
    expect(notification, isNotNull);
  });

  test('Notification fromMap error active', () {
    final map = {
      "to": [],
      "device_id": "device1",
      "active": "true",
      "emails": [
        {"email": "fvisticot@gmail.com"}
      ]
    };

    expect(() => Notification.fromMap(map),
        throwsA(TypeMatcher<DeserializationError>()));
  });

  test('Notification fromMap error active', () {
    expect(() => Notification.fromJson("test"),
        throwsA(TypeMatcher<FormatException>()));
  });
}
