// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beacon_event.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ButtonPressedType _$simpleClick =
    const ButtonPressedType._('simpleClick');
const ButtonPressedType _$doubleClick =
    const ButtonPressedType._('doubleClick');

ButtonPressedType _$buttonPressedTypeValueOf(String name) {
  switch (name) {
    case 'simpleClick':
      return _$simpleClick;
    case 'doubleClick':
      return _$doubleClick;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ButtonPressedType> _$buttonPressedTypeValues =
    new BuiltSet<ButtonPressedType>(const <ButtonPressedType>[
  _$simpleClick,
  _$doubleClick,
]);

Serializer<BeaconLocationEvent> _$beaconLocationEventSerializer =
    new _$BeaconLocationEventSerializer();
Serializer<BeaconBatteryEvent> _$beaconBatteryEventSerializer =
    new _$BeaconBatteryEventSerializer();
Serializer<BeaconButtonEvent> _$beaconButtonEventSerializer =
    new _$BeaconButtonEventSerializer();
Serializer<BeaconHumidityEvent> _$beaconHumidityEventSerializer =
    new _$BeaconHumidityEventSerializer();
Serializer<BeaconTemperatureEvent> _$beaconTemperatureEventSerializer =
    new _$BeaconTemperatureEventSerializer();
Serializer<BeaconWaterLeakEvent> _$beaconWaterLeakEventSerializer =
    new _$BeaconWaterLeakEventSerializer();
Serializer<BeaconRolloverEvent> _$beaconRolloverEventSerializer =
    new _$BeaconRolloverEventSerializer();
Serializer<BeaconShockEvent> _$beaconShockEventSerializer =
    new _$BeaconShockEventSerializer();
Serializer<BeaconMotionEvent> _$beaconMotionEventSerializer =
    new _$BeaconMotionEventSerializer();
Serializer<BeaconDoorEvent> _$beaconDoorEventSerializer =
    new _$BeaconDoorEventSerializer();
Serializer<BeaconCo2Event> _$beaconCo2EventSerializer =
    new _$BeaconCo2EventSerializer();
Serializer<BeaconTotalOdometerEvent> _$beaconTotalOdometerEventSerializer =
    new _$BeaconTotalOdometerEventSerializer();
Serializer<ButtonPressedType> _$buttonPressedTypeSerializer =
    new _$ButtonPressedTypeSerializer();

class _$BeaconLocationEventSerializer
    implements StructuredSerializer<BeaconLocationEvent> {
  @override
  final Iterable<Type> types = const [
    BeaconLocationEvent,
    _$BeaconLocationEvent
  ];
  @override
  final String wireName = 'BeaconLocationEvent';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, BeaconLocationEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'latitude',
      serializers.serialize(object.latitude,
          specifiedType: const FullType(double)),
      'longitude',
      serializers.serialize(object.longitude,
          specifiedType: const FullType(double)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconLocationEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconLocationEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconBatteryEventSerializer
    implements StructuredSerializer<BeaconBatteryEvent> {
  @override
  final Iterable<Type> types = const [BeaconBatteryEvent, _$BeaconBatteryEvent];
  @override
  final String wireName = 'BeaconBatteryEvent';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, BeaconBatteryEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'value',
      serializers.serialize(object.value, specifiedType: const FullType(int)),
      'unit',
      serializers.serialize(object.unit,
          specifiedType: const FullType(BatteryUnit)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconBatteryEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconBatteryEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'unit':
          result.unit = serializers.deserialize(value,
              specifiedType: const FullType(BatteryUnit))! as BatteryUnit;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconButtonEventSerializer
    implements StructuredSerializer<BeaconButtonEvent> {
  @override
  final Iterable<Type> types = const [BeaconButtonEvent, _$BeaconButtonEvent];
  @override
  final String wireName = 'BeaconButtonEvent';

  @override
  Iterable<Object?> serialize(Serializers serializers, BeaconButtonEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'buttonPressedType',
      serializers.serialize(object.buttonPressedType,
          specifiedType: const FullType(ButtonPressedType)),
      'latitude',
      serializers.serialize(object.latitude,
          specifiedType: const FullType(double)),
      'longitude',
      serializers.serialize(object.longitude,
          specifiedType: const FullType(double)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconButtonEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconButtonEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'buttonPressedType':
          result.buttonPressedType = serializers.deserialize(value,
                  specifiedType: const FullType(ButtonPressedType))!
              as ButtonPressedType;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconHumidityEventSerializer
    implements StructuredSerializer<BeaconHumidityEvent> {
  @override
  final Iterable<Type> types = const [
    BeaconHumidityEvent,
    _$BeaconHumidityEvent
  ];
  @override
  final String wireName = 'BeaconHumidityEvent';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, BeaconHumidityEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'value',
      serializers.serialize(object.value,
          specifiedType: const FullType(double)),
      'unit',
      serializers.serialize(object.unit, specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconHumidityEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconHumidityEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'unit':
          result.unit = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconTemperatureEventSerializer
    implements StructuredSerializer<BeaconTemperatureEvent> {
  @override
  final Iterable<Type> types = const [
    BeaconTemperatureEvent,
    _$BeaconTemperatureEvent
  ];
  @override
  final String wireName = 'BeaconTemperatureEvent';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, BeaconTemperatureEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'value',
      serializers.serialize(object.value,
          specifiedType: const FullType(double)),
      'unit',
      serializers.serialize(object.unit, specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconTemperatureEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconTemperatureEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'unit':
          result.unit = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconWaterLeakEventSerializer
    implements StructuredSerializer<BeaconWaterLeakEvent> {
  @override
  final Iterable<Type> types = const [
    BeaconWaterLeakEvent,
    _$BeaconWaterLeakEvent
  ];
  @override
  final String wireName = 'BeaconWaterLeakEvent';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, BeaconWaterLeakEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'status',
      serializers.serialize(object.status,
          specifiedType: const FullType(WaterLeakStatus)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconWaterLeakEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconWaterLeakEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(WaterLeakStatus))!
              as WaterLeakStatus;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconRolloverEventSerializer
    implements StructuredSerializer<BeaconRolloverEvent> {
  @override
  final Iterable<Type> types = const [
    BeaconRolloverEvent,
    _$BeaconRolloverEvent
  ];
  @override
  final String wireName = 'BeaconRolloverEvent';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, BeaconRolloverEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'status',
      serializers.serialize(object.status,
          specifiedType: const FullType(RolloverStatus)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconRolloverEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconRolloverEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(RolloverStatus))! as RolloverStatus;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconShockEventSerializer
    implements StructuredSerializer<BeaconShockEvent> {
  @override
  final Iterable<Type> types = const [BeaconShockEvent, _$BeaconShockEvent];
  @override
  final String wireName = 'BeaconShockEvent';

  @override
  Iterable<Object?> serialize(Serializers serializers, BeaconShockEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'status',
      serializers.serialize(object.status,
          specifiedType: const FullType(ShockStatus)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconShockEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconShockEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(ShockStatus))! as ShockStatus;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconMotionEventSerializer
    implements StructuredSerializer<BeaconMotionEvent> {
  @override
  final Iterable<Type> types = const [BeaconMotionEvent, _$BeaconMotionEvent];
  @override
  final String wireName = 'BeaconMotionEvent';

  @override
  Iterable<Object?> serialize(Serializers serializers, BeaconMotionEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'status',
      serializers.serialize(object.status,
          specifiedType: const FullType(MotionStatus)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconMotionEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconMotionEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(MotionStatus))! as MotionStatus;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconDoorEventSerializer
    implements StructuredSerializer<BeaconDoorEvent> {
  @override
  final Iterable<Type> types = const [BeaconDoorEvent, _$BeaconDoorEvent];
  @override
  final String wireName = 'BeaconDoorEvent';

  @override
  Iterable<Object?> serialize(Serializers serializers, BeaconDoorEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'status',
      serializers.serialize(object.status,
          specifiedType: const FullType(DoorStatus)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconDoorEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconDoorEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(DoorStatus))! as DoorStatus;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconCo2EventSerializer
    implements StructuredSerializer<BeaconCo2Event> {
  @override
  final Iterable<Type> types = const [BeaconCo2Event, _$BeaconCo2Event];
  @override
  final String wireName = 'BeaconCo2Event';

  @override
  Iterable<Object?> serialize(Serializers serializers, BeaconCo2Event object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'value',
      serializers.serialize(object.value, specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconCo2Event deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconCo2EventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconTotalOdometerEventSerializer
    implements StructuredSerializer<BeaconTotalOdometerEvent> {
  @override
  final Iterable<Type> types = const [
    BeaconTotalOdometerEvent,
    _$BeaconTotalOdometerEvent
  ];
  @override
  final String wireName = 'BeaconTotalOdometerEvent';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, BeaconTotalOdometerEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'value',
      serializers.serialize(object.value, specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.macAddress;
    if (value != null) {
      result
        ..add('macAddress')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BeaconTotalOdometerEvent deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconTotalOdometerEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'macAddress':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ButtonPressedTypeSerializer
    implements PrimitiveSerializer<ButtonPressedType> {
  @override
  final Iterable<Type> types = const <Type>[ButtonPressedType];
  @override
  final String wireName = 'ButtonPressedType';

  @override
  Object serialize(Serializers serializers, ButtonPressedType object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  ButtonPressedType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ButtonPressedType.valueOf(serialized as String);
}

abstract mixin class BeaconEventBuilder {
  void replace(BeaconEvent other);
  void update(void Function(BeaconEventBuilder) updates);
  DateTime? get timestamp;
  set timestamp(DateTime? timestamp);

  String? get macAddress;
  set macAddress(String? macAddress);
}

class _$BeaconLocationEvent extends BeaconLocationEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final double latitude;
  @override
  final double longitude;

  factory _$BeaconLocationEvent(
          [void Function(BeaconLocationEventBuilder)? updates]) =>
      (new BeaconLocationEventBuilder()..update(updates))._build();

  _$BeaconLocationEvent._(
      {required this.timestamp,
      this.macAddress,
      required this.latitude,
      required this.longitude})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconLocationEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        latitude, r'BeaconLocationEvent', 'latitude');
    BuiltValueNullFieldError.checkNotNull(
        longitude, r'BeaconLocationEvent', 'longitude');
  }

  @override
  BeaconLocationEvent rebuild(
          void Function(BeaconLocationEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconLocationEventBuilder toBuilder() =>
      new BeaconLocationEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconLocationEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        latitude == other.latitude &&
        longitude == other.longitude;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, latitude.hashCode);
    _$hash = $jc(_$hash, longitude.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconLocationEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('latitude', latitude)
          ..add('longitude', longitude))
        .toString();
  }
}

class BeaconLocationEventBuilder
    implements
        Builder<BeaconLocationEvent, BeaconLocationEventBuilder>,
        BeaconEventBuilder {
  _$BeaconLocationEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(covariant double? latitude) => _$this._latitude = latitude;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(covariant double? longitude) => _$this._longitude = longitude;

  BeaconLocationEventBuilder();

  BeaconLocationEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconLocationEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconLocationEvent;
  }

  @override
  void update(void Function(BeaconLocationEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconLocationEvent build() => _build();

  _$BeaconLocationEvent _build() {
    final _$result = _$v ??
        new _$BeaconLocationEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconLocationEvent', 'timestamp'),
            macAddress: macAddress,
            latitude: BuiltValueNullFieldError.checkNotNull(
                latitude, r'BeaconLocationEvent', 'latitude'),
            longitude: BuiltValueNullFieldError.checkNotNull(
                longitude, r'BeaconLocationEvent', 'longitude'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconBatteryEvent extends BeaconBatteryEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final int value;
  @override
  final BatteryUnit unit;

  factory _$BeaconBatteryEvent(
          [void Function(BeaconBatteryEventBuilder)? updates]) =>
      (new BeaconBatteryEventBuilder()..update(updates))._build();

  _$BeaconBatteryEvent._(
      {required this.timestamp,
      this.macAddress,
      required this.value,
      required this.unit})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconBatteryEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        value, r'BeaconBatteryEvent', 'value');
    BuiltValueNullFieldError.checkNotNull(unit, r'BeaconBatteryEvent', 'unit');
  }

  @override
  BeaconBatteryEvent rebuild(
          void Function(BeaconBatteryEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconBatteryEventBuilder toBuilder() =>
      new BeaconBatteryEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconBatteryEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        value == other.value &&
        unit == other.unit;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jc(_$hash, unit.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconBatteryEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('value', value)
          ..add('unit', unit))
        .toString();
  }
}

class BeaconBatteryEventBuilder
    implements
        Builder<BeaconBatteryEvent, BeaconBatteryEventBuilder>,
        BeaconEventBuilder {
  _$BeaconBatteryEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  int? _value;
  int? get value => _$this._value;
  set value(covariant int? value) => _$this._value = value;

  BatteryUnit? _unit;
  BatteryUnit? get unit => _$this._unit;
  set unit(covariant BatteryUnit? unit) => _$this._unit = unit;

  BeaconBatteryEventBuilder();

  BeaconBatteryEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _value = $v.value;
      _unit = $v.unit;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconBatteryEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconBatteryEvent;
  }

  @override
  void update(void Function(BeaconBatteryEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconBatteryEvent build() => _build();

  _$BeaconBatteryEvent _build() {
    final _$result = _$v ??
        new _$BeaconBatteryEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconBatteryEvent', 'timestamp'),
            macAddress: macAddress,
            value: BuiltValueNullFieldError.checkNotNull(
                value, r'BeaconBatteryEvent', 'value'),
            unit: BuiltValueNullFieldError.checkNotNull(
                unit, r'BeaconBatteryEvent', 'unit'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconButtonEvent extends BeaconButtonEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final ButtonPressedType buttonPressedType;
  @override
  final double latitude;
  @override
  final double longitude;

  factory _$BeaconButtonEvent(
          [void Function(BeaconButtonEventBuilder)? updates]) =>
      (new BeaconButtonEventBuilder()..update(updates))._build();

  _$BeaconButtonEvent._(
      {required this.timestamp,
      this.macAddress,
      required this.buttonPressedType,
      required this.latitude,
      required this.longitude})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconButtonEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        buttonPressedType, r'BeaconButtonEvent', 'buttonPressedType');
    BuiltValueNullFieldError.checkNotNull(
        latitude, r'BeaconButtonEvent', 'latitude');
    BuiltValueNullFieldError.checkNotNull(
        longitude, r'BeaconButtonEvent', 'longitude');
  }

  @override
  BeaconButtonEvent rebuild(void Function(BeaconButtonEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconButtonEventBuilder toBuilder() =>
      new BeaconButtonEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconButtonEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        buttonPressedType == other.buttonPressedType &&
        latitude == other.latitude &&
        longitude == other.longitude;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, buttonPressedType.hashCode);
    _$hash = $jc(_$hash, latitude.hashCode);
    _$hash = $jc(_$hash, longitude.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconButtonEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('buttonPressedType', buttonPressedType)
          ..add('latitude', latitude)
          ..add('longitude', longitude))
        .toString();
  }
}

class BeaconButtonEventBuilder
    implements
        Builder<BeaconButtonEvent, BeaconButtonEventBuilder>,
        BeaconEventBuilder {
  _$BeaconButtonEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  ButtonPressedType? _buttonPressedType;
  ButtonPressedType? get buttonPressedType => _$this._buttonPressedType;
  set buttonPressedType(covariant ButtonPressedType? buttonPressedType) =>
      _$this._buttonPressedType = buttonPressedType;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(covariant double? latitude) => _$this._latitude = latitude;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(covariant double? longitude) => _$this._longitude = longitude;

  BeaconButtonEventBuilder();

  BeaconButtonEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _buttonPressedType = $v.buttonPressedType;
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconButtonEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconButtonEvent;
  }

  @override
  void update(void Function(BeaconButtonEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconButtonEvent build() => _build();

  _$BeaconButtonEvent _build() {
    final _$result = _$v ??
        new _$BeaconButtonEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconButtonEvent', 'timestamp'),
            macAddress: macAddress,
            buttonPressedType: BuiltValueNullFieldError.checkNotNull(
                buttonPressedType, r'BeaconButtonEvent', 'buttonPressedType'),
            latitude: BuiltValueNullFieldError.checkNotNull(
                latitude, r'BeaconButtonEvent', 'latitude'),
            longitude: BuiltValueNullFieldError.checkNotNull(
                longitude, r'BeaconButtonEvent', 'longitude'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconHumidityEvent extends BeaconHumidityEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final double value;
  @override
  final String unit;

  factory _$BeaconHumidityEvent(
          [void Function(BeaconHumidityEventBuilder)? updates]) =>
      (new BeaconHumidityEventBuilder()..update(updates))._build();

  _$BeaconHumidityEvent._(
      {required this.timestamp,
      this.macAddress,
      required this.value,
      required this.unit})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconHumidityEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        value, r'BeaconHumidityEvent', 'value');
    BuiltValueNullFieldError.checkNotNull(unit, r'BeaconHumidityEvent', 'unit');
  }

  @override
  BeaconHumidityEvent rebuild(
          void Function(BeaconHumidityEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconHumidityEventBuilder toBuilder() =>
      new BeaconHumidityEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconHumidityEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        value == other.value &&
        unit == other.unit;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jc(_$hash, unit.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconHumidityEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('value', value)
          ..add('unit', unit))
        .toString();
  }
}

class BeaconHumidityEventBuilder
    implements
        Builder<BeaconHumidityEvent, BeaconHumidityEventBuilder>,
        BeaconEventBuilder {
  _$BeaconHumidityEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  double? _value;
  double? get value => _$this._value;
  set value(covariant double? value) => _$this._value = value;

  String? _unit;
  String? get unit => _$this._unit;
  set unit(covariant String? unit) => _$this._unit = unit;

  BeaconHumidityEventBuilder();

  BeaconHumidityEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _value = $v.value;
      _unit = $v.unit;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconHumidityEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconHumidityEvent;
  }

  @override
  void update(void Function(BeaconHumidityEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconHumidityEvent build() => _build();

  _$BeaconHumidityEvent _build() {
    final _$result = _$v ??
        new _$BeaconHumidityEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconHumidityEvent', 'timestamp'),
            macAddress: macAddress,
            value: BuiltValueNullFieldError.checkNotNull(
                value, r'BeaconHumidityEvent', 'value'),
            unit: BuiltValueNullFieldError.checkNotNull(
                unit, r'BeaconHumidityEvent', 'unit'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconTemperatureEvent extends BeaconTemperatureEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final double value;
  @override
  final String unit;

  factory _$BeaconTemperatureEvent(
          [void Function(BeaconTemperatureEventBuilder)? updates]) =>
      (new BeaconTemperatureEventBuilder()..update(updates))._build();

  _$BeaconTemperatureEvent._(
      {required this.timestamp,
      this.macAddress,
      required this.value,
      required this.unit})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconTemperatureEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        value, r'BeaconTemperatureEvent', 'value');
    BuiltValueNullFieldError.checkNotNull(
        unit, r'BeaconTemperatureEvent', 'unit');
  }

  @override
  BeaconTemperatureEvent rebuild(
          void Function(BeaconTemperatureEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconTemperatureEventBuilder toBuilder() =>
      new BeaconTemperatureEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconTemperatureEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        value == other.value &&
        unit == other.unit;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jc(_$hash, unit.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconTemperatureEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('value', value)
          ..add('unit', unit))
        .toString();
  }
}

class BeaconTemperatureEventBuilder
    implements
        Builder<BeaconTemperatureEvent, BeaconTemperatureEventBuilder>,
        BeaconEventBuilder {
  _$BeaconTemperatureEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  double? _value;
  double? get value => _$this._value;
  set value(covariant double? value) => _$this._value = value;

  String? _unit;
  String? get unit => _$this._unit;
  set unit(covariant String? unit) => _$this._unit = unit;

  BeaconTemperatureEventBuilder();

  BeaconTemperatureEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _value = $v.value;
      _unit = $v.unit;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconTemperatureEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconTemperatureEvent;
  }

  @override
  void update(void Function(BeaconTemperatureEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconTemperatureEvent build() => _build();

  _$BeaconTemperatureEvent _build() {
    final _$result = _$v ??
        new _$BeaconTemperatureEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconTemperatureEvent', 'timestamp'),
            macAddress: macAddress,
            value: BuiltValueNullFieldError.checkNotNull(
                value, r'BeaconTemperatureEvent', 'value'),
            unit: BuiltValueNullFieldError.checkNotNull(
                unit, r'BeaconTemperatureEvent', 'unit'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconWaterLeakEvent extends BeaconWaterLeakEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final WaterLeakStatus status;

  factory _$BeaconWaterLeakEvent(
          [void Function(BeaconWaterLeakEventBuilder)? updates]) =>
      (new BeaconWaterLeakEventBuilder()..update(updates))._build();

  _$BeaconWaterLeakEvent._(
      {required this.timestamp, this.macAddress, required this.status})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconWaterLeakEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        status, r'BeaconWaterLeakEvent', 'status');
  }

  @override
  BeaconWaterLeakEvent rebuild(
          void Function(BeaconWaterLeakEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconWaterLeakEventBuilder toBuilder() =>
      new BeaconWaterLeakEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconWaterLeakEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconWaterLeakEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('status', status))
        .toString();
  }
}

class BeaconWaterLeakEventBuilder
    implements
        Builder<BeaconWaterLeakEvent, BeaconWaterLeakEventBuilder>,
        BeaconEventBuilder {
  _$BeaconWaterLeakEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  WaterLeakStatus? _status;
  WaterLeakStatus? get status => _$this._status;
  set status(covariant WaterLeakStatus? status) => _$this._status = status;

  BeaconWaterLeakEventBuilder();

  BeaconWaterLeakEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconWaterLeakEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconWaterLeakEvent;
  }

  @override
  void update(void Function(BeaconWaterLeakEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconWaterLeakEvent build() => _build();

  _$BeaconWaterLeakEvent _build() {
    final _$result = _$v ??
        new _$BeaconWaterLeakEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconWaterLeakEvent', 'timestamp'),
            macAddress: macAddress,
            status: BuiltValueNullFieldError.checkNotNull(
                status, r'BeaconWaterLeakEvent', 'status'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconRolloverEvent extends BeaconRolloverEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final RolloverStatus status;

  factory _$BeaconRolloverEvent(
          [void Function(BeaconRolloverEventBuilder)? updates]) =>
      (new BeaconRolloverEventBuilder()..update(updates))._build();

  _$BeaconRolloverEvent._(
      {required this.timestamp, this.macAddress, required this.status})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconRolloverEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        status, r'BeaconRolloverEvent', 'status');
  }

  @override
  BeaconRolloverEvent rebuild(
          void Function(BeaconRolloverEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconRolloverEventBuilder toBuilder() =>
      new BeaconRolloverEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconRolloverEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconRolloverEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('status', status))
        .toString();
  }
}

class BeaconRolloverEventBuilder
    implements
        Builder<BeaconRolloverEvent, BeaconRolloverEventBuilder>,
        BeaconEventBuilder {
  _$BeaconRolloverEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  RolloverStatus? _status;
  RolloverStatus? get status => _$this._status;
  set status(covariant RolloverStatus? status) => _$this._status = status;

  BeaconRolloverEventBuilder();

  BeaconRolloverEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconRolloverEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconRolloverEvent;
  }

  @override
  void update(void Function(BeaconRolloverEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconRolloverEvent build() => _build();

  _$BeaconRolloverEvent _build() {
    final _$result = _$v ??
        new _$BeaconRolloverEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconRolloverEvent', 'timestamp'),
            macAddress: macAddress,
            status: BuiltValueNullFieldError.checkNotNull(
                status, r'BeaconRolloverEvent', 'status'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconShockEvent extends BeaconShockEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final ShockStatus status;

  factory _$BeaconShockEvent(
          [void Function(BeaconShockEventBuilder)? updates]) =>
      (new BeaconShockEventBuilder()..update(updates))._build();

  _$BeaconShockEvent._(
      {required this.timestamp, this.macAddress, required this.status})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconShockEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        status, r'BeaconShockEvent', 'status');
  }

  @override
  BeaconShockEvent rebuild(void Function(BeaconShockEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconShockEventBuilder toBuilder() =>
      new BeaconShockEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconShockEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconShockEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('status', status))
        .toString();
  }
}

class BeaconShockEventBuilder
    implements
        Builder<BeaconShockEvent, BeaconShockEventBuilder>,
        BeaconEventBuilder {
  _$BeaconShockEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  ShockStatus? _status;
  ShockStatus? get status => _$this._status;
  set status(covariant ShockStatus? status) => _$this._status = status;

  BeaconShockEventBuilder();

  BeaconShockEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconShockEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconShockEvent;
  }

  @override
  void update(void Function(BeaconShockEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconShockEvent build() => _build();

  _$BeaconShockEvent _build() {
    final _$result = _$v ??
        new _$BeaconShockEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconShockEvent', 'timestamp'),
            macAddress: macAddress,
            status: BuiltValueNullFieldError.checkNotNull(
                status, r'BeaconShockEvent', 'status'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconMotionEvent extends BeaconMotionEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final MotionStatus status;

  factory _$BeaconMotionEvent(
          [void Function(BeaconMotionEventBuilder)? updates]) =>
      (new BeaconMotionEventBuilder()..update(updates))._build();

  _$BeaconMotionEvent._(
      {required this.timestamp, this.macAddress, required this.status})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconMotionEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        status, r'BeaconMotionEvent', 'status');
  }

  @override
  BeaconMotionEvent rebuild(void Function(BeaconMotionEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconMotionEventBuilder toBuilder() =>
      new BeaconMotionEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconMotionEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconMotionEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('status', status))
        .toString();
  }
}

class BeaconMotionEventBuilder
    implements
        Builder<BeaconMotionEvent, BeaconMotionEventBuilder>,
        BeaconEventBuilder {
  _$BeaconMotionEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  MotionStatus? _status;
  MotionStatus? get status => _$this._status;
  set status(covariant MotionStatus? status) => _$this._status = status;

  BeaconMotionEventBuilder();

  BeaconMotionEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconMotionEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconMotionEvent;
  }

  @override
  void update(void Function(BeaconMotionEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconMotionEvent build() => _build();

  _$BeaconMotionEvent _build() {
    final _$result = _$v ??
        new _$BeaconMotionEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconMotionEvent', 'timestamp'),
            macAddress: macAddress,
            status: BuiltValueNullFieldError.checkNotNull(
                status, r'BeaconMotionEvent', 'status'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconDoorEvent extends BeaconDoorEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final DoorStatus status;

  factory _$BeaconDoorEvent([void Function(BeaconDoorEventBuilder)? updates]) =>
      (new BeaconDoorEventBuilder()..update(updates))._build();

  _$BeaconDoorEvent._(
      {required this.timestamp, this.macAddress, required this.status})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconDoorEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(status, r'BeaconDoorEvent', 'status');
  }

  @override
  BeaconDoorEvent rebuild(void Function(BeaconDoorEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconDoorEventBuilder toBuilder() =>
      new BeaconDoorEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconDoorEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconDoorEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('status', status))
        .toString();
  }
}

class BeaconDoorEventBuilder
    implements
        Builder<BeaconDoorEvent, BeaconDoorEventBuilder>,
        BeaconEventBuilder {
  _$BeaconDoorEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  DoorStatus? _status;
  DoorStatus? get status => _$this._status;
  set status(covariant DoorStatus? status) => _$this._status = status;

  BeaconDoorEventBuilder();

  BeaconDoorEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconDoorEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconDoorEvent;
  }

  @override
  void update(void Function(BeaconDoorEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconDoorEvent build() => _build();

  _$BeaconDoorEvent _build() {
    final _$result = _$v ??
        new _$BeaconDoorEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconDoorEvent', 'timestamp'),
            macAddress: macAddress,
            status: BuiltValueNullFieldError.checkNotNull(
                status, r'BeaconDoorEvent', 'status'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconCo2Event extends BeaconCo2Event {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final int value;

  factory _$BeaconCo2Event([void Function(BeaconCo2EventBuilder)? updates]) =>
      (new BeaconCo2EventBuilder()..update(updates))._build();

  _$BeaconCo2Event._(
      {required this.timestamp, this.macAddress, required this.value})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconCo2Event', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(value, r'BeaconCo2Event', 'value');
  }

  @override
  BeaconCo2Event rebuild(void Function(BeaconCo2EventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconCo2EventBuilder toBuilder() =>
      new BeaconCo2EventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconCo2Event &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        value == other.value;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconCo2Event')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('value', value))
        .toString();
  }
}

class BeaconCo2EventBuilder
    implements
        Builder<BeaconCo2Event, BeaconCo2EventBuilder>,
        BeaconEventBuilder {
  _$BeaconCo2Event? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  int? _value;
  int? get value => _$this._value;
  set value(covariant int? value) => _$this._value = value;

  BeaconCo2EventBuilder();

  BeaconCo2EventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconCo2Event other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconCo2Event;
  }

  @override
  void update(void Function(BeaconCo2EventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconCo2Event build() => _build();

  _$BeaconCo2Event _build() {
    final _$result = _$v ??
        new _$BeaconCo2Event._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconCo2Event', 'timestamp'),
            macAddress: macAddress,
            value: BuiltValueNullFieldError.checkNotNull(
                value, r'BeaconCo2Event', 'value'));
    replace(_$result);
    return _$result;
  }
}

class _$BeaconTotalOdometerEvent extends BeaconTotalOdometerEvent {
  @override
  final DateTime timestamp;
  @override
  final String? macAddress;
  @override
  final int value;

  factory _$BeaconTotalOdometerEvent(
          [void Function(BeaconTotalOdometerEventBuilder)? updates]) =>
      (new BeaconTotalOdometerEventBuilder()..update(updates))._build();

  _$BeaconTotalOdometerEvent._(
      {required this.timestamp, this.macAddress, required this.value})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'BeaconTotalOdometerEvent', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        value, r'BeaconTotalOdometerEvent', 'value');
  }

  @override
  BeaconTotalOdometerEvent rebuild(
          void Function(BeaconTotalOdometerEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconTotalOdometerEventBuilder toBuilder() =>
      new BeaconTotalOdometerEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconTotalOdometerEvent &&
        timestamp == other.timestamp &&
        macAddress == other.macAddress &&
        value == other.value;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BeaconTotalOdometerEvent')
          ..add('timestamp', timestamp)
          ..add('macAddress', macAddress)
          ..add('value', value))
        .toString();
  }
}

class BeaconTotalOdometerEventBuilder
    implements
        Builder<BeaconTotalOdometerEvent, BeaconTotalOdometerEventBuilder>,
        BeaconEventBuilder {
  _$BeaconTotalOdometerEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(covariant DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(covariant String? macAddress) =>
      _$this._macAddress = macAddress;

  int? _value;
  int? get value => _$this._value;
  set value(covariant int? value) => _$this._value = value;

  BeaconTotalOdometerEventBuilder();

  BeaconTotalOdometerEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _macAddress = $v.macAddress;
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant BeaconTotalOdometerEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconTotalOdometerEvent;
  }

  @override
  void update(void Function(BeaconTotalOdometerEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconTotalOdometerEvent build() => _build();

  _$BeaconTotalOdometerEvent _build() {
    final _$result = _$v ??
        new _$BeaconTotalOdometerEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'BeaconTotalOdometerEvent', 'timestamp'),
            macAddress: macAddress,
            value: BuiltValueNullFieldError.checkNotNull(
                value, r'BeaconTotalOdometerEvent', 'value'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
