// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beacon.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const BatteryUnit _$millivolt = const BatteryUnit._('millivolt');
const BatteryUnit _$percent = const BatteryUnit._('percent');

BatteryUnit _$batteryUnitValueOf(String name) {
  switch (name) {
    case 'millivolt':
      return _$millivolt;
    case 'percent':
      return _$percent;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<BatteryUnit> _$batteryUnitValues =
    new BuiltSet<BatteryUnit>(const <BatteryUnit>[
  _$millivolt,
  _$percent,
]);

const BeaconState _$connecting = const BeaconState._('connecting');
const BeaconState _$connected = const BeaconState._('connected');
const BeaconState _$disconnecting = const BeaconState._('disconnecting');
const BeaconState _$disconnected = const BeaconState._('disconnected');
const BeaconState _$unknown = const BeaconState._('unknown');

BeaconState _$stateValueOf(String name) {
  switch (name) {
    case 'connecting':
      return _$connecting;
    case 'connected':
      return _$connected;
    case 'disconnecting':
      return _$disconnecting;
    case 'disconnected':
      return _$disconnected;
    case 'unknown':
      return _$unknown;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<BeaconState> _$stateValues =
    new BuiltSet<BeaconState>(const <BeaconState>[
  _$connecting,
  _$connected,
  _$disconnecting,
  _$disconnected,
  _$unknown,
]);

const BeaconStatus _$active = const BeaconStatus._('active');
const BeaconStatus _$inactive = const BeaconStatus._('inactive');
const BeaconStatus _$alert = const BeaconStatus._('alert');
const BeaconStatus _$unknownA = const BeaconStatus._('unknown');
const BeaconStatus _$lost = const BeaconStatus._('lost');

BeaconStatus _$statusValueOf(String name) {
  switch (name) {
    case 'active':
      return _$active;
    case 'inactive':
      return _$inactive;
    case 'alert':
      return _$alert;
    case 'unknown':
      return _$unknownA;
    case 'lost':
      return _$lost;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<BeaconStatus> _$statusValues =
    new BuiltSet<BeaconStatus>(const <BeaconStatus>[
  _$active,
  _$inactive,
  _$alert,
  _$unknownA,
  _$lost,
]);

const TemperatureUnit _$celsius = const TemperatureUnit._('celsius');
const TemperatureUnit _$degrees = const TemperatureUnit._('degrees');

TemperatureUnit _$temperatureUnitValueOf(String name) {
  switch (name) {
    case 'celsius':
      return _$celsius;
    case 'degrees':
      return _$degrees;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<TemperatureUnit> _$temperatureUnitValues =
    new BuiltSet<TemperatureUnit>(const <TemperatureUnit>[
  _$celsius,
  _$degrees,
]);

const SubscriptionStatus _$subscriptionStatusTrialing =
    const SubscriptionStatus._('trialing');
const SubscriptionStatus _$subscriptionStatusActive =
    const SubscriptionStatus._('active');
const SubscriptionStatus _$subscriptionStatusCanceled =
    const SubscriptionStatus._('canceled');
const SubscriptionStatus _$subscriptionStatusUnpaid =
    const SubscriptionStatus._('unpaid');
const SubscriptionStatus _$subscriptionStatusPast =
    const SubscriptionStatus._('past');
const SubscriptionStatus _$subscriptionStatusUnknown =
    const SubscriptionStatus._('unknown');

SubscriptionStatus _$subscriptionStatusValueOf(String name) {
  switch (name) {
    case 'trialing':
      return _$subscriptionStatusTrialing;
    case 'active':
      return _$subscriptionStatusActive;
    case 'canceled':
      return _$subscriptionStatusCanceled;
    case 'unpaid':
      return _$subscriptionStatusUnpaid;
    case 'past':
      return _$subscriptionStatusPast;
    case 'unknown':
      return _$subscriptionStatusUnknown;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<SubscriptionStatus> _$subscriptionStatusValues =
    new BuiltSet<SubscriptionStatus>(const <SubscriptionStatus>[
  _$subscriptionStatusTrialing,
  _$subscriptionStatusActive,
  _$subscriptionStatusCanceled,
  _$subscriptionStatusUnpaid,
  _$subscriptionStatusPast,
  _$subscriptionStatusUnknown,
]);

const EventType _$buttonPressed = const EventType._('buttonPressed');
const EventType _$buttonPressedSingleClick =
    const EventType._('buttonPressedSingleClick');
const EventType _$buttonPressedDoubleClick =
    const EventType._('buttonPressedDoubleClick');
const EventType _$buttonPressedLongClick =
    const EventType._('buttonPressedLongClick');
const EventType _$geofenceCrossed = const EventType._('geofenceCrossed');
const EventType _$batteryLow = const EventType._('batteryLow');
const EventType _$batteryBadStatus = const EventType._('batteryBadStatus');
const EventType _$doorOpened = const EventType._('doorOpened');
const EventType _$waterLeakDetected = const EventType._('waterLeakDetected');
const EventType _$shockDetected = const EventType._('shockDetected');
const EventType _$rolloverDetected = const EventType._('rolloverDetected');
const EventType _$temperatureThresholdReached =
    const EventType._('temperatureThresholdReached');
const EventType _$co2ThresholdReached =
    const EventType._('co2ThresholdReached');
const EventType _$humidityThresholdReached =
    const EventType._('humidityThresholdReached');
const EventType _$addressCountryChanged =
    const EventType._('addressCountryChanged');
const EventType _$addressStateChanged =
    const EventType._('addressStateChanged');
const EventType _$addressCityChanged = const EventType._('addressCityChanged');
const EventType _$motionDetected = const EventType._('motionDetected');

EventType _$eventTypeValueOf(String name) {
  switch (name) {
    case 'buttonPressed':
      return _$buttonPressed;
    case 'buttonPressedSingleClick':
      return _$buttonPressedSingleClick;
    case 'buttonPressedDoubleClick':
      return _$buttonPressedDoubleClick;
    case 'buttonPressedLongClick':
      return _$buttonPressedLongClick;
    case 'geofenceCrossed':
      return _$geofenceCrossed;
    case 'batteryLow':
      return _$batteryLow;
    case 'batteryBadStatus':
      return _$batteryBadStatus;
    case 'doorOpened':
      return _$doorOpened;
    case 'waterLeakDetected':
      return _$waterLeakDetected;
    case 'shockDetected':
      return _$shockDetected;
    case 'rolloverDetected':
      return _$rolloverDetected;
    case 'temperatureThresholdReached':
      return _$temperatureThresholdReached;
    case 'co2ThresholdReached':
      return _$co2ThresholdReached;
    case 'humidityThresholdReached':
      return _$humidityThresholdReached;
    case 'addressCountryChanged':
      return _$addressCountryChanged;
    case 'addressStateChanged':
      return _$addressStateChanged;
    case 'addressCityChanged':
      return _$addressCityChanged;
    case 'motionDetected':
      return _$motionDetected;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<EventType> _$eventTypeValues =
    new BuiltSet<EventType>(const <EventType>[
  _$buttonPressed,
  _$buttonPressedSingleClick,
  _$buttonPressedDoubleClick,
  _$buttonPressedLongClick,
  _$geofenceCrossed,
  _$batteryLow,
  _$batteryBadStatus,
  _$doorOpened,
  _$waterLeakDetected,
  _$shockDetected,
  _$rolloverDetected,
  _$temperatureThresholdReached,
  _$co2ThresholdReached,
  _$humidityThresholdReached,
  _$addressCountryChanged,
  _$addressStateChanged,
  _$addressCityChanged,
  _$motionDetected,
]);

const RecoveryModeStatus _$recoveryModeInactive =
    const RecoveryModeStatus._('inactive');
const RecoveryModeStatus _$recoveryModeactive =
    const RecoveryModeStatus._('active');
const RecoveryModeStatus _$recoveryModepending =
    const RecoveryModeStatus._('pending');

RecoveryModeStatus _$recoveryModeStatusValueOf(String name) {
  switch (name) {
    case 'inactive':
      return _$recoveryModeInactive;
    case 'active':
      return _$recoveryModeactive;
    case 'pending':
      return _$recoveryModepending;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<RecoveryModeStatus> _$recoveryModeStatusValues =
    new BuiltSet<RecoveryModeStatus>(const <RecoveryModeStatus>[
  _$recoveryModeInactive,
  _$recoveryModeactive,
  _$recoveryModepending,
]);

const PlanType _$prepaid = const PlanType._('prepaid');
const PlanType _$postpaid = const PlanType._('postpaid');
const PlanType _$voucher = const PlanType._('voucher');

PlanType _$typeValueOf(String name) {
  switch (name) {
    case 'prepaid':
      return _$prepaid;
    case 'postpaid':
      return _$postpaid;
    case 'voucher':
      return _$voucher;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<PlanType> _$typeValues = new BuiltSet<PlanType>(const <PlanType>[
  _$prepaid,
  _$postpaid,
  _$voucher,
]);

const BatteryStatus _$good = const BatteryStatus._('good');
const BatteryStatus _$medium = const BatteryStatus._('medium');
const BatteryStatus _$bad = const BatteryStatus._('bad');
const BatteryStatus _$unknownB = const BatteryStatus._('unknown');

BatteryStatus _$batteryStatusValueOf(String name) {
  switch (name) {
    case 'good':
      return _$good;
    case 'medium':
      return _$medium;
    case 'bad':
      return _$bad;
    case 'unknown':
      return _$unknownB;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<BatteryStatus> _$batteryStatusValues =
    new BuiltSet<BatteryStatus>(const <BatteryStatus>[
  _$good,
  _$medium,
  _$bad,
  _$unknownB,
]);

const WaterLeakStatus _$on = const WaterLeakStatus._('on');
const WaterLeakStatus _$off = const WaterLeakStatus._('off');

WaterLeakStatus _$waterLeakStatusValueOf(String name) {
  switch (name) {
    case 'on':
      return _$on;
    case 'off':
      return _$off;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<WaterLeakStatus> _$waterLeakStatusValues =
    new BuiltSet<WaterLeakStatus>(const <WaterLeakStatus>[
  _$on,
  _$off,
]);

const DoorStatus _$open = const DoorStatus._('open');
const DoorStatus _$closed = const DoorStatus._('closed');

DoorStatus _$doorStatusValueOf(String name) {
  switch (name) {
    case 'open':
      return _$open;
    case 'closed':
      return _$closed;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<DoorStatus> _$doorStatusValues =
    new BuiltSet<DoorStatus>(const <DoorStatus>[
  _$open,
  _$closed,
]);

const ShockStatus _$clear = const ShockStatus._('clear');
const ShockStatus _$detected = const ShockStatus._('detected');

ShockStatus _$shockStatusValueOf(String name) {
  switch (name) {
    case 'clear':
      return _$clear;
    case 'detected':
      return _$detected;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ShockStatus> _$shockStatusValues =
    new BuiltSet<ShockStatus>(const <ShockStatus>[
  _$clear,
  _$detected,
]);

const MotionStatus _$still = const MotionStatus._('still');
const MotionStatus _$moving = const MotionStatus._('moving');

MotionStatus _$motionStatusValueOf(String name) {
  switch (name) {
    case 'still':
      return _$still;
    case 'moving':
      return _$moving;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<MotionStatus> _$motionStatusValues =
    new BuiltSet<MotionStatus>(const <MotionStatus>[
  _$still,
  _$moving,
]);

const RolloverStatus _$rolloverStatusClear = const RolloverStatus._('clear');
const RolloverStatus _$rolloverStatusDetected =
    const RolloverStatus._('detected');

RolloverStatus _$rolloverStatusValueOf(String name) {
  switch (name) {
    case 'clear':
      return _$rolloverStatusClear;
    case 'detected':
      return _$rolloverStatusDetected;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<RolloverStatus> _$rolloverStatusValues =
    new BuiltSet<RolloverStatus>(const <RolloverStatus>[
  _$rolloverStatusClear,
  _$rolloverStatusDetected,
]);

Serializer<Beacon> _$beaconSerializer = new _$BeaconSerializer();
Serializer<BatteryUnit> _$batteryUnitSerializer = new _$BatteryUnitSerializer();
Serializer<BeaconState> _$beaconStateSerializer = new _$BeaconStateSerializer();
Serializer<BeaconStatus> _$beaconStatusSerializer =
    new _$BeaconStatusSerializer();
Serializer<ButtonEvent> _$buttonEventSerializer = new _$ButtonEventSerializer();
Serializer<MotionInfo> _$motionInfoSerializer = new _$MotionInfoSerializer();
Serializer<ShockInfo> _$shockInfoSerializer = new _$ShockInfoSerializer();
Serializer<RolloverInfo> _$rolloverInfoSerializer =
    new _$RolloverInfoSerializer();
Serializer<DoorInfo> _$doorInfoSerializer = new _$DoorInfoSerializer();
Serializer<Co2Info> _$co2InfoSerializer = new _$Co2InfoSerializer();
Serializer<HumidityInfo> _$humidityInfoSerializer =
    new _$HumidityInfoSerializer();
Serializer<TemperatureUnit> _$temperatureUnitSerializer =
    new _$TemperatureUnitSerializer();
Serializer<TemperatureInfo> _$temperatureInfoSerializer =
    new _$TemperatureInfoSerializer();
Serializer<TotalOdometerInfo> _$totalOdometerInfoSerializer =
    new _$TotalOdometerInfoSerializer();
Serializer<WaterLeakInfo> _$waterLeakInfoSerializer =
    new _$WaterLeakInfoSerializer();
Serializer<LocationInfo> _$locationInfoSerializer =
    new _$LocationInfoSerializer();
Serializer<LastBattery> _$lastBatterySerializer = new _$LastBatterySerializer();
Serializer<Product> _$productSerializer = new _$ProductSerializer();
Serializer<Subscription> _$subscriptionSerializer =
    new _$SubscriptionSerializer();
Serializer<SubscriptionStatus> _$subscriptionStatusSerializer =
    new _$SubscriptionStatusSerializer();
Serializer<Plan> _$planSerializer = new _$PlanSerializer();
Serializer<CreditCard> _$creditCardSerializer = new _$CreditCardSerializer();
Serializer<Voucher> _$voucherSerializer = new _$VoucherSerializer();
Serializer<Invoice> _$invoiceSerializer = new _$InvoiceSerializer();
Serializer<Notification> _$notificationSerializer =
    new _$NotificationSerializer();
Serializer<BeaconConfiguration> _$beaconConfigurationSerializer =
    new _$BeaconConfigurationSerializer();
Serializer<Email> _$emailSerializer = new _$EmailSerializer();
Serializer<Phone> _$phoneSerializer = new _$PhoneSerializer();
Serializer<EventType> _$eventTypeSerializer = new _$EventTypeSerializer();
Serializer<RecoveryModeStatus> _$recoveryModeStatusSerializer =
    new _$RecoveryModeStatusSerializer();
Serializer<PlanType> _$planTypeSerializer = new _$PlanTypeSerializer();
Serializer<BatteryStatus> _$batteryStatusSerializer =
    new _$BatteryStatusSerializer();
Serializer<WaterLeakStatus> _$waterLeakStatusSerializer =
    new _$WaterLeakStatusSerializer();
Serializer<DoorStatus> _$doorStatusSerializer = new _$DoorStatusSerializer();
Serializer<ShockStatus> _$shockStatusSerializer = new _$ShockStatusSerializer();
Serializer<MotionStatus> _$motionStatusSerializer =
    new _$MotionStatusSerializer();
Serializer<RolloverStatus> _$rolloverStatusSerializer =
    new _$RolloverStatusSerializer();

class _$BeaconSerializer implements StructuredSerializer<Beacon> {
  @override
  final Iterable<Type> types = const [Beacon, _$Beacon];
  @override
  final String wireName = 'Beacon';

  @override
  Iterable<Object?> serialize(Serializers serializers, Beacon object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'device_id',
      serializers.serialize(object.identifier,
          specifiedType: const FullType(String)),
      'product',
      serializers.serialize(object.product,
          specifiedType: const FullType(Product)),
    ];
    Object? value;
    value = object.serialNumber;
    if (value != null) {
      result
        ..add('serial_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.macAddress;
    if (value != null) {
      result
        ..add('mac_address')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.iosPeripheralId;
    if (value != null) {
      result
        ..add('iosPeripheralId')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.name;
    if (value != null) {
      result
        ..add('name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BeaconStatus)));
    }
    value = object.iconName;
    if (value != null) {
      result
        ..add('icon_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.major;
    if (value != null) {
      result
        ..add('major')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.minor;
    if (value != null) {
      result
        ..add('minor')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.lastDoor;
    if (value != null) {
      result
        ..add('last_door')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DoorInfo)));
    }
    value = object.lastLocation;
    if (value != null) {
      result
        ..add('last_location')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(LocationInfo)));
    }
    value = object.lastWaterLeak;
    if (value != null) {
      result
        ..add('last_water_leak')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(WaterLeakInfo)));
    }
    value = object.lastTotalOdometer;
    if (value != null) {
      result
        ..add('last_total_odometer')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(TotalOdometerInfo)));
    }
    value = object.lastCo2;
    if (value != null) {
      result
        ..add('last_co2')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Co2Info)));
    }
    value = object.lastTemperature;
    if (value != null) {
      result
        ..add('last_temperature')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(TemperatureInfo)));
    }
    value = object.lastHumidity;
    if (value != null) {
      result
        ..add('last_humidity')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(HumidityInfo)));
    }
    value = object.lastRollover;
    if (value != null) {
      result
        ..add('last_rollover')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(RolloverInfo)));
    }
    value = object.lastMotion;
    if (value != null) {
      result
        ..add('last_motion')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(MotionInfo)));
    }
    value = object.lastShock;
    if (value != null) {
      result
        ..add('last_shock')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(ShockInfo)));
    }
    value = object.lastBattery;
    if (value != null) {
      result
        ..add('last_battery')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(LastBattery)));
    }
    value = object.imageUrl;
    if (value != null) {
      result
        ..add('image_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.thumbnailUrl;
    if (value != null) {
      result
        ..add('thumbnail_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.lastSubscription;
    if (value != null) {
      result
        ..add('last_subscription')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Subscription)));
    }
    value = object.configuration;
    if (value != null) {
      result
        ..add('configuration')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BeaconConfiguration)));
    }
    value = object.softwareVersion;
    if (value != null) {
      result
        ..add('software_version')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.organization;
    if (value != null) {
      result
        ..add('organization')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Organization)));
    }
    return result;
  }

  @override
  Beacon deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'device_id':
          result.identifier = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'serial_number':
          result.serialNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'mac_address':
          result.macAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'iosPeripheralId':
          result.iosPeripheralId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(BeaconStatus)) as BeaconStatus?;
          break;
        case 'icon_name':
          result.iconName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'major':
          result.major = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'minor':
          result.minor = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'product':
          result.product.replace(serializers.deserialize(value,
              specifiedType: const FullType(Product))! as Product);
          break;
        case 'last_door':
          result.lastDoor.replace(serializers.deserialize(value,
              specifiedType: const FullType(DoorInfo))! as DoorInfo);
          break;
        case 'last_location':
          result.lastLocation.replace(serializers.deserialize(value,
              specifiedType: const FullType(LocationInfo))! as LocationInfo);
          break;
        case 'last_water_leak':
          result.lastWaterLeak.replace(serializers.deserialize(value,
              specifiedType: const FullType(WaterLeakInfo))! as WaterLeakInfo);
          break;
        case 'last_total_odometer':
          result.lastTotalOdometer.replace(serializers.deserialize(value,
                  specifiedType: const FullType(TotalOdometerInfo))!
              as TotalOdometerInfo);
          break;
        case 'last_co2':
          result.lastCo2.replace(serializers.deserialize(value,
              specifiedType: const FullType(Co2Info))! as Co2Info);
          break;
        case 'last_temperature':
          result.lastTemperature.replace(serializers.deserialize(value,
                  specifiedType: const FullType(TemperatureInfo))!
              as TemperatureInfo);
          break;
        case 'last_humidity':
          result.lastHumidity.replace(serializers.deserialize(value,
              specifiedType: const FullType(HumidityInfo))! as HumidityInfo);
          break;
        case 'last_rollover':
          result.lastRollover.replace(serializers.deserialize(value,
              specifiedType: const FullType(RolloverInfo))! as RolloverInfo);
          break;
        case 'last_motion':
          result.lastMotion.replace(serializers.deserialize(value,
              specifiedType: const FullType(MotionInfo))! as MotionInfo);
          break;
        case 'last_shock':
          result.lastShock.replace(serializers.deserialize(value,
              specifiedType: const FullType(ShockInfo))! as ShockInfo);
          break;
        case 'last_battery':
          result.lastBattery.replace(serializers.deserialize(value,
              specifiedType: const FullType(LastBattery))! as LastBattery);
          break;
        case 'image_url':
          result.imageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'thumbnail_url':
          result.thumbnailUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'last_subscription':
          result.lastSubscription.replace(serializers.deserialize(value,
              specifiedType: const FullType(Subscription))! as Subscription);
          break;
        case 'configuration':
          result.configuration.replace(serializers.deserialize(value,
                  specifiedType: const FullType(BeaconConfiguration))!
              as BeaconConfiguration);
          break;
        case 'software_version':
          result.softwareVersion = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'organization':
          result.organization.replace(serializers.deserialize(value,
              specifiedType: const FullType(Organization))! as Organization);
          break;
      }
    }

    return result.build();
  }
}

class _$BatteryUnitSerializer implements PrimitiveSerializer<BatteryUnit> {
  @override
  final Iterable<Type> types = const <Type>[BatteryUnit];
  @override
  final String wireName = 'BatteryUnit';

  @override
  Object serialize(Serializers serializers, BatteryUnit object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  BatteryUnit deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      BatteryUnit.valueOf(serialized as String);
}

class _$BeaconStateSerializer implements PrimitiveSerializer<BeaconState> {
  @override
  final Iterable<Type> types = const <Type>[BeaconState];
  @override
  final String wireName = 'BeaconState';

  @override
  Object serialize(Serializers serializers, BeaconState object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  BeaconState deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      BeaconState.valueOf(serialized as String);
}

class _$BeaconStatusSerializer implements PrimitiveSerializer<BeaconStatus> {
  @override
  final Iterable<Type> types = const <Type>[BeaconStatus];
  @override
  final String wireName = 'BeaconStatus';

  @override
  Object serialize(Serializers serializers, BeaconStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  BeaconStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      BeaconStatus.valueOf(serialized as String);
}

class _$ButtonEventSerializer implements StructuredSerializer<ButtonEvent> {
  @override
  final Iterable<Type> types = const [ButtonEvent, _$ButtonEvent];
  @override
  final String wireName = 'ButtonEvent';

  @override
  Iterable<Object?> serialize(Serializers serializers, ButtonEvent object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
    ];

    return result;
  }

  @override
  ButtonEvent deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ButtonEventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
      }
    }

    return result.build();
  }
}

class _$MotionInfoSerializer implements StructuredSerializer<MotionInfo> {
  @override
  final Iterable<Type> types = const [MotionInfo, _$MotionInfo];
  @override
  final String wireName = 'MotionInfo';

  @override
  Iterable<Object?> serialize(Serializers serializers, MotionInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.timestamp;
    if (value != null) {
      result
        ..add('timestamp')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(MotionStatus)));
    }
    return result;
  }

  @override
  MotionInfo deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MotionInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(MotionStatus)) as MotionStatus?;
          break;
      }
    }

    return result.build();
  }
}

class _$ShockInfoSerializer implements StructuredSerializer<ShockInfo> {
  @override
  final Iterable<Type> types = const [ShockInfo, _$ShockInfo];
  @override
  final String wireName = 'ShockInfo';

  @override
  Iterable<Object?> serialize(Serializers serializers, ShockInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.timestamp;
    if (value != null) {
      result
        ..add('timestamp')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(ShockStatus)));
    }
    return result;
  }

  @override
  ShockInfo deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ShockInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(ShockStatus)) as ShockStatus?;
          break;
      }
    }

    return result.build();
  }
}

class _$RolloverInfoSerializer implements StructuredSerializer<RolloverInfo> {
  @override
  final Iterable<Type> types = const [RolloverInfo, _$RolloverInfo];
  @override
  final String wireName = 'RolloverInfo';

  @override
  Iterable<Object?> serialize(Serializers serializers, RolloverInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.timestamp;
    if (value != null) {
      result
        ..add('timestamp')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(RolloverStatus)));
    }
    return result;
  }

  @override
  RolloverInfo deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RolloverInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(RolloverStatus)) as RolloverStatus?;
          break;
      }
    }

    return result.build();
  }
}

class _$DoorInfoSerializer implements StructuredSerializer<DoorInfo> {
  @override
  final Iterable<Type> types = const [DoorInfo, _$DoorInfo];
  @override
  final String wireName = 'DoorInfo';

  @override
  Iterable<Object?> serialize(Serializers serializers, DoorInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.timestamp;
    if (value != null) {
      result
        ..add('timestamp')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DoorStatus)));
    }
    return result;
  }

  @override
  DoorInfo deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DoorInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(DoorStatus)) as DoorStatus?;
          break;
      }
    }

    return result.build();
  }
}

class _$Co2InfoSerializer implements StructuredSerializer<Co2Info> {
  @override
  final Iterable<Type> types = const [Co2Info, _$Co2Info];
  @override
  final String wireName = 'Co2Info';

  @override
  Iterable<Object?> serialize(Serializers serializers, Co2Info object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.timestamp;
    if (value != null) {
      result
        ..add('timestamp')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.value;
    if (value != null) {
      result
        ..add('value')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  Co2Info deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new Co2InfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$HumidityInfoSerializer implements StructuredSerializer<HumidityInfo> {
  @override
  final Iterable<Type> types = const [HumidityInfo, _$HumidityInfo];
  @override
  final String wireName = 'HumidityInfo';

  @override
  Iterable<Object?> serialize(Serializers serializers, HumidityInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.timestamp;
    if (value != null) {
      result
        ..add('timestamp')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.value;
    if (value != null) {
      result
        ..add('value')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  HumidityInfo deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new HumidityInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
      }
    }

    return result.build();
  }
}

class _$TemperatureUnitSerializer
    implements PrimitiveSerializer<TemperatureUnit> {
  @override
  final Iterable<Type> types = const <Type>[TemperatureUnit];
  @override
  final String wireName = 'TemperatureUnit';

  @override
  Object serialize(Serializers serializers, TemperatureUnit object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  TemperatureUnit deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      TemperatureUnit.valueOf(serialized as String);
}

class _$TemperatureInfoSerializer
    implements StructuredSerializer<TemperatureInfo> {
  @override
  final Iterable<Type> types = const [TemperatureInfo, _$TemperatureInfo];
  @override
  final String wireName = 'TemperatureInfo';

  @override
  Iterable<Object?> serialize(Serializers serializers, TemperatureInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.timestamp;
    if (value != null) {
      result
        ..add('timestamp')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.value;
    if (value != null) {
      result
        ..add('value')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.unit;
    if (value != null) {
      result
        ..add('unit')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(TemperatureUnit)));
    }
    return result;
  }

  @override
  TemperatureInfo deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TemperatureInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'unit':
          result.unit = serializers.deserialize(value,
                  specifiedType: const FullType(TemperatureUnit))
              as TemperatureUnit?;
          break;
      }
    }

    return result.build();
  }
}

class _$TotalOdometerInfoSerializer
    implements StructuredSerializer<TotalOdometerInfo> {
  @override
  final Iterable<Type> types = const [TotalOdometerInfo, _$TotalOdometerInfo];
  @override
  final String wireName = 'TotalOdometerInfo';

  @override
  Iterable<Object?> serialize(Serializers serializers, TotalOdometerInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.timestamp;
    if (value != null) {
      result
        ..add('timestamp')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.value;
    if (value != null) {
      result
        ..add('value')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  TotalOdometerInfo deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TotalOdometerInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$WaterLeakInfoSerializer implements StructuredSerializer<WaterLeakInfo> {
  @override
  final Iterable<Type> types = const [WaterLeakInfo, _$WaterLeakInfo];
  @override
  final String wireName = 'WaterLeakInfo';

  @override
  Iterable<Object?> serialize(Serializers serializers, WaterLeakInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.timestamp;
    if (value != null) {
      result
        ..add('timestamp')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(WaterLeakStatus)));
    }
    return result;
  }

  @override
  WaterLeakInfo deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WaterLeakInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(WaterLeakStatus))
              as WaterLeakStatus?;
          break;
      }
    }

    return result.build();
  }
}

class _$LocationInfoSerializer implements StructuredSerializer<LocationInfo> {
  @override
  final Iterable<Type> types = const [LocationInfo, _$LocationInfo];
  @override
  final String wireName = 'LocationInfo';

  @override
  Iterable<Object?> serialize(Serializers serializers, LocationInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'latitude',
      serializers.serialize(object.latitude,
          specifiedType: const FullType(double)),
      'longitude',
      serializers.serialize(object.longitude,
          specifiedType: const FullType(double)),
    ];
    Object? value;
    value = object.altitude;
    if (value != null) {
      result
        ..add('altitude')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.speed;
    if (value != null) {
      result
        ..add('speed')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.satellites;
    if (value != null) {
      result
        ..add('satellites')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.formattedAddress;
    if (value != null) {
      result
        ..add('formatted_address')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isButtonEventDoubleClick;
    if (value != null) {
      result
        ..add('isButtonEventDoubleClick')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  LocationInfo deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LocationInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'altitude':
          result.altitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'speed':
          result.speed = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'satellites':
          result.satellites = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'formatted_address':
          result.formattedAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'isButtonEventDoubleClick':
          result.isButtonEventDoubleClick = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
      }
    }

    return result.build();
  }
}

class _$LastBatterySerializer implements StructuredSerializer<LastBattery> {
  @override
  final Iterable<Type> types = const [LastBattery, _$LastBattery];
  @override
  final String wireName = 'LastBattery';

  @override
  Iterable<Object?> serialize(Serializers serializers, LastBattery object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
    ];
    Object? value;
    value = object.value;
    if (value != null) {
      result
        ..add('value')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.unit;
    if (value != null) {
      result
        ..add('unit')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BatteryUnit)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BatteryStatus)));
    }
    return result;
  }

  @override
  LastBattery deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LastBatteryBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'unit':
          result.unit = serializers.deserialize(value,
              specifiedType: const FullType(BatteryUnit)) as BatteryUnit?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(BatteryStatus)) as BatteryStatus?;
          break;
      }
    }

    return result.build();
  }
}

class _$ProductSerializer implements StructuredSerializer<Product> {
  @override
  final Iterable<Type> types = const [Product, _$Product];
  @override
  final String wireName = 'Product';

  @override
  Iterable<Object?> serialize(Serializers serializers, Product object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'marketing_name',
      serializers.serialize(object.marketingName,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.imageUrl;
    if (value != null) {
      result
        ..add('image_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Product deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProductBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'image_url':
          result.imageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'marketing_name':
          result.marketingName = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SubscriptionSerializer implements StructuredSerializer<Subscription> {
  @override
  final Iterable<Type> types = const [Subscription, _$Subscription];
  @override
  final String wireName = 'Subscription';

  @override
  Iterable<Object?> serialize(Serializers serializers, Subscription object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'start_date',
      serializers.serialize(object.startDate,
          specifiedType: const FullType(DateTime)),
      'end_date',
      serializers.serialize(object.endDate,
          specifiedType: const FullType(DateTime)),
      'status',
      serializers.serialize(object.status,
          specifiedType: const FullType(SubscriptionStatus)),
    ];
    Object? value;
    value = object.subscriptionId;
    if (value != null) {
      result
        ..add('subscription_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.deviceId;
    if (value != null) {
      result
        ..add('device_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.voucherId;
    if (value != null) {
      result
        ..add('voucher_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.plan;
    if (value != null) {
      result
        ..add('plan')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(Plan)));
    }
    return result;
  }

  @override
  Subscription deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SubscriptionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'subscription_id':
          result.subscriptionId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'device_id':
          result.deviceId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'voucher_id':
          result.voucherId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'start_date':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'end_date':
          result.endDate = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(SubscriptionStatus))!
              as SubscriptionStatus;
          break;
        case 'plan':
          result.plan.replace(serializers.deserialize(value,
              specifiedType: const FullType(Plan))! as Plan);
          break;
      }
    }

    return result.build();
  }
}

class _$SubscriptionStatusSerializer
    implements PrimitiveSerializer<SubscriptionStatus> {
  @override
  final Iterable<Type> types = const <Type>[SubscriptionStatus];
  @override
  final String wireName = 'SubscriptionStatus';

  @override
  Object serialize(Serializers serializers, SubscriptionStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  SubscriptionStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      SubscriptionStatus.valueOf(serialized as String);
}

class _$PlanSerializer implements StructuredSerializer<Plan> {
  @override
  final Iterable<Type> types = const [Plan, _$Plan];
  @override
  final String wireName = 'Plan';

  @override
  Iterable<Object?> serialize(Serializers serializers, Plan object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'plan_id',
      serializers.serialize(object.planId,
          specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'amount',
      serializers.serialize(object.amount,
          specifiedType: const FullType(double)),
      'tax_percent',
      serializers.serialize(object.taxPercent,
          specifiedType: const FullType(double)),
      'currency',
      serializers.serialize(object.currency,
          specifiedType: const FullType(String)),
      'type',
      serializers.serialize(object.type,
          specifiedType: const FullType(PlanType)),
      'frequency',
      serializers.serialize(object.frequency,
          specifiedType: const FullType(String)),
      'frequency_interval',
      serializers.serialize(object.frequencyInterval,
          specifiedType: const FullType(int)),
    ];
    Object? value;
    value = object.description;
    if (value != null) {
      result
        ..add('description')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.operator;
    if (value != null) {
      result
        ..add('operator')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.trialPeriodDays;
    if (value != null) {
      result
        ..add('trial_period_days')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  Plan deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PlanBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'plan_id':
          result.planId = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'amount':
          result.amount = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'tax_percent':
          result.taxPercent = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'currency':
          result.currency = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(PlanType))! as PlanType;
          break;
        case 'frequency':
          result.frequency = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'frequency_interval':
          result.frequencyInterval = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'operator':
          result.operator = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'trial_period_days':
          result.trialPeriodDays = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$CreditCardSerializer implements StructuredSerializer<CreditCard> {
  @override
  final Iterable<Type> types = const [CreditCard, _$CreditCard];
  @override
  final String wireName = 'CreditCard';

  @override
  Iterable<Object?> serialize(Serializers serializers, CreditCard object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'fop_id',
      serializers.serialize(object.fopId,
          specifiedType: const FullType(String)),
      'exp_month',
      serializers.serialize(object.expirationMonth,
          specifiedType: const FullType(int)),
      'exp_year',
      serializers.serialize(object.expirationYear,
          specifiedType: const FullType(int)),
      'last4',
      serializers.serialize(object.last4Digits,
          specifiedType: const FullType(String)),
      'brand',
      serializers.serialize(object.brand,
          specifiedType: const FullType(String)),
      'default_card',
      serializers.serialize(object.isDefaultCard,
          specifiedType: const FullType(bool)),
    ];

    return result;
  }

  @override
  CreditCard deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CreditCardBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'fop_id':
          result.fopId = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'exp_month':
          result.expirationMonth = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'exp_year':
          result.expirationYear = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'last4':
          result.last4Digits = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'brand':
          result.brand = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'default_card':
          result.isDefaultCard = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$VoucherSerializer implements StructuredSerializer<Voucher> {
  @override
  final Iterable<Type> types = const [Voucher, _$Voucher];
  @override
  final String wireName = 'Voucher';

  @override
  Iterable<Object?> serialize(Serializers serializers, Voucher object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'voucher_id',
      serializers.serialize(object.voucherId,
          specifiedType: const FullType(String)),
      'code',
      serializers.serialize(object.code, specifiedType: const FullType(String)),
      'serial_number',
      serializers.serialize(object.serialNumber,
          specifiedType: const FullType(String)),
      'activated',
      serializers.serialize(object.isActivated,
          specifiedType: const FullType(bool)),
      'plan',
      serializers.serialize(object.plan, specifiedType: const FullType(Plan)),
    ];

    return result;
  }

  @override
  Voucher deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new VoucherBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'voucher_id':
          result.voucherId = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'serial_number':
          result.serialNumber = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'activated':
          result.isActivated = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'plan':
          result.plan.replace(serializers.deserialize(value,
              specifiedType: const FullType(Plan))! as Plan);
          break;
      }
    }

    return result.build();
  }
}

class _$InvoiceSerializer implements StructuredSerializer<Invoice> {
  @override
  final Iterable<Type> types = const [Invoice, _$Invoice];
  @override
  final String wireName = 'Invoice';

  @override
  Iterable<Object?> serialize(Serializers serializers, Invoice object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'invoice_id',
      serializers.serialize(object.invoiceId,
          specifiedType: const FullType(String)),
      'period_start',
      serializers.serialize(object.startDate,
          specifiedType: const FullType(DateTime)),
      'period_end',
      serializers.serialize(object.endDate,
          specifiedType: const FullType(DateTime)),
      'currency',
      serializers.serialize(object.currency,
          specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'number',
      serializers.serialize(object.number,
          specifiedType: const FullType(String)),
      'created',
      serializers.serialize(object.created,
          specifiedType: const FullType(DateTime)),
      'tax_amount',
      serializers.serialize(object.taxAmount,
          specifiedType: const FullType(int)),
      'total_amount',
      serializers.serialize(object.totalAmount,
          specifiedType: const FullType(int)),
      'owner_id',
      serializers.serialize(object.ownerId,
          specifiedType: const FullType(String)),
      'owner_stripe_id',
      serializers.serialize(object.stripeId,
          specifiedType: const FullType(String)),
      'pdf',
      serializers.serialize(object.pdfUrl,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Invoice deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new InvoiceBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'invoice_id':
          result.invoiceId = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'period_start':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'period_end':
          result.endDate = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'currency':
          result.currency = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'number':
          result.number = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'created':
          result.created = serializers.deserialize(value,
              specifiedType: const FullType(DateTime))! as DateTime;
          break;
        case 'tax_amount':
          result.taxAmount = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'total_amount':
          result.totalAmount = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'owner_id':
          result.ownerId = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'owner_stripe_id':
          result.stripeId = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'pdf':
          result.pdfUrl = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$NotificationSerializer implements StructuredSerializer<Notification> {
  @override
  final Iterable<Type> types = const [Notification, _$Notification];
  @override
  final String wireName = 'Notification';

  @override
  Iterable<Object?> serialize(Serializers serializers, Notification object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'to',
      serializers.serialize(object.to,
          specifiedType:
              const FullType(BuiltList, const [const FullType(String)])),
      'device_id',
      serializers.serialize(object.deviceId,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.channel;
    if (value != null) {
      result
        ..add('channel')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.event;
    if (value != null) {
      result
        ..add('event')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(EventType)));
    }
    value = object.options;
    if (value != null) {
      result
        ..add('options')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.isActive;
    if (value != null) {
      result
        ..add('active')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.id;
    if (value != null) {
      result
        ..add('notification_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.emails;
    if (value != null) {
      result
        ..add('emails')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Email)])));
    }
    value = object.phones;
    if (value != null) {
      result
        ..add('phones')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Phone)])));
    }
    return result;
  }

  @override
  Notification deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new NotificationBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'channel':
          result.channel = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'event':
          result.event = serializers.deserialize(value,
              specifiedType: const FullType(EventType)) as EventType?;
          break;
        case 'options':
          result.options.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case 'active':
          result.isActive = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'to':
          result.to.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case 'notification_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'device_id':
          result.deviceId = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'emails':
          result.emails.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Email)]))!
              as BuiltList<Object?>);
          break;
        case 'phones':
          result.phones.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Phone)]))!
              as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$BeaconConfigurationSerializer
    implements StructuredSerializer<BeaconConfiguration> {
  @override
  final Iterable<Type> types = const [
    BeaconConfiguration,
    _$BeaconConfiguration
  ];
  @override
  final String wireName = 'BeaconConfiguration';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, BeaconConfiguration object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.deviceConfigurationId;
    if (value != null) {
      result
        ..add('device_configuration_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.geolocationPeriod;
    if (value != null) {
      result
        ..add('geolocation_period')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.sendMessageInMotion;
    if (value != null) {
      result
        ..add('send_message_in_motion')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.retryPeriod;
    if (value != null) {
      result
        ..add('retry_period')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.isWifiSniffing;
    if (value != null) {
      result
        ..add('wifi_sniffing')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.isCo2SendStatistics;
    if (value != null) {
      result
        ..add('co2_send_statistics')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.co2SendInterval;
    if (value != null) {
      result
        ..add('co2_send_interval')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.co2ExtraMeasurementCount;
    if (value != null) {
      result
        ..add('co2_extra_measurement_count')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.co2Offset;
    if (value != null) {
      result
        ..add('co2_offset')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.co2AlertThreshold;
    if (value != null) {
      result
        ..add('co2_alert_threshold')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.recoveryModeRequest;
    if (value != null) {
      result
        ..add('recovery_mode_request')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.recoveryModeExit;
    if (value != null) {
      result
        ..add('recovery_mode_exit')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.configurationPlanId;
    if (value != null) {
      result
        ..add('yabby_configuration_plan_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.subscriptionPlanId;
    if (value != null) {
      result
        ..add('subscription_plan_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.recoveryModeStatus;
    if (value != null) {
      result
        ..add('recovery_mode_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(RecoveryModeStatus)));
    }
    return result;
  }

  @override
  BeaconConfiguration deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeaconConfigurationBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'device_configuration_id':
          result.deviceConfigurationId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'geolocation_period':
          result.geolocationPeriod = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'send_message_in_motion':
          result.sendMessageInMotion = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'retry_period':
          result.retryPeriod = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'wifi_sniffing':
          result.isWifiSniffing = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'co2_send_statistics':
          result.isCo2SendStatistics = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'co2_send_interval':
          result.co2SendInterval = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'co2_extra_measurement_count':
          result.co2ExtraMeasurementCount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'co2_offset':
          result.co2Offset = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'co2_alert_threshold':
          result.co2AlertThreshold = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'recovery_mode_request':
          result.recoveryModeRequest = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'recovery_mode_exit':
          result.recoveryModeExit = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'yabby_configuration_plan_id':
          result.configurationPlanId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'subscription_plan_id':
          result.subscriptionPlanId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'recovery_mode_status':
          result.recoveryModeStatus = serializers.deserialize(value,
                  specifiedType: const FullType(RecoveryModeStatus))
              as RecoveryModeStatus?;
          break;
      }
    }

    return result.build();
  }
}

class _$EmailSerializer implements StructuredSerializer<Email> {
  @override
  final Iterable<Type> types = const [Email, _$Email];
  @override
  final String wireName = 'Email';

  @override
  Iterable<Object?> serialize(Serializers serializers, Email object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'email',
      serializers.serialize(object.email,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Email deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EmailBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PhoneSerializer implements StructuredSerializer<Phone> {
  @override
  final Iterable<Type> types = const [Phone, _$Phone];
  @override
  final String wireName = 'Phone';

  @override
  Iterable<Object?> serialize(Serializers serializers, Phone object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Phone deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PhoneBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'phone':
          result.phone = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$EventTypeSerializer implements PrimitiveSerializer<EventType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'buttonPressed': 'button_press',
    'buttonPressedSingleClick': 'button_pressed',
    'buttonPressedDoubleClick': 'button_double_pressed',
    'buttonPressedLongClick': 'button_held',
    'geofenceCrossed': 'geofence_crossed',
    'batteryLow': 'battery_low',
    'batteryBadStatus': 'battery_status_bad',
    'doorOpened': 'door_opened',
    'waterLeakDetected': 'water_leak_detected',
    'shockDetected': 'shock_detected',
    'rolloverDetected': 'rollover_detected',
    'temperatureThresholdReached': 'temperature_threshold_reached',
    'co2ThresholdReached': 'co2_threshold_reached',
    'humidityThresholdReached': 'humidity_threshold_reached',
    'addressCountryChanged': 'address_country_changed',
    'addressStateChanged': 'address_state_changed',
    'addressCityChanged': 'address_city_changed',
    'motionDetected': 'motion_detected',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'button_press': 'buttonPressed',
    'button_pressed': 'buttonPressedSingleClick',
    'button_double_pressed': 'buttonPressedDoubleClick',
    'button_held': 'buttonPressedLongClick',
    'geofence_crossed': 'geofenceCrossed',
    'battery_low': 'batteryLow',
    'battery_status_bad': 'batteryBadStatus',
    'door_opened': 'doorOpened',
    'water_leak_detected': 'waterLeakDetected',
    'shock_detected': 'shockDetected',
    'rollover_detected': 'rolloverDetected',
    'temperature_threshold_reached': 'temperatureThresholdReached',
    'co2_threshold_reached': 'co2ThresholdReached',
    'humidity_threshold_reached': 'humidityThresholdReached',
    'address_country_changed': 'addressCountryChanged',
    'address_state_changed': 'addressStateChanged',
    'address_city_changed': 'addressCityChanged',
    'motion_detected': 'motionDetected',
  };

  @override
  final Iterable<Type> types = const <Type>[EventType];
  @override
  final String wireName = 'EventType';

  @override
  Object serialize(Serializers serializers, EventType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  EventType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      EventType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$RecoveryModeStatusSerializer
    implements PrimitiveSerializer<RecoveryModeStatus> {
  @override
  final Iterable<Type> types = const <Type>[RecoveryModeStatus];
  @override
  final String wireName = 'RecoveryModeStatus';

  @override
  Object serialize(Serializers serializers, RecoveryModeStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  RecoveryModeStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      RecoveryModeStatus.valueOf(serialized as String);
}

class _$PlanTypeSerializer implements PrimitiveSerializer<PlanType> {
  @override
  final Iterable<Type> types = const <Type>[PlanType];
  @override
  final String wireName = 'PlanType';

  @override
  Object serialize(Serializers serializers, PlanType object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  PlanType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      PlanType.valueOf(serialized as String);
}

class _$BatteryStatusSerializer implements PrimitiveSerializer<BatteryStatus> {
  @override
  final Iterable<Type> types = const <Type>[BatteryStatus];
  @override
  final String wireName = 'BatteryStatus';

  @override
  Object serialize(Serializers serializers, BatteryStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  BatteryStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      BatteryStatus.valueOf(serialized as String);
}

class _$WaterLeakStatusSerializer
    implements PrimitiveSerializer<WaterLeakStatus> {
  @override
  final Iterable<Type> types = const <Type>[WaterLeakStatus];
  @override
  final String wireName = 'WaterLeakStatus';

  @override
  Object serialize(Serializers serializers, WaterLeakStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  WaterLeakStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      WaterLeakStatus.valueOf(serialized as String);
}

class _$DoorStatusSerializer implements PrimitiveSerializer<DoorStatus> {
  @override
  final Iterable<Type> types = const <Type>[DoorStatus];
  @override
  final String wireName = 'DoorStatus';

  @override
  Object serialize(Serializers serializers, DoorStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  DoorStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      DoorStatus.valueOf(serialized as String);
}

class _$ShockStatusSerializer implements PrimitiveSerializer<ShockStatus> {
  @override
  final Iterable<Type> types = const <Type>[ShockStatus];
  @override
  final String wireName = 'ShockStatus';

  @override
  Object serialize(Serializers serializers, ShockStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  ShockStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ShockStatus.valueOf(serialized as String);
}

class _$MotionStatusSerializer implements PrimitiveSerializer<MotionStatus> {
  @override
  final Iterable<Type> types = const <Type>[MotionStatus];
  @override
  final String wireName = 'MotionStatus';

  @override
  Object serialize(Serializers serializers, MotionStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  MotionStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      MotionStatus.valueOf(serialized as String);
}

class _$RolloverStatusSerializer
    implements PrimitiveSerializer<RolloverStatus> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'clear': 'clear',
    'detected': 'detected',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'clear': 'clear',
    'detected': 'detected',
  };

  @override
  final Iterable<Type> types = const <Type>[RolloverStatus];
  @override
  final String wireName = 'RolloverStatus';

  @override
  Object serialize(Serializers serializers, RolloverStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  RolloverStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      RolloverStatus.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$Beacon extends Beacon {
  @override
  final String identifier;
  @override
  final String? serialNumber;
  @override
  final String? macAddress;
  @override
  final String? iosPeripheralId;
  @override
  final String? name;
  @override
  final BeaconStatus? status;
  @override
  final String? iconName;
  @override
  final int? major;
  @override
  final int? minor;
  @override
  final Product product;
  @override
  final DoorInfo? lastDoor;
  @override
  final LocationInfo? lastLocation;
  @override
  final WaterLeakInfo? lastWaterLeak;
  @override
  final TotalOdometerInfo? lastTotalOdometer;
  @override
  final Co2Info? lastCo2;
  @override
  final TemperatureInfo? lastTemperature;
  @override
  final HumidityInfo? lastHumidity;
  @override
  final RolloverInfo? lastRollover;
  @override
  final MotionInfo? lastMotion;
  @override
  final ShockInfo? lastShock;
  @override
  final BeaconState? state;
  @override
  final LastBattery? lastBattery;
  @override
  final String? imageUrl;
  @override
  final String? thumbnailUrl;
  @override
  final Subscription? lastSubscription;
  @override
  final BeaconConfiguration? configuration;
  @override
  final String? softwareVersion;
  @override
  final Organization? organization;

  factory _$Beacon([void Function(BeaconBuilder)? updates]) =>
      (new BeaconBuilder()..update(updates))._build();

  _$Beacon._(
      {required this.identifier,
      this.serialNumber,
      this.macAddress,
      this.iosPeripheralId,
      this.name,
      this.status,
      this.iconName,
      this.major,
      this.minor,
      required this.product,
      this.lastDoor,
      this.lastLocation,
      this.lastWaterLeak,
      this.lastTotalOdometer,
      this.lastCo2,
      this.lastTemperature,
      this.lastHumidity,
      this.lastRollover,
      this.lastMotion,
      this.lastShock,
      this.state,
      this.lastBattery,
      this.imageUrl,
      this.thumbnailUrl,
      this.lastSubscription,
      this.configuration,
      this.softwareVersion,
      this.organization})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(identifier, r'Beacon', 'identifier');
    BuiltValueNullFieldError.checkNotNull(product, r'Beacon', 'product');
  }

  @override
  Beacon rebuild(void Function(BeaconBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconBuilder toBuilder() => new BeaconBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Beacon &&
        identifier == other.identifier &&
        serialNumber == other.serialNumber &&
        macAddress == other.macAddress &&
        iosPeripheralId == other.iosPeripheralId &&
        name == other.name &&
        status == other.status &&
        iconName == other.iconName &&
        major == other.major &&
        minor == other.minor &&
        product == other.product &&
        lastDoor == other.lastDoor &&
        lastLocation == other.lastLocation &&
        lastWaterLeak == other.lastWaterLeak &&
        lastTotalOdometer == other.lastTotalOdometer &&
        lastCo2 == other.lastCo2 &&
        lastTemperature == other.lastTemperature &&
        lastHumidity == other.lastHumidity &&
        lastRollover == other.lastRollover &&
        lastMotion == other.lastMotion &&
        lastShock == other.lastShock &&
        state == other.state &&
        lastBattery == other.lastBattery &&
        imageUrl == other.imageUrl &&
        thumbnailUrl == other.thumbnailUrl &&
        lastSubscription == other.lastSubscription &&
        configuration == other.configuration &&
        softwareVersion == other.softwareVersion &&
        organization == other.organization;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, identifier.hashCode);
    _$hash = $jc(_$hash, serialNumber.hashCode);
    _$hash = $jc(_$hash, macAddress.hashCode);
    _$hash = $jc(_$hash, iosPeripheralId.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, iconName.hashCode);
    _$hash = $jc(_$hash, major.hashCode);
    _$hash = $jc(_$hash, minor.hashCode);
    _$hash = $jc(_$hash, product.hashCode);
    _$hash = $jc(_$hash, lastDoor.hashCode);
    _$hash = $jc(_$hash, lastLocation.hashCode);
    _$hash = $jc(_$hash, lastWaterLeak.hashCode);
    _$hash = $jc(_$hash, lastTotalOdometer.hashCode);
    _$hash = $jc(_$hash, lastCo2.hashCode);
    _$hash = $jc(_$hash, lastTemperature.hashCode);
    _$hash = $jc(_$hash, lastHumidity.hashCode);
    _$hash = $jc(_$hash, lastRollover.hashCode);
    _$hash = $jc(_$hash, lastMotion.hashCode);
    _$hash = $jc(_$hash, lastShock.hashCode);
    _$hash = $jc(_$hash, state.hashCode);
    _$hash = $jc(_$hash, lastBattery.hashCode);
    _$hash = $jc(_$hash, imageUrl.hashCode);
    _$hash = $jc(_$hash, thumbnailUrl.hashCode);
    _$hash = $jc(_$hash, lastSubscription.hashCode);
    _$hash = $jc(_$hash, configuration.hashCode);
    _$hash = $jc(_$hash, softwareVersion.hashCode);
    _$hash = $jc(_$hash, organization.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class BeaconBuilder implements Builder<Beacon, BeaconBuilder> {
  _$Beacon? _$v;

  String? _identifier;
  String? get identifier => _$this._identifier;
  set identifier(String? identifier) => _$this._identifier = identifier;

  String? _serialNumber;
  String? get serialNumber => _$this._serialNumber;
  set serialNumber(String? serialNumber) => _$this._serialNumber = serialNumber;

  String? _macAddress;
  String? get macAddress => _$this._macAddress;
  set macAddress(String? macAddress) => _$this._macAddress = macAddress;

  String? _iosPeripheralId;
  String? get iosPeripheralId => _$this._iosPeripheralId;
  set iosPeripheralId(String? iosPeripheralId) =>
      _$this._iosPeripheralId = iosPeripheralId;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  BeaconStatus? _status;
  BeaconStatus? get status => _$this._status;
  set status(BeaconStatus? status) => _$this._status = status;

  String? _iconName;
  String? get iconName => _$this._iconName;
  set iconName(String? iconName) => _$this._iconName = iconName;

  int? _major;
  int? get major => _$this._major;
  set major(int? major) => _$this._major = major;

  int? _minor;
  int? get minor => _$this._minor;
  set minor(int? minor) => _$this._minor = minor;

  ProductBuilder? _product;
  ProductBuilder get product => _$this._product ??= new ProductBuilder();
  set product(ProductBuilder? product) => _$this._product = product;

  DoorInfoBuilder? _lastDoor;
  DoorInfoBuilder get lastDoor => _$this._lastDoor ??= new DoorInfoBuilder();
  set lastDoor(DoorInfoBuilder? lastDoor) => _$this._lastDoor = lastDoor;

  LocationInfoBuilder? _lastLocation;
  LocationInfoBuilder get lastLocation =>
      _$this._lastLocation ??= new LocationInfoBuilder();
  set lastLocation(LocationInfoBuilder? lastLocation) =>
      _$this._lastLocation = lastLocation;

  WaterLeakInfoBuilder? _lastWaterLeak;
  WaterLeakInfoBuilder get lastWaterLeak =>
      _$this._lastWaterLeak ??= new WaterLeakInfoBuilder();
  set lastWaterLeak(WaterLeakInfoBuilder? lastWaterLeak) =>
      _$this._lastWaterLeak = lastWaterLeak;

  TotalOdometerInfoBuilder? _lastTotalOdometer;
  TotalOdometerInfoBuilder get lastTotalOdometer =>
      _$this._lastTotalOdometer ??= new TotalOdometerInfoBuilder();
  set lastTotalOdometer(TotalOdometerInfoBuilder? lastTotalOdometer) =>
      _$this._lastTotalOdometer = lastTotalOdometer;

  Co2InfoBuilder? _lastCo2;
  Co2InfoBuilder get lastCo2 => _$this._lastCo2 ??= new Co2InfoBuilder();
  set lastCo2(Co2InfoBuilder? lastCo2) => _$this._lastCo2 = lastCo2;

  TemperatureInfoBuilder? _lastTemperature;
  TemperatureInfoBuilder get lastTemperature =>
      _$this._lastTemperature ??= new TemperatureInfoBuilder();
  set lastTemperature(TemperatureInfoBuilder? lastTemperature) =>
      _$this._lastTemperature = lastTemperature;

  HumidityInfoBuilder? _lastHumidity;
  HumidityInfoBuilder get lastHumidity =>
      _$this._lastHumidity ??= new HumidityInfoBuilder();
  set lastHumidity(HumidityInfoBuilder? lastHumidity) =>
      _$this._lastHumidity = lastHumidity;

  RolloverInfoBuilder? _lastRollover;
  RolloverInfoBuilder get lastRollover =>
      _$this._lastRollover ??= new RolloverInfoBuilder();
  set lastRollover(RolloverInfoBuilder? lastRollover) =>
      _$this._lastRollover = lastRollover;

  MotionInfoBuilder? _lastMotion;
  MotionInfoBuilder get lastMotion =>
      _$this._lastMotion ??= new MotionInfoBuilder();
  set lastMotion(MotionInfoBuilder? lastMotion) =>
      _$this._lastMotion = lastMotion;

  ShockInfoBuilder? _lastShock;
  ShockInfoBuilder get lastShock =>
      _$this._lastShock ??= new ShockInfoBuilder();
  set lastShock(ShockInfoBuilder? lastShock) => _$this._lastShock = lastShock;

  BeaconState? _state;
  BeaconState? get state => _$this._state;
  set state(BeaconState? state) => _$this._state = state;

  LastBatteryBuilder? _lastBattery;
  LastBatteryBuilder get lastBattery =>
      _$this._lastBattery ??= new LastBatteryBuilder();
  set lastBattery(LastBatteryBuilder? lastBattery) =>
      _$this._lastBattery = lastBattery;

  String? _imageUrl;
  String? get imageUrl => _$this._imageUrl;
  set imageUrl(String? imageUrl) => _$this._imageUrl = imageUrl;

  String? _thumbnailUrl;
  String? get thumbnailUrl => _$this._thumbnailUrl;
  set thumbnailUrl(String? thumbnailUrl) => _$this._thumbnailUrl = thumbnailUrl;

  SubscriptionBuilder? _lastSubscription;
  SubscriptionBuilder get lastSubscription =>
      _$this._lastSubscription ??= new SubscriptionBuilder();
  set lastSubscription(SubscriptionBuilder? lastSubscription) =>
      _$this._lastSubscription = lastSubscription;

  BeaconConfigurationBuilder? _configuration;
  BeaconConfigurationBuilder get configuration =>
      _$this._configuration ??= new BeaconConfigurationBuilder();
  set configuration(BeaconConfigurationBuilder? configuration) =>
      _$this._configuration = configuration;

  String? _softwareVersion;
  String? get softwareVersion => _$this._softwareVersion;
  set softwareVersion(String? softwareVersion) =>
      _$this._softwareVersion = softwareVersion;

  OrganizationBuilder? _organization;
  OrganizationBuilder get organization =>
      _$this._organization ??= new OrganizationBuilder();
  set organization(OrganizationBuilder? organization) =>
      _$this._organization = organization;

  BeaconBuilder();

  BeaconBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _identifier = $v.identifier;
      _serialNumber = $v.serialNumber;
      _macAddress = $v.macAddress;
      _iosPeripheralId = $v.iosPeripheralId;
      _name = $v.name;
      _status = $v.status;
      _iconName = $v.iconName;
      _major = $v.major;
      _minor = $v.minor;
      _product = $v.product.toBuilder();
      _lastDoor = $v.lastDoor?.toBuilder();
      _lastLocation = $v.lastLocation?.toBuilder();
      _lastWaterLeak = $v.lastWaterLeak?.toBuilder();
      _lastTotalOdometer = $v.lastTotalOdometer?.toBuilder();
      _lastCo2 = $v.lastCo2?.toBuilder();
      _lastTemperature = $v.lastTemperature?.toBuilder();
      _lastHumidity = $v.lastHumidity?.toBuilder();
      _lastRollover = $v.lastRollover?.toBuilder();
      _lastMotion = $v.lastMotion?.toBuilder();
      _lastShock = $v.lastShock?.toBuilder();
      _state = $v.state;
      _lastBattery = $v.lastBattery?.toBuilder();
      _imageUrl = $v.imageUrl;
      _thumbnailUrl = $v.thumbnailUrl;
      _lastSubscription = $v.lastSubscription?.toBuilder();
      _configuration = $v.configuration?.toBuilder();
      _softwareVersion = $v.softwareVersion;
      _organization = $v.organization?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Beacon other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Beacon;
  }

  @override
  void update(void Function(BeaconBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Beacon build() => _build();

  _$Beacon _build() {
    _$Beacon _$result;
    try {
      _$result = _$v ??
          new _$Beacon._(
              identifier: BuiltValueNullFieldError.checkNotNull(
                  identifier, r'Beacon', 'identifier'),
              serialNumber: serialNumber,
              macAddress: macAddress,
              iosPeripheralId: iosPeripheralId,
              name: name,
              status: status,
              iconName: iconName,
              major: major,
              minor: minor,
              product: product.build(),
              lastDoor: _lastDoor?.build(),
              lastLocation: _lastLocation?.build(),
              lastWaterLeak: _lastWaterLeak?.build(),
              lastTotalOdometer: _lastTotalOdometer?.build(),
              lastCo2: _lastCo2?.build(),
              lastTemperature: _lastTemperature?.build(),
              lastHumidity: _lastHumidity?.build(),
              lastRollover: _lastRollover?.build(),
              lastMotion: _lastMotion?.build(),
              lastShock: _lastShock?.build(),
              state: state,
              lastBattery: _lastBattery?.build(),
              imageUrl: imageUrl,
              thumbnailUrl: thumbnailUrl,
              lastSubscription: _lastSubscription?.build(),
              configuration: _configuration?.build(),
              softwareVersion: softwareVersion,
              organization: _organization?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'product';
        product.build();
        _$failedField = 'lastDoor';
        _lastDoor?.build();
        _$failedField = 'lastLocation';
        _lastLocation?.build();
        _$failedField = 'lastWaterLeak';
        _lastWaterLeak?.build();
        _$failedField = 'lastTotalOdometer';
        _lastTotalOdometer?.build();
        _$failedField = 'lastCo2';
        _lastCo2?.build();
        _$failedField = 'lastTemperature';
        _lastTemperature?.build();
        _$failedField = 'lastHumidity';
        _lastHumidity?.build();
        _$failedField = 'lastRollover';
        _lastRollover?.build();
        _$failedField = 'lastMotion';
        _lastMotion?.build();
        _$failedField = 'lastShock';
        _lastShock?.build();

        _$failedField = 'lastBattery';
        _lastBattery?.build();

        _$failedField = 'lastSubscription';
        _lastSubscription?.build();
        _$failedField = 'configuration';
        _configuration?.build();

        _$failedField = 'organization';
        _organization?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Beacon', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$ButtonEvent extends ButtonEvent {
  @override
  final DateTime timestamp;

  factory _$ButtonEvent([void Function(ButtonEventBuilder)? updates]) =>
      (new ButtonEventBuilder()..update(updates))._build();

  _$ButtonEvent._({required this.timestamp}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'ButtonEvent', 'timestamp');
  }

  @override
  ButtonEvent rebuild(void Function(ButtonEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ButtonEventBuilder toBuilder() => new ButtonEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ButtonEvent && timestamp == other.timestamp;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ButtonEvent')
          ..add('timestamp', timestamp))
        .toString();
  }
}

class ButtonEventBuilder implements Builder<ButtonEvent, ButtonEventBuilder> {
  _$ButtonEvent? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  ButtonEventBuilder();

  ButtonEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ButtonEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ButtonEvent;
  }

  @override
  void update(void Function(ButtonEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ButtonEvent build() => _build();

  _$ButtonEvent _build() {
    final _$result = _$v ??
        new _$ButtonEvent._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'ButtonEvent', 'timestamp'));
    replace(_$result);
    return _$result;
  }
}

class _$MotionInfo extends MotionInfo {
  @override
  final DateTime? timestamp;
  @override
  final MotionStatus? status;

  factory _$MotionInfo([void Function(MotionInfoBuilder)? updates]) =>
      (new MotionInfoBuilder()..update(updates))._build();

  _$MotionInfo._({this.timestamp, this.status}) : super._();

  @override
  MotionInfo rebuild(void Function(MotionInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MotionInfoBuilder toBuilder() => new MotionInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MotionInfo &&
        timestamp == other.timestamp &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'MotionInfo')
          ..add('timestamp', timestamp)
          ..add('status', status))
        .toString();
  }
}

class MotionInfoBuilder implements Builder<MotionInfo, MotionInfoBuilder> {
  _$MotionInfo? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  MotionStatus? _status;
  MotionStatus? get status => _$this._status;
  set status(MotionStatus? status) => _$this._status = status;

  MotionInfoBuilder();

  MotionInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MotionInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MotionInfo;
  }

  @override
  void update(void Function(MotionInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  MotionInfo build() => _build();

  _$MotionInfo _build() {
    final _$result =
        _$v ?? new _$MotionInfo._(timestamp: timestamp, status: status);
    replace(_$result);
    return _$result;
  }
}

class _$ShockInfo extends ShockInfo {
  @override
  final DateTime? timestamp;
  @override
  final ShockStatus? status;

  factory _$ShockInfo([void Function(ShockInfoBuilder)? updates]) =>
      (new ShockInfoBuilder()..update(updates))._build();

  _$ShockInfo._({this.timestamp, this.status}) : super._();

  @override
  ShockInfo rebuild(void Function(ShockInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ShockInfoBuilder toBuilder() => new ShockInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ShockInfo &&
        timestamp == other.timestamp &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ShockInfo')
          ..add('timestamp', timestamp)
          ..add('status', status))
        .toString();
  }
}

class ShockInfoBuilder implements Builder<ShockInfo, ShockInfoBuilder> {
  _$ShockInfo? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  ShockStatus? _status;
  ShockStatus? get status => _$this._status;
  set status(ShockStatus? status) => _$this._status = status;

  ShockInfoBuilder();

  ShockInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ShockInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ShockInfo;
  }

  @override
  void update(void Function(ShockInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ShockInfo build() => _build();

  _$ShockInfo _build() {
    final _$result =
        _$v ?? new _$ShockInfo._(timestamp: timestamp, status: status);
    replace(_$result);
    return _$result;
  }
}

class _$RolloverInfo extends RolloverInfo {
  @override
  final DateTime? timestamp;
  @override
  final RolloverStatus? status;

  factory _$RolloverInfo([void Function(RolloverInfoBuilder)? updates]) =>
      (new RolloverInfoBuilder()..update(updates))._build();

  _$RolloverInfo._({this.timestamp, this.status}) : super._();

  @override
  RolloverInfo rebuild(void Function(RolloverInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RolloverInfoBuilder toBuilder() => new RolloverInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RolloverInfo &&
        timestamp == other.timestamp &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'RolloverInfo')
          ..add('timestamp', timestamp)
          ..add('status', status))
        .toString();
  }
}

class RolloverInfoBuilder
    implements Builder<RolloverInfo, RolloverInfoBuilder> {
  _$RolloverInfo? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  RolloverStatus? _status;
  RolloverStatus? get status => _$this._status;
  set status(RolloverStatus? status) => _$this._status = status;

  RolloverInfoBuilder();

  RolloverInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RolloverInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RolloverInfo;
  }

  @override
  void update(void Function(RolloverInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  RolloverInfo build() => _build();

  _$RolloverInfo _build() {
    final _$result =
        _$v ?? new _$RolloverInfo._(timestamp: timestamp, status: status);
    replace(_$result);
    return _$result;
  }
}

class _$DoorInfo extends DoorInfo {
  @override
  final DateTime? timestamp;
  @override
  final DoorStatus? status;

  factory _$DoorInfo([void Function(DoorInfoBuilder)? updates]) =>
      (new DoorInfoBuilder()..update(updates))._build();

  _$DoorInfo._({this.timestamp, this.status}) : super._();

  @override
  DoorInfo rebuild(void Function(DoorInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DoorInfoBuilder toBuilder() => new DoorInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DoorInfo &&
        timestamp == other.timestamp &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'DoorInfo')
          ..add('timestamp', timestamp)
          ..add('status', status))
        .toString();
  }
}

class DoorInfoBuilder implements Builder<DoorInfo, DoorInfoBuilder> {
  _$DoorInfo? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  DoorStatus? _status;
  DoorStatus? get status => _$this._status;
  set status(DoorStatus? status) => _$this._status = status;

  DoorInfoBuilder();

  DoorInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DoorInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$DoorInfo;
  }

  @override
  void update(void Function(DoorInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  DoorInfo build() => _build();

  _$DoorInfo _build() {
    final _$result =
        _$v ?? new _$DoorInfo._(timestamp: timestamp, status: status);
    replace(_$result);
    return _$result;
  }
}

class _$Co2Info extends Co2Info {
  @override
  final DateTime? timestamp;
  @override
  final int? value;

  factory _$Co2Info([void Function(Co2InfoBuilder)? updates]) =>
      (new Co2InfoBuilder()..update(updates))._build();

  _$Co2Info._({this.timestamp, this.value}) : super._();

  @override
  Co2Info rebuild(void Function(Co2InfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  Co2InfoBuilder toBuilder() => new Co2InfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Co2Info &&
        timestamp == other.timestamp &&
        value == other.value;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Co2Info')
          ..add('timestamp', timestamp)
          ..add('value', value))
        .toString();
  }
}

class Co2InfoBuilder implements Builder<Co2Info, Co2InfoBuilder> {
  _$Co2Info? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  int? _value;
  int? get value => _$this._value;
  set value(int? value) => _$this._value = value;

  Co2InfoBuilder();

  Co2InfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Co2Info other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Co2Info;
  }

  @override
  void update(void Function(Co2InfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Co2Info build() => _build();

  _$Co2Info _build() {
    final _$result = _$v ?? new _$Co2Info._(timestamp: timestamp, value: value);
    replace(_$result);
    return _$result;
  }
}

class _$HumidityInfo extends HumidityInfo {
  @override
  final DateTime? timestamp;
  @override
  final double? value;

  factory _$HumidityInfo([void Function(HumidityInfoBuilder)? updates]) =>
      (new HumidityInfoBuilder()..update(updates))._build();

  _$HumidityInfo._({this.timestamp, this.value}) : super._();

  @override
  HumidityInfo rebuild(void Function(HumidityInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HumidityInfoBuilder toBuilder() => new HumidityInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HumidityInfo &&
        timestamp == other.timestamp &&
        value == other.value;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'HumidityInfo')
          ..add('timestamp', timestamp)
          ..add('value', value))
        .toString();
  }
}

class HumidityInfoBuilder
    implements Builder<HumidityInfo, HumidityInfoBuilder> {
  _$HumidityInfo? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  double? _value;
  double? get value => _$this._value;
  set value(double? value) => _$this._value = value;

  HumidityInfoBuilder();

  HumidityInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HumidityInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HumidityInfo;
  }

  @override
  void update(void Function(HumidityInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  HumidityInfo build() => _build();

  _$HumidityInfo _build() {
    final _$result =
        _$v ?? new _$HumidityInfo._(timestamp: timestamp, value: value);
    replace(_$result);
    return _$result;
  }
}

class _$TemperatureInfo extends TemperatureInfo {
  @override
  final DateTime? timestamp;
  @override
  final double? value;
  @override
  final TemperatureUnit? unit;

  factory _$TemperatureInfo([void Function(TemperatureInfoBuilder)? updates]) =>
      (new TemperatureInfoBuilder()..update(updates))._build();

  _$TemperatureInfo._({this.timestamp, this.value, this.unit}) : super._();

  @override
  TemperatureInfo rebuild(void Function(TemperatureInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TemperatureInfoBuilder toBuilder() =>
      new TemperatureInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TemperatureInfo &&
        timestamp == other.timestamp &&
        value == other.value &&
        unit == other.unit;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jc(_$hash, unit.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'TemperatureInfo')
          ..add('timestamp', timestamp)
          ..add('value', value)
          ..add('unit', unit))
        .toString();
  }
}

class TemperatureInfoBuilder
    implements Builder<TemperatureInfo, TemperatureInfoBuilder> {
  _$TemperatureInfo? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  double? _value;
  double? get value => _$this._value;
  set value(double? value) => _$this._value = value;

  TemperatureUnit? _unit;
  TemperatureUnit? get unit => _$this._unit;
  set unit(TemperatureUnit? unit) => _$this._unit = unit;

  TemperatureInfoBuilder();

  TemperatureInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _value = $v.value;
      _unit = $v.unit;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TemperatureInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TemperatureInfo;
  }

  @override
  void update(void Function(TemperatureInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  TemperatureInfo build() => _build();

  _$TemperatureInfo _build() {
    final _$result = _$v ??
        new _$TemperatureInfo._(timestamp: timestamp, value: value, unit: unit);
    replace(_$result);
    return _$result;
  }
}

class _$TotalOdometerInfo extends TotalOdometerInfo {
  @override
  final DateTime? timestamp;
  @override
  final int? value;

  factory _$TotalOdometerInfo(
          [void Function(TotalOdometerInfoBuilder)? updates]) =>
      (new TotalOdometerInfoBuilder()..update(updates))._build();

  _$TotalOdometerInfo._({this.timestamp, this.value}) : super._();

  @override
  TotalOdometerInfo rebuild(void Function(TotalOdometerInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TotalOdometerInfoBuilder toBuilder() =>
      new TotalOdometerInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TotalOdometerInfo &&
        timestamp == other.timestamp &&
        value == other.value;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'TotalOdometerInfo')
          ..add('timestamp', timestamp)
          ..add('value', value))
        .toString();
  }
}

class TotalOdometerInfoBuilder
    implements Builder<TotalOdometerInfo, TotalOdometerInfoBuilder> {
  _$TotalOdometerInfo? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  int? _value;
  int? get value => _$this._value;
  set value(int? value) => _$this._value = value;

  TotalOdometerInfoBuilder();

  TotalOdometerInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TotalOdometerInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TotalOdometerInfo;
  }

  @override
  void update(void Function(TotalOdometerInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  TotalOdometerInfo build() => _build();

  _$TotalOdometerInfo _build() {
    final _$result =
        _$v ?? new _$TotalOdometerInfo._(timestamp: timestamp, value: value);
    replace(_$result);
    return _$result;
  }
}

class _$WaterLeakInfo extends WaterLeakInfo {
  @override
  final DateTime? timestamp;
  @override
  final WaterLeakStatus? status;

  factory _$WaterLeakInfo([void Function(WaterLeakInfoBuilder)? updates]) =>
      (new WaterLeakInfoBuilder()..update(updates))._build();

  _$WaterLeakInfo._({this.timestamp, this.status}) : super._();

  @override
  WaterLeakInfo rebuild(void Function(WaterLeakInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WaterLeakInfoBuilder toBuilder() => new WaterLeakInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WaterLeakInfo &&
        timestamp == other.timestamp &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'WaterLeakInfo')
          ..add('timestamp', timestamp)
          ..add('status', status))
        .toString();
  }
}

class WaterLeakInfoBuilder
    implements Builder<WaterLeakInfo, WaterLeakInfoBuilder> {
  _$WaterLeakInfo? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  WaterLeakStatus? _status;
  WaterLeakStatus? get status => _$this._status;
  set status(WaterLeakStatus? status) => _$this._status = status;

  WaterLeakInfoBuilder();

  WaterLeakInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WaterLeakInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$WaterLeakInfo;
  }

  @override
  void update(void Function(WaterLeakInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  WaterLeakInfo build() => _build();

  _$WaterLeakInfo _build() {
    final _$result =
        _$v ?? new _$WaterLeakInfo._(timestamp: timestamp, status: status);
    replace(_$result);
    return _$result;
  }
}

class _$LocationInfo extends LocationInfo {
  @override
  final DateTime timestamp;
  @override
  final double latitude;
  @override
  final double longitude;
  @override
  final double? altitude;
  @override
  final double? speed;
  @override
  final int? satellites;
  @override
  final String? formattedAddress;
  @override
  final bool? isButtonEventDoubleClick;

  factory _$LocationInfo([void Function(LocationInfoBuilder)? updates]) =>
      (new LocationInfoBuilder()..update(updates))._build();

  _$LocationInfo._(
      {required this.timestamp,
      required this.latitude,
      required this.longitude,
      this.altitude,
      this.speed,
      this.satellites,
      this.formattedAddress,
      this.isButtonEventDoubleClick})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'LocationInfo', 'timestamp');
    BuiltValueNullFieldError.checkNotNull(
        latitude, r'LocationInfo', 'latitude');
    BuiltValueNullFieldError.checkNotNull(
        longitude, r'LocationInfo', 'longitude');
  }

  @override
  LocationInfo rebuild(void Function(LocationInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LocationInfoBuilder toBuilder() => new LocationInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LocationInfo &&
        timestamp == other.timestamp &&
        latitude == other.latitude &&
        longitude == other.longitude &&
        altitude == other.altitude &&
        speed == other.speed &&
        satellites == other.satellites &&
        formattedAddress == other.formattedAddress &&
        isButtonEventDoubleClick == other.isButtonEventDoubleClick;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, latitude.hashCode);
    _$hash = $jc(_$hash, longitude.hashCode);
    _$hash = $jc(_$hash, altitude.hashCode);
    _$hash = $jc(_$hash, speed.hashCode);
    _$hash = $jc(_$hash, satellites.hashCode);
    _$hash = $jc(_$hash, formattedAddress.hashCode);
    _$hash = $jc(_$hash, isButtonEventDoubleClick.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LocationInfo')
          ..add('timestamp', timestamp)
          ..add('latitude', latitude)
          ..add('longitude', longitude)
          ..add('altitude', altitude)
          ..add('speed', speed)
          ..add('satellites', satellites)
          ..add('formattedAddress', formattedAddress)
          ..add('isButtonEventDoubleClick', isButtonEventDoubleClick))
        .toString();
  }
}

class LocationInfoBuilder
    implements Builder<LocationInfo, LocationInfoBuilder> {
  _$LocationInfo? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(double? latitude) => _$this._latitude = latitude;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(double? longitude) => _$this._longitude = longitude;

  double? _altitude;
  double? get altitude => _$this._altitude;
  set altitude(double? altitude) => _$this._altitude = altitude;

  double? _speed;
  double? get speed => _$this._speed;
  set speed(double? speed) => _$this._speed = speed;

  int? _satellites;
  int? get satellites => _$this._satellites;
  set satellites(int? satellites) => _$this._satellites = satellites;

  String? _formattedAddress;
  String? get formattedAddress => _$this._formattedAddress;
  set formattedAddress(String? formattedAddress) =>
      _$this._formattedAddress = formattedAddress;

  bool? _isButtonEventDoubleClick;
  bool? get isButtonEventDoubleClick => _$this._isButtonEventDoubleClick;
  set isButtonEventDoubleClick(bool? isButtonEventDoubleClick) =>
      _$this._isButtonEventDoubleClick = isButtonEventDoubleClick;

  LocationInfoBuilder();

  LocationInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _altitude = $v.altitude;
      _speed = $v.speed;
      _satellites = $v.satellites;
      _formattedAddress = $v.formattedAddress;
      _isButtonEventDoubleClick = $v.isButtonEventDoubleClick;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LocationInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LocationInfo;
  }

  @override
  void update(void Function(LocationInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LocationInfo build() => _build();

  _$LocationInfo _build() {
    final _$result = _$v ??
        new _$LocationInfo._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'LocationInfo', 'timestamp'),
            latitude: BuiltValueNullFieldError.checkNotNull(
                latitude, r'LocationInfo', 'latitude'),
            longitude: BuiltValueNullFieldError.checkNotNull(
                longitude, r'LocationInfo', 'longitude'),
            altitude: altitude,
            speed: speed,
            satellites: satellites,
            formattedAddress: formattedAddress,
            isButtonEventDoubleClick: isButtonEventDoubleClick);
    replace(_$result);
    return _$result;
  }
}

class _$LastBattery extends LastBattery {
  @override
  final DateTime timestamp;
  @override
  final double? value;
  @override
  final BatteryUnit? unit;
  @override
  final BatteryStatus? status;

  factory _$LastBattery([void Function(LastBatteryBuilder)? updates]) =>
      (new LastBatteryBuilder()..update(updates))._build();

  _$LastBattery._({required this.timestamp, this.value, this.unit, this.status})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        timestamp, r'LastBattery', 'timestamp');
  }

  @override
  LastBattery rebuild(void Function(LastBatteryBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LastBatteryBuilder toBuilder() => new LastBatteryBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LastBattery &&
        timestamp == other.timestamp &&
        value == other.value &&
        unit == other.unit &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, value.hashCode);
    _$hash = $jc(_$hash, unit.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LastBattery')
          ..add('timestamp', timestamp)
          ..add('value', value)
          ..add('unit', unit)
          ..add('status', status))
        .toString();
  }
}

class LastBatteryBuilder implements Builder<LastBattery, LastBatteryBuilder> {
  _$LastBattery? _$v;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  double? _value;
  double? get value => _$this._value;
  set value(double? value) => _$this._value = value;

  BatteryUnit? _unit;
  BatteryUnit? get unit => _$this._unit;
  set unit(BatteryUnit? unit) => _$this._unit = unit;

  BatteryStatus? _status;
  BatteryStatus? get status => _$this._status;
  set status(BatteryStatus? status) => _$this._status = status;

  LastBatteryBuilder();

  LastBatteryBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _timestamp = $v.timestamp;
      _value = $v.value;
      _unit = $v.unit;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LastBattery other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LastBattery;
  }

  @override
  void update(void Function(LastBatteryBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LastBattery build() => _build();

  _$LastBattery _build() {
    final _$result = _$v ??
        new _$LastBattery._(
            timestamp: BuiltValueNullFieldError.checkNotNull(
                timestamp, r'LastBattery', 'timestamp'),
            value: value,
            unit: unit,
            status: status);
    replace(_$result);
    return _$result;
  }
}

class _$Product extends Product {
  @override
  final String name;
  @override
  final String? imageUrl;
  @override
  final String marketingName;

  factory _$Product([void Function(ProductBuilder)? updates]) =>
      (new ProductBuilder()..update(updates))._build();

  _$Product._({required this.name, this.imageUrl, required this.marketingName})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'Product', 'name');
    BuiltValueNullFieldError.checkNotNull(
        marketingName, r'Product', 'marketingName');
  }

  @override
  Product rebuild(void Function(ProductBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductBuilder toBuilder() => new ProductBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Product &&
        name == other.name &&
        imageUrl == other.imageUrl &&
        marketingName == other.marketingName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, imageUrl.hashCode);
    _$hash = $jc(_$hash, marketingName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class ProductBuilder implements Builder<Product, ProductBuilder> {
  _$Product? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _imageUrl;
  String? get imageUrl => _$this._imageUrl;
  set imageUrl(String? imageUrl) => _$this._imageUrl = imageUrl;

  String? _marketingName;
  String? get marketingName => _$this._marketingName;
  set marketingName(String? marketingName) =>
      _$this._marketingName = marketingName;

  ProductBuilder();

  ProductBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _imageUrl = $v.imageUrl;
      _marketingName = $v.marketingName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Product other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Product;
  }

  @override
  void update(void Function(ProductBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Product build() => _build();

  _$Product _build() {
    final _$result = _$v ??
        new _$Product._(
            name:
                BuiltValueNullFieldError.checkNotNull(name, r'Product', 'name'),
            imageUrl: imageUrl,
            marketingName: BuiltValueNullFieldError.checkNotNull(
                marketingName, r'Product', 'marketingName'));
    replace(_$result);
    return _$result;
  }
}

class _$Subscription extends Subscription {
  @override
  final String? subscriptionId;
  @override
  final String? deviceId;
  @override
  final String? voucherId;
  @override
  final DateTime startDate;
  @override
  final DateTime endDate;
  @override
  final SubscriptionStatus status;
  @override
  final Plan? plan;

  factory _$Subscription([void Function(SubscriptionBuilder)? updates]) =>
      (new SubscriptionBuilder()..update(updates))._build();

  _$Subscription._(
      {this.subscriptionId,
      this.deviceId,
      this.voucherId,
      required this.startDate,
      required this.endDate,
      required this.status,
      this.plan})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        startDate, r'Subscription', 'startDate');
    BuiltValueNullFieldError.checkNotNull(endDate, r'Subscription', 'endDate');
    BuiltValueNullFieldError.checkNotNull(status, r'Subscription', 'status');
  }

  @override
  Subscription rebuild(void Function(SubscriptionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SubscriptionBuilder toBuilder() => new SubscriptionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Subscription &&
        subscriptionId == other.subscriptionId &&
        deviceId == other.deviceId &&
        voucherId == other.voucherId &&
        startDate == other.startDate &&
        endDate == other.endDate &&
        status == other.status &&
        plan == other.plan;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, subscriptionId.hashCode);
    _$hash = $jc(_$hash, deviceId.hashCode);
    _$hash = $jc(_$hash, voucherId.hashCode);
    _$hash = $jc(_$hash, startDate.hashCode);
    _$hash = $jc(_$hash, endDate.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, plan.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class SubscriptionBuilder
    implements Builder<Subscription, SubscriptionBuilder> {
  _$Subscription? _$v;

  String? _subscriptionId;
  String? get subscriptionId => _$this._subscriptionId;
  set subscriptionId(String? subscriptionId) =>
      _$this._subscriptionId = subscriptionId;

  String? _deviceId;
  String? get deviceId => _$this._deviceId;
  set deviceId(String? deviceId) => _$this._deviceId = deviceId;

  String? _voucherId;
  String? get voucherId => _$this._voucherId;
  set voucherId(String? voucherId) => _$this._voucherId = voucherId;

  DateTime? _startDate;
  DateTime? get startDate => _$this._startDate;
  set startDate(DateTime? startDate) => _$this._startDate = startDate;

  DateTime? _endDate;
  DateTime? get endDate => _$this._endDate;
  set endDate(DateTime? endDate) => _$this._endDate = endDate;

  SubscriptionStatus? _status;
  SubscriptionStatus? get status => _$this._status;
  set status(SubscriptionStatus? status) => _$this._status = status;

  PlanBuilder? _plan;
  PlanBuilder get plan => _$this._plan ??= new PlanBuilder();
  set plan(PlanBuilder? plan) => _$this._plan = plan;

  SubscriptionBuilder();

  SubscriptionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _subscriptionId = $v.subscriptionId;
      _deviceId = $v.deviceId;
      _voucherId = $v.voucherId;
      _startDate = $v.startDate;
      _endDate = $v.endDate;
      _status = $v.status;
      _plan = $v.plan?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Subscription other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Subscription;
  }

  @override
  void update(void Function(SubscriptionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Subscription build() => _build();

  _$Subscription _build() {
    _$Subscription _$result;
    try {
      _$result = _$v ??
          new _$Subscription._(
              subscriptionId: subscriptionId,
              deviceId: deviceId,
              voucherId: voucherId,
              startDate: BuiltValueNullFieldError.checkNotNull(
                  startDate, r'Subscription', 'startDate'),
              endDate: BuiltValueNullFieldError.checkNotNull(
                  endDate, r'Subscription', 'endDate'),
              status: BuiltValueNullFieldError.checkNotNull(
                  status, r'Subscription', 'status'),
              plan: _plan?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'plan';
        _plan?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Subscription', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Plan extends Plan {
  @override
  final String planId;
  @override
  final String title;
  @override
  final String? description;
  @override
  final double amount;
  @override
  final double taxPercent;
  @override
  final String currency;
  @override
  final PlanType type;
  @override
  final String frequency;
  @override
  final int frequencyInterval;
  @override
  final String? operator;
  @override
  final int? trialPeriodDays;

  factory _$Plan([void Function(PlanBuilder)? updates]) =>
      (new PlanBuilder()..update(updates))._build();

  _$Plan._(
      {required this.planId,
      required this.title,
      this.description,
      required this.amount,
      required this.taxPercent,
      required this.currency,
      required this.type,
      required this.frequency,
      required this.frequencyInterval,
      this.operator,
      this.trialPeriodDays})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(planId, r'Plan', 'planId');
    BuiltValueNullFieldError.checkNotNull(title, r'Plan', 'title');
    BuiltValueNullFieldError.checkNotNull(amount, r'Plan', 'amount');
    BuiltValueNullFieldError.checkNotNull(taxPercent, r'Plan', 'taxPercent');
    BuiltValueNullFieldError.checkNotNull(currency, r'Plan', 'currency');
    BuiltValueNullFieldError.checkNotNull(type, r'Plan', 'type');
    BuiltValueNullFieldError.checkNotNull(frequency, r'Plan', 'frequency');
    BuiltValueNullFieldError.checkNotNull(
        frequencyInterval, r'Plan', 'frequencyInterval');
  }

  @override
  Plan rebuild(void Function(PlanBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlanBuilder toBuilder() => new PlanBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Plan &&
        planId == other.planId &&
        title == other.title &&
        description == other.description &&
        amount == other.amount &&
        taxPercent == other.taxPercent &&
        currency == other.currency &&
        type == other.type &&
        frequency == other.frequency &&
        frequencyInterval == other.frequencyInterval &&
        operator == other.operator &&
        trialPeriodDays == other.trialPeriodDays;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, planId.hashCode);
    _$hash = $jc(_$hash, title.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, amount.hashCode);
    _$hash = $jc(_$hash, taxPercent.hashCode);
    _$hash = $jc(_$hash, currency.hashCode);
    _$hash = $jc(_$hash, type.hashCode);
    _$hash = $jc(_$hash, frequency.hashCode);
    _$hash = $jc(_$hash, frequencyInterval.hashCode);
    _$hash = $jc(_$hash, operator.hashCode);
    _$hash = $jc(_$hash, trialPeriodDays.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class PlanBuilder implements Builder<Plan, PlanBuilder> {
  _$Plan? _$v;

  String? _planId;
  String? get planId => _$this._planId;
  set planId(String? planId) => _$this._planId = planId;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  double? _amount;
  double? get amount => _$this._amount;
  set amount(double? amount) => _$this._amount = amount;

  double? _taxPercent;
  double? get taxPercent => _$this._taxPercent;
  set taxPercent(double? taxPercent) => _$this._taxPercent = taxPercent;

  String? _currency;
  String? get currency => _$this._currency;
  set currency(String? currency) => _$this._currency = currency;

  PlanType? _type;
  PlanType? get type => _$this._type;
  set type(PlanType? type) => _$this._type = type;

  String? _frequency;
  String? get frequency => _$this._frequency;
  set frequency(String? frequency) => _$this._frequency = frequency;

  int? _frequencyInterval;
  int? get frequencyInterval => _$this._frequencyInterval;
  set frequencyInterval(int? frequencyInterval) =>
      _$this._frequencyInterval = frequencyInterval;

  String? _operator;
  String? get operator => _$this._operator;
  set operator(String? operator) => _$this._operator = operator;

  int? _trialPeriodDays;
  int? get trialPeriodDays => _$this._trialPeriodDays;
  set trialPeriodDays(int? trialPeriodDays) =>
      _$this._trialPeriodDays = trialPeriodDays;

  PlanBuilder();

  PlanBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _planId = $v.planId;
      _title = $v.title;
      _description = $v.description;
      _amount = $v.amount;
      _taxPercent = $v.taxPercent;
      _currency = $v.currency;
      _type = $v.type;
      _frequency = $v.frequency;
      _frequencyInterval = $v.frequencyInterval;
      _operator = $v.operator;
      _trialPeriodDays = $v.trialPeriodDays;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Plan other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Plan;
  }

  @override
  void update(void Function(PlanBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Plan build() => _build();

  _$Plan _build() {
    final _$result = _$v ??
        new _$Plan._(
            planId: BuiltValueNullFieldError.checkNotNull(
                planId, r'Plan', 'planId'),
            title:
                BuiltValueNullFieldError.checkNotNull(title, r'Plan', 'title'),
            description: description,
            amount: BuiltValueNullFieldError.checkNotNull(
                amount, r'Plan', 'amount'),
            taxPercent: BuiltValueNullFieldError.checkNotNull(
                taxPercent, r'Plan', 'taxPercent'),
            currency: BuiltValueNullFieldError.checkNotNull(
                currency, r'Plan', 'currency'),
            type: BuiltValueNullFieldError.checkNotNull(type, r'Plan', 'type'),
            frequency: BuiltValueNullFieldError.checkNotNull(
                frequency, r'Plan', 'frequency'),
            frequencyInterval: BuiltValueNullFieldError.checkNotNull(
                frequencyInterval, r'Plan', 'frequencyInterval'),
            operator: operator,
            trialPeriodDays: trialPeriodDays);
    replace(_$result);
    return _$result;
  }
}

class _$CreditCard extends CreditCard {
  @override
  final String fopId;
  @override
  final int expirationMonth;
  @override
  final int expirationYear;
  @override
  final String last4Digits;
  @override
  final String brand;
  @override
  final bool isDefaultCard;

  factory _$CreditCard([void Function(CreditCardBuilder)? updates]) =>
      (new CreditCardBuilder()..update(updates))._build();

  _$CreditCard._(
      {required this.fopId,
      required this.expirationMonth,
      required this.expirationYear,
      required this.last4Digits,
      required this.brand,
      required this.isDefaultCard})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(fopId, r'CreditCard', 'fopId');
    BuiltValueNullFieldError.checkNotNull(
        expirationMonth, r'CreditCard', 'expirationMonth');
    BuiltValueNullFieldError.checkNotNull(
        expirationYear, r'CreditCard', 'expirationYear');
    BuiltValueNullFieldError.checkNotNull(
        last4Digits, r'CreditCard', 'last4Digits');
    BuiltValueNullFieldError.checkNotNull(brand, r'CreditCard', 'brand');
    BuiltValueNullFieldError.checkNotNull(
        isDefaultCard, r'CreditCard', 'isDefaultCard');
  }

  @override
  CreditCard rebuild(void Function(CreditCardBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreditCardBuilder toBuilder() => new CreditCardBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreditCard &&
        fopId == other.fopId &&
        expirationMonth == other.expirationMonth &&
        expirationYear == other.expirationYear &&
        last4Digits == other.last4Digits &&
        brand == other.brand &&
        isDefaultCard == other.isDefaultCard;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, fopId.hashCode);
    _$hash = $jc(_$hash, expirationMonth.hashCode);
    _$hash = $jc(_$hash, expirationYear.hashCode);
    _$hash = $jc(_$hash, last4Digits.hashCode);
    _$hash = $jc(_$hash, brand.hashCode);
    _$hash = $jc(_$hash, isDefaultCard.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class CreditCardBuilder implements Builder<CreditCard, CreditCardBuilder> {
  _$CreditCard? _$v;

  String? _fopId;
  String? get fopId => _$this._fopId;
  set fopId(String? fopId) => _$this._fopId = fopId;

  int? _expirationMonth;
  int? get expirationMonth => _$this._expirationMonth;
  set expirationMonth(int? expirationMonth) =>
      _$this._expirationMonth = expirationMonth;

  int? _expirationYear;
  int? get expirationYear => _$this._expirationYear;
  set expirationYear(int? expirationYear) =>
      _$this._expirationYear = expirationYear;

  String? _last4Digits;
  String? get last4Digits => _$this._last4Digits;
  set last4Digits(String? last4Digits) => _$this._last4Digits = last4Digits;

  String? _brand;
  String? get brand => _$this._brand;
  set brand(String? brand) => _$this._brand = brand;

  bool? _isDefaultCard;
  bool? get isDefaultCard => _$this._isDefaultCard;
  set isDefaultCard(bool? isDefaultCard) =>
      _$this._isDefaultCard = isDefaultCard;

  CreditCardBuilder();

  CreditCardBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _fopId = $v.fopId;
      _expirationMonth = $v.expirationMonth;
      _expirationYear = $v.expirationYear;
      _last4Digits = $v.last4Digits;
      _brand = $v.brand;
      _isDefaultCard = $v.isDefaultCard;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreditCard other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreditCard;
  }

  @override
  void update(void Function(CreditCardBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CreditCard build() => _build();

  _$CreditCard _build() {
    final _$result = _$v ??
        new _$CreditCard._(
            fopId: BuiltValueNullFieldError.checkNotNull(
                fopId, r'CreditCard', 'fopId'),
            expirationMonth: BuiltValueNullFieldError.checkNotNull(
                expirationMonth, r'CreditCard', 'expirationMonth'),
            expirationYear: BuiltValueNullFieldError.checkNotNull(
                expirationYear, r'CreditCard', 'expirationYear'),
            last4Digits: BuiltValueNullFieldError.checkNotNull(
                last4Digits, r'CreditCard', 'last4Digits'),
            brand: BuiltValueNullFieldError.checkNotNull(
                brand, r'CreditCard', 'brand'),
            isDefaultCard: BuiltValueNullFieldError.checkNotNull(
                isDefaultCard, r'CreditCard', 'isDefaultCard'));
    replace(_$result);
    return _$result;
  }
}

class _$Voucher extends Voucher {
  @override
  final String voucherId;
  @override
  final String code;
  @override
  final String serialNumber;
  @override
  final bool isActivated;
  @override
  final Plan plan;

  factory _$Voucher([void Function(VoucherBuilder)? updates]) =>
      (new VoucherBuilder()..update(updates))._build();

  _$Voucher._(
      {required this.voucherId,
      required this.code,
      required this.serialNumber,
      required this.isActivated,
      required this.plan})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(voucherId, r'Voucher', 'voucherId');
    BuiltValueNullFieldError.checkNotNull(code, r'Voucher', 'code');
    BuiltValueNullFieldError.checkNotNull(
        serialNumber, r'Voucher', 'serialNumber');
    BuiltValueNullFieldError.checkNotNull(
        isActivated, r'Voucher', 'isActivated');
    BuiltValueNullFieldError.checkNotNull(plan, r'Voucher', 'plan');
  }

  @override
  Voucher rebuild(void Function(VoucherBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  VoucherBuilder toBuilder() => new VoucherBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Voucher &&
        voucherId == other.voucherId &&
        code == other.code &&
        serialNumber == other.serialNumber &&
        isActivated == other.isActivated &&
        plan == other.plan;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, voucherId.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, serialNumber.hashCode);
    _$hash = $jc(_$hash, isActivated.hashCode);
    _$hash = $jc(_$hash, plan.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class VoucherBuilder implements Builder<Voucher, VoucherBuilder> {
  _$Voucher? _$v;

  String? _voucherId;
  String? get voucherId => _$this._voucherId;
  set voucherId(String? voucherId) => _$this._voucherId = voucherId;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  String? _serialNumber;
  String? get serialNumber => _$this._serialNumber;
  set serialNumber(String? serialNumber) => _$this._serialNumber = serialNumber;

  bool? _isActivated;
  bool? get isActivated => _$this._isActivated;
  set isActivated(bool? isActivated) => _$this._isActivated = isActivated;

  PlanBuilder? _plan;
  PlanBuilder get plan => _$this._plan ??= new PlanBuilder();
  set plan(PlanBuilder? plan) => _$this._plan = plan;

  VoucherBuilder();

  VoucherBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _voucherId = $v.voucherId;
      _code = $v.code;
      _serialNumber = $v.serialNumber;
      _isActivated = $v.isActivated;
      _plan = $v.plan.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Voucher other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Voucher;
  }

  @override
  void update(void Function(VoucherBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Voucher build() => _build();

  _$Voucher _build() {
    _$Voucher _$result;
    try {
      _$result = _$v ??
          new _$Voucher._(
              voucherId: BuiltValueNullFieldError.checkNotNull(
                  voucherId, r'Voucher', 'voucherId'),
              code: BuiltValueNullFieldError.checkNotNull(
                  code, r'Voucher', 'code'),
              serialNumber: BuiltValueNullFieldError.checkNotNull(
                  serialNumber, r'Voucher', 'serialNumber'),
              isActivated: BuiltValueNullFieldError.checkNotNull(
                  isActivated, r'Voucher', 'isActivated'),
              plan: plan.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'plan';
        plan.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Voucher', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Invoice extends Invoice {
  @override
  final String invoiceId;
  @override
  final DateTime startDate;
  @override
  final DateTime endDate;
  @override
  final String currency;
  @override
  final String description;
  @override
  final String number;
  @override
  final DateTime created;
  @override
  final int taxAmount;
  @override
  final int totalAmount;
  @override
  final String ownerId;
  @override
  final String stripeId;
  @override
  final String pdfUrl;

  factory _$Invoice([void Function(InvoiceBuilder)? updates]) =>
      (new InvoiceBuilder()..update(updates))._build();

  _$Invoice._(
      {required this.invoiceId,
      required this.startDate,
      required this.endDate,
      required this.currency,
      required this.description,
      required this.number,
      required this.created,
      required this.taxAmount,
      required this.totalAmount,
      required this.ownerId,
      required this.stripeId,
      required this.pdfUrl})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(invoiceId, r'Invoice', 'invoiceId');
    BuiltValueNullFieldError.checkNotNull(startDate, r'Invoice', 'startDate');
    BuiltValueNullFieldError.checkNotNull(endDate, r'Invoice', 'endDate');
    BuiltValueNullFieldError.checkNotNull(currency, r'Invoice', 'currency');
    BuiltValueNullFieldError.checkNotNull(
        description, r'Invoice', 'description');
    BuiltValueNullFieldError.checkNotNull(number, r'Invoice', 'number');
    BuiltValueNullFieldError.checkNotNull(created, r'Invoice', 'created');
    BuiltValueNullFieldError.checkNotNull(taxAmount, r'Invoice', 'taxAmount');
    BuiltValueNullFieldError.checkNotNull(
        totalAmount, r'Invoice', 'totalAmount');
    BuiltValueNullFieldError.checkNotNull(ownerId, r'Invoice', 'ownerId');
    BuiltValueNullFieldError.checkNotNull(stripeId, r'Invoice', 'stripeId');
    BuiltValueNullFieldError.checkNotNull(pdfUrl, r'Invoice', 'pdfUrl');
  }

  @override
  Invoice rebuild(void Function(InvoiceBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  InvoiceBuilder toBuilder() => new InvoiceBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Invoice &&
        invoiceId == other.invoiceId &&
        startDate == other.startDate &&
        endDate == other.endDate &&
        currency == other.currency &&
        description == other.description &&
        number == other.number &&
        created == other.created &&
        taxAmount == other.taxAmount &&
        totalAmount == other.totalAmount &&
        ownerId == other.ownerId &&
        stripeId == other.stripeId &&
        pdfUrl == other.pdfUrl;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, invoiceId.hashCode);
    _$hash = $jc(_$hash, startDate.hashCode);
    _$hash = $jc(_$hash, endDate.hashCode);
    _$hash = $jc(_$hash, currency.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, number.hashCode);
    _$hash = $jc(_$hash, created.hashCode);
    _$hash = $jc(_$hash, taxAmount.hashCode);
    _$hash = $jc(_$hash, totalAmount.hashCode);
    _$hash = $jc(_$hash, ownerId.hashCode);
    _$hash = $jc(_$hash, stripeId.hashCode);
    _$hash = $jc(_$hash, pdfUrl.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class InvoiceBuilder implements Builder<Invoice, InvoiceBuilder> {
  _$Invoice? _$v;

  String? _invoiceId;
  String? get invoiceId => _$this._invoiceId;
  set invoiceId(String? invoiceId) => _$this._invoiceId = invoiceId;

  DateTime? _startDate;
  DateTime? get startDate => _$this._startDate;
  set startDate(DateTime? startDate) => _$this._startDate = startDate;

  DateTime? _endDate;
  DateTime? get endDate => _$this._endDate;
  set endDate(DateTime? endDate) => _$this._endDate = endDate;

  String? _currency;
  String? get currency => _$this._currency;
  set currency(String? currency) => _$this._currency = currency;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _number;
  String? get number => _$this._number;
  set number(String? number) => _$this._number = number;

  DateTime? _created;
  DateTime? get created => _$this._created;
  set created(DateTime? created) => _$this._created = created;

  int? _taxAmount;
  int? get taxAmount => _$this._taxAmount;
  set taxAmount(int? taxAmount) => _$this._taxAmount = taxAmount;

  int? _totalAmount;
  int? get totalAmount => _$this._totalAmount;
  set totalAmount(int? totalAmount) => _$this._totalAmount = totalAmount;

  String? _ownerId;
  String? get ownerId => _$this._ownerId;
  set ownerId(String? ownerId) => _$this._ownerId = ownerId;

  String? _stripeId;
  String? get stripeId => _$this._stripeId;
  set stripeId(String? stripeId) => _$this._stripeId = stripeId;

  String? _pdfUrl;
  String? get pdfUrl => _$this._pdfUrl;
  set pdfUrl(String? pdfUrl) => _$this._pdfUrl = pdfUrl;

  InvoiceBuilder();

  InvoiceBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _invoiceId = $v.invoiceId;
      _startDate = $v.startDate;
      _endDate = $v.endDate;
      _currency = $v.currency;
      _description = $v.description;
      _number = $v.number;
      _created = $v.created;
      _taxAmount = $v.taxAmount;
      _totalAmount = $v.totalAmount;
      _ownerId = $v.ownerId;
      _stripeId = $v.stripeId;
      _pdfUrl = $v.pdfUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Invoice other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Invoice;
  }

  @override
  void update(void Function(InvoiceBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Invoice build() => _build();

  _$Invoice _build() {
    final _$result = _$v ??
        new _$Invoice._(
            invoiceId: BuiltValueNullFieldError.checkNotNull(
                invoiceId, r'Invoice', 'invoiceId'),
            startDate: BuiltValueNullFieldError.checkNotNull(
                startDate, r'Invoice', 'startDate'),
            endDate: BuiltValueNullFieldError.checkNotNull(
                endDate, r'Invoice', 'endDate'),
            currency: BuiltValueNullFieldError.checkNotNull(
                currency, r'Invoice', 'currency'),
            description: BuiltValueNullFieldError.checkNotNull(
                description, r'Invoice', 'description'),
            number: BuiltValueNullFieldError.checkNotNull(
                number, r'Invoice', 'number'),
            created: BuiltValueNullFieldError.checkNotNull(
                created, r'Invoice', 'created'),
            taxAmount: BuiltValueNullFieldError.checkNotNull(
                taxAmount, r'Invoice', 'taxAmount'),
            totalAmount: BuiltValueNullFieldError.checkNotNull(
                totalAmount, r'Invoice', 'totalAmount'),
            ownerId: BuiltValueNullFieldError.checkNotNull(
                ownerId, r'Invoice', 'ownerId'),
            stripeId: BuiltValueNullFieldError.checkNotNull(
                stripeId, r'Invoice', 'stripeId'),
            pdfUrl: BuiltValueNullFieldError.checkNotNull(pdfUrl, r'Invoice', 'pdfUrl'));
    replace(_$result);
    return _$result;
  }
}

class _$Notification extends Notification {
  @override
  final String? channel;
  @override
  final EventType? event;
  @override
  final BuiltList<String>? options;
  @override
  final bool? isActive;
  @override
  final BuiltList<String> to;
  @override
  final String? id;
  @override
  final String deviceId;
  @override
  final BuiltList<Email>? emails;
  @override
  final BuiltList<Phone>? phones;

  factory _$Notification([void Function(NotificationBuilder)? updates]) =>
      (new NotificationBuilder()..update(updates))._build();

  _$Notification._(
      {this.channel,
      this.event,
      this.options,
      this.isActive,
      required this.to,
      this.id,
      required this.deviceId,
      this.emails,
      this.phones})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(to, r'Notification', 'to');
    BuiltValueNullFieldError.checkNotNull(
        deviceId, r'Notification', 'deviceId');
  }

  @override
  Notification rebuild(void Function(NotificationBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  NotificationBuilder toBuilder() => new NotificationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Notification &&
        channel == other.channel &&
        event == other.event &&
        options == other.options &&
        isActive == other.isActive &&
        to == other.to &&
        id == other.id &&
        deviceId == other.deviceId &&
        emails == other.emails &&
        phones == other.phones;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, channel.hashCode);
    _$hash = $jc(_$hash, event.hashCode);
    _$hash = $jc(_$hash, options.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, to.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, deviceId.hashCode);
    _$hash = $jc(_$hash, emails.hashCode);
    _$hash = $jc(_$hash, phones.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class NotificationBuilder
    implements Builder<Notification, NotificationBuilder> {
  _$Notification? _$v;

  String? _channel;
  String? get channel => _$this._channel;
  set channel(String? channel) => _$this._channel = channel;

  EventType? _event;
  EventType? get event => _$this._event;
  set event(EventType? event) => _$this._event = event;

  ListBuilder<String>? _options;
  ListBuilder<String> get options =>
      _$this._options ??= new ListBuilder<String>();
  set options(ListBuilder<String>? options) => _$this._options = options;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  ListBuilder<String>? _to;
  ListBuilder<String> get to => _$this._to ??= new ListBuilder<String>();
  set to(ListBuilder<String>? to) => _$this._to = to;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _deviceId;
  String? get deviceId => _$this._deviceId;
  set deviceId(String? deviceId) => _$this._deviceId = deviceId;

  ListBuilder<Email>? _emails;
  ListBuilder<Email> get emails => _$this._emails ??= new ListBuilder<Email>();
  set emails(ListBuilder<Email>? emails) => _$this._emails = emails;

  ListBuilder<Phone>? _phones;
  ListBuilder<Phone> get phones => _$this._phones ??= new ListBuilder<Phone>();
  set phones(ListBuilder<Phone>? phones) => _$this._phones = phones;

  NotificationBuilder();

  NotificationBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _channel = $v.channel;
      _event = $v.event;
      _options = $v.options?.toBuilder();
      _isActive = $v.isActive;
      _to = $v.to.toBuilder();
      _id = $v.id;
      _deviceId = $v.deviceId;
      _emails = $v.emails?.toBuilder();
      _phones = $v.phones?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Notification other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Notification;
  }

  @override
  void update(void Function(NotificationBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Notification build() => _build();

  _$Notification _build() {
    _$Notification _$result;
    try {
      _$result = _$v ??
          new _$Notification._(
              channel: channel,
              event: event,
              options: _options?.build(),
              isActive: isActive,
              to: to.build(),
              id: id,
              deviceId: BuiltValueNullFieldError.checkNotNull(
                  deviceId, r'Notification', 'deviceId'),
              emails: _emails?.build(),
              phones: _phones?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'options';
        _options?.build();

        _$failedField = 'to';
        to.build();

        _$failedField = 'emails';
        _emails?.build();
        _$failedField = 'phones';
        _phones?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Notification', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$BeaconConfiguration extends BeaconConfiguration {
  @override
  final String? deviceConfigurationId;
  @override
  final int? geolocationPeriod;
  @override
  final bool? sendMessageInMotion;
  @override
  final int? retryPeriod;
  @override
  final bool? isWifiSniffing;
  @override
  final bool? isCo2SendStatistics;
  @override
  final int? co2SendInterval;
  @override
  final int? co2ExtraMeasurementCount;
  @override
  final int? co2Offset;
  @override
  final int? co2AlertThreshold;
  @override
  final DateTime? recoveryModeRequest;
  @override
  final DateTime? recoveryModeExit;
  @override
  final String? configurationPlanId;
  @override
  final String? subscriptionPlanId;
  @override
  final RecoveryModeStatus? recoveryModeStatus;

  factory _$BeaconConfiguration(
          [void Function(BeaconConfigurationBuilder)? updates]) =>
      (new BeaconConfigurationBuilder()..update(updates))._build();

  _$BeaconConfiguration._(
      {this.deviceConfigurationId,
      this.geolocationPeriod,
      this.sendMessageInMotion,
      this.retryPeriod,
      this.isWifiSniffing,
      this.isCo2SendStatistics,
      this.co2SendInterval,
      this.co2ExtraMeasurementCount,
      this.co2Offset,
      this.co2AlertThreshold,
      this.recoveryModeRequest,
      this.recoveryModeExit,
      this.configurationPlanId,
      this.subscriptionPlanId,
      this.recoveryModeStatus})
      : super._();

  @override
  BeaconConfiguration rebuild(
          void Function(BeaconConfigurationBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeaconConfigurationBuilder toBuilder() =>
      new BeaconConfigurationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeaconConfiguration &&
        deviceConfigurationId == other.deviceConfigurationId &&
        geolocationPeriod == other.geolocationPeriod &&
        sendMessageInMotion == other.sendMessageInMotion &&
        retryPeriod == other.retryPeriod &&
        isWifiSniffing == other.isWifiSniffing &&
        isCo2SendStatistics == other.isCo2SendStatistics &&
        co2SendInterval == other.co2SendInterval &&
        co2ExtraMeasurementCount == other.co2ExtraMeasurementCount &&
        co2Offset == other.co2Offset &&
        co2AlertThreshold == other.co2AlertThreshold &&
        recoveryModeRequest == other.recoveryModeRequest &&
        recoveryModeExit == other.recoveryModeExit &&
        configurationPlanId == other.configurationPlanId &&
        subscriptionPlanId == other.subscriptionPlanId &&
        recoveryModeStatus == other.recoveryModeStatus;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, deviceConfigurationId.hashCode);
    _$hash = $jc(_$hash, geolocationPeriod.hashCode);
    _$hash = $jc(_$hash, sendMessageInMotion.hashCode);
    _$hash = $jc(_$hash, retryPeriod.hashCode);
    _$hash = $jc(_$hash, isWifiSniffing.hashCode);
    _$hash = $jc(_$hash, isCo2SendStatistics.hashCode);
    _$hash = $jc(_$hash, co2SendInterval.hashCode);
    _$hash = $jc(_$hash, co2ExtraMeasurementCount.hashCode);
    _$hash = $jc(_$hash, co2Offset.hashCode);
    _$hash = $jc(_$hash, co2AlertThreshold.hashCode);
    _$hash = $jc(_$hash, recoveryModeRequest.hashCode);
    _$hash = $jc(_$hash, recoveryModeExit.hashCode);
    _$hash = $jc(_$hash, configurationPlanId.hashCode);
    _$hash = $jc(_$hash, subscriptionPlanId.hashCode);
    _$hash = $jc(_$hash, recoveryModeStatus.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class BeaconConfigurationBuilder
    implements Builder<BeaconConfiguration, BeaconConfigurationBuilder> {
  _$BeaconConfiguration? _$v;

  String? _deviceConfigurationId;
  String? get deviceConfigurationId => _$this._deviceConfigurationId;
  set deviceConfigurationId(String? deviceConfigurationId) =>
      _$this._deviceConfigurationId = deviceConfigurationId;

  int? _geolocationPeriod;
  int? get geolocationPeriod => _$this._geolocationPeriod;
  set geolocationPeriod(int? geolocationPeriod) =>
      _$this._geolocationPeriod = geolocationPeriod;

  bool? _sendMessageInMotion;
  bool? get sendMessageInMotion => _$this._sendMessageInMotion;
  set sendMessageInMotion(bool? sendMessageInMotion) =>
      _$this._sendMessageInMotion = sendMessageInMotion;

  int? _retryPeriod;
  int? get retryPeriod => _$this._retryPeriod;
  set retryPeriod(int? retryPeriod) => _$this._retryPeriod = retryPeriod;

  bool? _isWifiSniffing;
  bool? get isWifiSniffing => _$this._isWifiSniffing;
  set isWifiSniffing(bool? isWifiSniffing) =>
      _$this._isWifiSniffing = isWifiSniffing;

  bool? _isCo2SendStatistics;
  bool? get isCo2SendStatistics => _$this._isCo2SendStatistics;
  set isCo2SendStatistics(bool? isCo2SendStatistics) =>
      _$this._isCo2SendStatistics = isCo2SendStatistics;

  int? _co2SendInterval;
  int? get co2SendInterval => _$this._co2SendInterval;
  set co2SendInterval(int? co2SendInterval) =>
      _$this._co2SendInterval = co2SendInterval;

  int? _co2ExtraMeasurementCount;
  int? get co2ExtraMeasurementCount => _$this._co2ExtraMeasurementCount;
  set co2ExtraMeasurementCount(int? co2ExtraMeasurementCount) =>
      _$this._co2ExtraMeasurementCount = co2ExtraMeasurementCount;

  int? _co2Offset;
  int? get co2Offset => _$this._co2Offset;
  set co2Offset(int? co2Offset) => _$this._co2Offset = co2Offset;

  int? _co2AlertThreshold;
  int? get co2AlertThreshold => _$this._co2AlertThreshold;
  set co2AlertThreshold(int? co2AlertThreshold) =>
      _$this._co2AlertThreshold = co2AlertThreshold;

  DateTime? _recoveryModeRequest;
  DateTime? get recoveryModeRequest => _$this._recoveryModeRequest;
  set recoveryModeRequest(DateTime? recoveryModeRequest) =>
      _$this._recoveryModeRequest = recoveryModeRequest;

  DateTime? _recoveryModeExit;
  DateTime? get recoveryModeExit => _$this._recoveryModeExit;
  set recoveryModeExit(DateTime? recoveryModeExit) =>
      _$this._recoveryModeExit = recoveryModeExit;

  String? _configurationPlanId;
  String? get configurationPlanId => _$this._configurationPlanId;
  set configurationPlanId(String? configurationPlanId) =>
      _$this._configurationPlanId = configurationPlanId;

  String? _subscriptionPlanId;
  String? get subscriptionPlanId => _$this._subscriptionPlanId;
  set subscriptionPlanId(String? subscriptionPlanId) =>
      _$this._subscriptionPlanId = subscriptionPlanId;

  RecoveryModeStatus? _recoveryModeStatus;
  RecoveryModeStatus? get recoveryModeStatus => _$this._recoveryModeStatus;
  set recoveryModeStatus(RecoveryModeStatus? recoveryModeStatus) =>
      _$this._recoveryModeStatus = recoveryModeStatus;

  BeaconConfigurationBuilder();

  BeaconConfigurationBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _deviceConfigurationId = $v.deviceConfigurationId;
      _geolocationPeriod = $v.geolocationPeriod;
      _sendMessageInMotion = $v.sendMessageInMotion;
      _retryPeriod = $v.retryPeriod;
      _isWifiSniffing = $v.isWifiSniffing;
      _isCo2SendStatistics = $v.isCo2SendStatistics;
      _co2SendInterval = $v.co2SendInterval;
      _co2ExtraMeasurementCount = $v.co2ExtraMeasurementCount;
      _co2Offset = $v.co2Offset;
      _co2AlertThreshold = $v.co2AlertThreshold;
      _recoveryModeRequest = $v.recoveryModeRequest;
      _recoveryModeExit = $v.recoveryModeExit;
      _configurationPlanId = $v.configurationPlanId;
      _subscriptionPlanId = $v.subscriptionPlanId;
      _recoveryModeStatus = $v.recoveryModeStatus;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BeaconConfiguration other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BeaconConfiguration;
  }

  @override
  void update(void Function(BeaconConfigurationBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BeaconConfiguration build() => _build();

  _$BeaconConfiguration _build() {
    final _$result = _$v ??
        new _$BeaconConfiguration._(
            deviceConfigurationId: deviceConfigurationId,
            geolocationPeriod: geolocationPeriod,
            sendMessageInMotion: sendMessageInMotion,
            retryPeriod: retryPeriod,
            isWifiSniffing: isWifiSniffing,
            isCo2SendStatistics: isCo2SendStatistics,
            co2SendInterval: co2SendInterval,
            co2ExtraMeasurementCount: co2ExtraMeasurementCount,
            co2Offset: co2Offset,
            co2AlertThreshold: co2AlertThreshold,
            recoveryModeRequest: recoveryModeRequest,
            recoveryModeExit: recoveryModeExit,
            configurationPlanId: configurationPlanId,
            subscriptionPlanId: subscriptionPlanId,
            recoveryModeStatus: recoveryModeStatus);
    replace(_$result);
    return _$result;
  }
}

class _$Email extends Email {
  @override
  final String email;

  factory _$Email([void Function(EmailBuilder)? updates]) =>
      (new EmailBuilder()..update(updates))._build();

  _$Email._({required this.email}) : super._() {
    BuiltValueNullFieldError.checkNotNull(email, r'Email', 'email');
  }

  @override
  Email rebuild(void Function(EmailBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EmailBuilder toBuilder() => new EmailBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Email && email == other.email;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, email.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class EmailBuilder implements Builder<Email, EmailBuilder> {
  _$Email? _$v;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  EmailBuilder();

  EmailBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _email = $v.email;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Email other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Email;
  }

  @override
  void update(void Function(EmailBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Email build() => _build();

  _$Email _build() {
    final _$result = _$v ??
        new _$Email._(
            email: BuiltValueNullFieldError.checkNotNull(
                email, r'Email', 'email'));
    replace(_$result);
    return _$result;
  }
}

class _$Phone extends Phone {
  @override
  final String phone;

  factory _$Phone([void Function(PhoneBuilder)? updates]) =>
      (new PhoneBuilder()..update(updates))._build();

  _$Phone._({required this.phone}) : super._() {
    BuiltValueNullFieldError.checkNotNull(phone, r'Phone', 'phone');
  }

  @override
  Phone rebuild(void Function(PhoneBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PhoneBuilder toBuilder() => new PhoneBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Phone && phone == other.phone;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, phone.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class PhoneBuilder implements Builder<Phone, PhoneBuilder> {
  _$Phone? _$v;

  String? _phone;
  String? get phone => _$this._phone;
  set phone(String? phone) => _$this._phone = phone;

  PhoneBuilder();

  PhoneBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _phone = $v.phone;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Phone other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Phone;
  }

  @override
  void update(void Function(PhoneBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Phone build() => _build();

  _$Phone _build() {
    final _$result = _$v ??
        new _$Phone._(
            phone: BuiltValueNullFieldError.checkNotNull(
                phone, r'Phone', 'phone'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
