import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ticatag_connect/ticatag_connect.dart';

part 'product_metadata.g.dart';

class SensorId extends EnumClass {
  static Serializer<SensorId> get serializer => _$sensorIdSerializer;

  static const SensorId button = _$button;
  static const SensorId door = _$door;
  static const SensorId rollover = _$rollover;
  static const SensorId location = _$location;
  static const SensorId waterleak = _$waterleak;
  static const SensorId shock = _$shock;
  static const SensorId motion = _$motion;
  static const SensorId temperature = _$temperature;
  static const SensorId humidity = _$humidity;
  static const SensorId battery = _$battery;
  static const SensorId totalOdometer = _$totalOdometer;
  static const SensorId co2 = _$co2;

  const SensorId._(String name) : super(name);

  static BuiltSet<SensorId> get values => _$sensorIdValues;
  static SensorId valueOf(String name) => _$sensorIdValueOf(name);
}

class Operator extends EnumClass {
  static Serializer<Operator> get serializer => _$operatorSerializer;

  static const Operator sigfox = _$sigfox;
  static const Operator digitalmatter = _$digitalmatter;
  static const Operator truphone = _$truphone;
  static const Operator smartphone = _$smartphone;

  const Operator._(String name) : super(name);

  static BuiltSet<Operator> get values => _$operatorValues;
  static Operator valueOf(String name) => _$operatorValueOf(name);
}

abstract class Sensor implements Built<Sensor, SensorBuilder> {
  static Serializer<Sensor> get serializer => _$sensorSerializer;
  String get apiEventName;
  String? get imageUrl;
  String get imageAsset;
  SensorId get sensorId;
  Map<String, String> get descriptions;
  Map<String, String> get names;

  Sensor._();
  factory Sensor([void Function(SensorBuilder) updates]) = _$Sensor;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Sensor.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Sensor.serializer, this));
  }

  static Sensor fromJson(String jsonString) {
    return serializers.deserializeWith(
        Sensor.serializer, json.decode(jsonString))!;
  }

  static Sensor fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Sensor.serializer, map)!;
  }
}

abstract class ProductMetadata
    implements Built<ProductMetadata, ProductMetadataBuilder> {
  static Serializer<ProductMetadata> get serializer =>
      _$productMetadataSerializer;

  @BuiltValueHook(initializeBuilder: true)
  static void _setDefaults(ProductMetadataBuilder b) => b
    ..isVoucherPlan = true
    ..isPrepaidPlan = true
    ..isPostpaidPlan = true
    ..isAllowPlanUpdate = false
    ..isAskForLocation = false
    ..vendor = Vendor((v) => v
      ..name = 'Ticatag'
      ..description = 'description ticatag').toBuilder();

  String get productId;
  Vendor get vendor;
  Operator get operator;
  BuiltMap<String, String> get names;
  BuiltMap<String, String> get descriptions;
  String? get imageAsset;
  String? get imageUrl;
  String? get instruction;
  String get instructionImageAsset;
  bool get isBluetoothConfig;
  bool get isScanQrcode;
  bool get isAskForLocation;
  bool? get isOnlineSubscription;
  bool get isPrepaidPlan;
  bool get isPostpaidPlan;
  bool get isVoucherPlan;
  bool get isAllowPlanUpdate;
  BuiltList<EventType> get eventTypes;
  BuiltList<SensorId> get sensorsIds;

  ProductMetadata._();
  factory ProductMetadata([void Function(ProductMetadataBuilder) updates]) =
      _$ProductMetadata;

  String toJson() {
    return json.encode(
        standardSerializers.serializeWith(ProductMetadata.serializer, this));
  }

  String toString() {
    return json.encode(
        standardSerializers.serializeWith(ProductMetadata.serializer, this));
  }

  static ProductMetadata fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProductMetadata.serializer, json.decode(jsonString))!;
  }

  static ProductMetadata fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(ProductMetadata.serializer, map)!;
  }
}

abstract class Vendor implements Built<Vendor, VendorBuilder> {
  static Serializer<Vendor> get serializer => _$vendorSerializer;
  String get name;
  String? get logoAsset;
  String get description;

  Vendor._();
  factory Vendor([void Function(VendorBuilder) updates]) = _$Vendor;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Vendor.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Vendor.serializer, this));
  }

  static Vendor fromJson(String jsonString) {
    return serializers.deserializeWith(
        Vendor.serializer, json.decode(jsonString))!;
  }

  static Vendor fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Vendor.serializer, map)!;
  }
}
