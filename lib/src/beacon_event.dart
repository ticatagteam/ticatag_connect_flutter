//flutter packages dart run build_runner build  --delete-conflicting-outputs

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'beacon.dart';
import 'serializers.dart';

part 'beacon_event.g.dart';

@BuiltValue(instantiable: false)
abstract class BeaconEvent extends Object {
  DateTime get timestamp;
  String? get macAddress;

  BeaconEvent rebuild(void Function(BeaconEventBuilder) updates);
  BeaconEventBuilder toBuilder();

  String toJson();
}

abstract class BeaconLocationEvent extends Object
    with Location
    implements
        BeaconEvent,
        Built<BeaconLocationEvent, BeaconLocationEventBuilder> {
  static Serializer<BeaconLocationEvent> get serializer =>
      _$beaconLocationEventSerializer;

  factory BeaconLocationEvent(
          [void Function(BeaconLocationEventBuilder) updates]) =
      _$BeaconLocationEvent;
  BeaconLocationEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }
}

abstract class BeaconBatteryEvent extends Object
    with Battery
    implements
        BeaconEvent,
        Built<BeaconBatteryEvent, BeaconBatteryEventBuilder> {
  static Serializer<BeaconBatteryEvent> get serializer =>
      _$beaconBatteryEventSerializer;

  factory BeaconBatteryEvent(
          [void Function(BeaconBatteryEventBuilder) updates]) =
      _$BeaconBatteryEvent;
  BeaconBatteryEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconBatteryEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(
        BeaconBatteryEvent.serializer, map);
  }
}

abstract class BeaconButtonEvent extends Object
    with Button, Location
    implements BeaconEvent, Built<BeaconButtonEvent, BeaconButtonEventBuilder> {
  static Serializer<BeaconButtonEvent> get serializer =>
      _$beaconButtonEventSerializer;

  factory BeaconButtonEvent([void Function(BeaconButtonEventBuilder) updates]) =
      _$BeaconButtonEvent;
  BeaconButtonEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }
}

abstract class BeaconHumidityEvent extends Object
    with Humidity
    implements
        BeaconEvent,
        Built<BeaconHumidityEvent, BeaconHumidityEventBuilder> {
  static Serializer<BeaconHumidityEvent> get serializer =>
      _$beaconHumidityEventSerializer;

  factory BeaconHumidityEvent(
          [void Function(BeaconHumidityEventBuilder) updates]) =
      _$BeaconHumidityEvent;
  BeaconHumidityEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconHumidityEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(
        BeaconHumidityEvent.serializer, map);
  }
}

abstract class BeaconTemperatureEvent extends Object
    with Temperature
    implements
        BeaconEvent,
        Built<BeaconTemperatureEvent, BeaconTemperatureEventBuilder> {
  static Serializer<BeaconTemperatureEvent> get serializer =>
      _$beaconTemperatureEventSerializer;

  factory BeaconTemperatureEvent(
          [void Function(BeaconTemperatureEventBuilder) updates]) =
      _$BeaconTemperatureEvent;
  BeaconTemperatureEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconTemperatureEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(
        BeaconTemperatureEvent.serializer, map);
  }
}

abstract class BeaconWaterLeakEvent extends Object
    with WaterLeak
    implements
        BeaconEvent,
        Built<BeaconWaterLeakEvent, BeaconWaterLeakEventBuilder> {
  static Serializer<BeaconWaterLeakEvent> get serializer =>
      _$beaconWaterLeakEventSerializer;

  factory BeaconWaterLeakEvent(
          [void Function(BeaconWaterLeakEventBuilder) updates]) =
      _$BeaconWaterLeakEvent;
  BeaconWaterLeakEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconWaterLeakEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(
        BeaconWaterLeakEvent.serializer, map);
  }
}

abstract class BeaconRolloverEvent extends Object
    with Rollover
    implements
        BeaconEvent,
        Built<BeaconRolloverEvent, BeaconRolloverEventBuilder> {
  static Serializer<BeaconRolloverEvent> get serializer =>
      _$beaconRolloverEventSerializer;

  factory BeaconRolloverEvent(
          [void Function(BeaconRolloverEventBuilder) updates]) =
      _$BeaconRolloverEvent;
  BeaconRolloverEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconRolloverEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(
        BeaconRolloverEvent.serializer, map);
  }
}

abstract class BeaconShockEvent extends Object
    with Shock
    implements BeaconEvent, Built<BeaconShockEvent, BeaconShockEventBuilder> {
  static Serializer<BeaconShockEvent> get serializer =>
      _$beaconShockEventSerializer;

  factory BeaconShockEvent([void Function(BeaconShockEventBuilder) updates]) =
      _$BeaconShockEvent;
  BeaconShockEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconShockEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(
        BeaconShockEvent.serializer, map);
  }
}

abstract class BeaconMotionEvent extends Object
    with Motion
    implements BeaconEvent, Built<BeaconMotionEvent, BeaconMotionEventBuilder> {
  static Serializer<BeaconMotionEvent> get serializer =>
      _$beaconMotionEventSerializer;

  factory BeaconMotionEvent([void Function(BeaconMotionEventBuilder) updates]) =
      _$BeaconMotionEvent;
  BeaconMotionEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconMotionEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(
        BeaconMotionEvent.serializer, map);
  }
}

abstract class BeaconDoorEvent extends Object
    with Door
    implements BeaconEvent, Built<BeaconDoorEvent, BeaconDoorEventBuilder> {
  static Serializer<BeaconDoorEvent> get serializer =>
      _$beaconDoorEventSerializer;

  factory BeaconDoorEvent([void Function(BeaconDoorEventBuilder) updates]) =
      _$BeaconDoorEvent;
  BeaconDoorEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconDoorEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(BeaconDoorEvent.serializer, map);
  }
}

abstract class BeaconCo2Event extends Object
    with Co2
    implements BeaconEvent, Built<BeaconCo2Event, BeaconCo2EventBuilder> {
  static Serializer<BeaconCo2Event> get serializer =>
      _$beaconCo2EventSerializer;

  factory BeaconCo2Event([void Function(BeaconCo2EventBuilder) updates]) =
      _$BeaconCo2Event;
  BeaconCo2Event._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconCo2Event? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(BeaconCo2Event.serializer, map);
  }
}

abstract class BeaconTotalOdometerEvent extends Object
    with TotalOdometer
    implements
        BeaconEvent,
        Built<BeaconTotalOdometerEvent, BeaconTotalOdometerEventBuilder> {
  static Serializer<BeaconTotalOdometerEvent> get serializer =>
      _$beaconTotalOdometerEventSerializer;

  factory BeaconTotalOdometerEvent(
          [void Function(BeaconTotalOdometerEventBuilder) updates]) =
      _$BeaconTotalOdometerEvent;
  BeaconTotalOdometerEvent._();

  String toJson() {
    return json.encode(standardSerializers.serialize(this));
  }

  static BeaconTotalOdometerEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(
        BeaconTotalOdometerEvent.serializer, map);
  }
}

mixin Location {
  double get latitude;
  double get longitude;
}

mixin Battery implements BeaconEvent {
  int get value;
  BatteryUnit get unit;
}

mixin Temperature implements BeaconEvent {
  double get value;
  String get unit;
}

mixin Humidity implements BeaconEvent {
  double get value;
  String get unit;
}

mixin WaterLeak implements BeaconEvent {
  WaterLeakStatus get status;
}

mixin Door implements BeaconEvent {
  DoorStatus get status;
}

mixin TotalOdometer implements BeaconEvent {
  int get value;
}

mixin Co2 implements BeaconEvent {
  int get value;
}

mixin Shock implements BeaconEvent {
  ShockStatus get status;
}

mixin Rollover implements BeaconEvent {
  RolloverStatus get status;
}

mixin Motion implements BeaconEvent {
  MotionStatus get status;
}

mixin Button implements BeaconEvent {
  ButtonPressedType get buttonPressedType;
}

class ButtonPressedType extends EnumClass {
  static Serializer<ButtonPressedType> get serializer =>
      _$buttonPressedTypeSerializer;

  static const ButtonPressedType simpleClick = _$simpleClick;
  static const ButtonPressedType doubleClick = _$doubleClick;

  const ButtonPressedType._(String name) : super(name);

  static BuiltSet<ButtonPressedType> get values => _$buttonPressedTypeValues;
  static ButtonPressedType valueOf(String name) =>
      _$buttonPressedTypeValueOf(name);
}
