// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(BatteryStatus.serializer)
      ..add(BatteryUnit.serializer)
      ..add(Beacon.serializer)
      ..add(BeaconBatteryEvent.serializer)
      ..add(BeaconButtonEvent.serializer)
      ..add(BeaconCo2Event.serializer)
      ..add(BeaconConfiguration.serializer)
      ..add(BeaconDoorEvent.serializer)
      ..add(BeaconHumidityEvent.serializer)
      ..add(BeaconLocationEvent.serializer)
      ..add(BeaconMotionEvent.serializer)
      ..add(BeaconRolloverEvent.serializer)
      ..add(BeaconShockEvent.serializer)
      ..add(BeaconState.serializer)
      ..add(BeaconStatus.serializer)
      ..add(BeaconTemperatureEvent.serializer)
      ..add(BeaconTotalOdometerEvent.serializer)
      ..add(BeaconWaterLeakEvent.serializer)
      ..add(ButtonEvent.serializer)
      ..add(ButtonPressedType.serializer)
      ..add(Co2Info.serializer)
      ..add(CreditCard.serializer)
      ..add(DoorInfo.serializer)
      ..add(DoorStatus.serializer)
      ..add(Email.serializer)
      ..add(EventType.serializer)
      ..add(Geofence.serializer)
      ..add(HumidityInfo.serializer)
      ..add(Invoice.serializer)
      ..add(LastBattery.serializer)
      ..add(LocationInfo.serializer)
      ..add(MotionInfo.serializer)
      ..add(MotionStatus.serializer)
      ..add(Notification.serializer)
      ..add(Operator.serializer)
      ..add(Organization.serializer)
      ..add(Phone.serializer)
      ..add(Plan.serializer)
      ..add(PlanType.serializer)
      ..add(Product.serializer)
      ..add(ProductMetadata.serializer)
      ..add(RecoveryModeStatus.serializer)
      ..add(RoleType.serializer)
      ..add(RolloverInfo.serializer)
      ..add(RolloverStatus.serializer)
      ..add(Sensor.serializer)
      ..add(SensorId.serializer)
      ..add(ShockInfo.serializer)
      ..add(ShockStatus.serializer)
      ..add(Subscription.serializer)
      ..add(SubscriptionStatus.serializer)
      ..add(TemperatureInfo.serializer)
      ..add(TemperatureUnit.serializer)
      ..add(TotalOdometerInfo.serializer)
      ..add(User.serializer)
      ..add(Vendor.serializer)
      ..add(Voucher.serializer)
      ..add(WaterLeakInfo.serializer)
      ..add(WaterLeakStatus.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Email)]),
          () => new ListBuilder<Email>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Phone)]),
          () => new ListBuilder<Phone>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(EventType)]),
          () => new ListBuilder<EventType>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SensorId)]),
          () => new ListBuilder<SensorId>()))
    .build();

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
