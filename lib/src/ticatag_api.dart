import 'dart:async';
import 'dart:io';

import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';
//import 'package:dio_firebase_performance/dio_firebase_performance.dart';
import 'package:flutter/foundation.dart';
import 'package:http_parser/http_parser.dart';
import 'package:logging/logging.dart';
import 'package:oauth_dio/oauth_dio.dart';
import 'package:ticatag_connect/src/beacon.dart';
import 'package:ticatag_connect/src/beacon_event.dart';
import 'package:ticatag_connect/src/date_utils.dart';
import 'package:ticatag_connect/src/geofence.dart';
import 'package:ticatag_connect/src/ios_peripheral_identifiers_manager.dart';
import 'package:ticatag_connect/src/serializers.dart';
import 'package:ticatag_connect/src/user.dart';
import 'package:ticatag_connect/ticatag_connect.dart';

typedef void OnUpdatePictureProgress(int send, int total);

enum DevicePlatform { ios, android }

class TicatagApi {
  final Logger _log = Logger("TicatagApi");
  String apiGate = 'https://api.ticatag.com/v2';
  final String clientId;
  final String clientSecret;
  Options? _cacheOption;
  final enableFirebasePerformance;
  final enableCache;
  final dioInterceptor;
  final enableLogging;
  final oAuthStorage;
  final VoidCallback? onAuthenticationError;
  StreamController _authenticationErrorStreamController =
      StreamController.broadcast();

  Stream<void> get errors => _authenticationErrorStreamController.stream;

  static const kTicatagAccessToken = 'TicatagAcessToken';
  static const kTicatagRefreshToken = 'TicatagRefreshToken';
  static Dio _dio = Dio();
  static TicatagApi? _instance;
  late OAuth _oauth;

  static TicatagApi initialize(
      {required String clientId,
      required String clientSecret,
      bool enableFirebasePerformance = true,
      bool enableCache = true,
      required OAuthStorage oAuthStorage,
      bool enableLogging = true,
      Interceptor? dioInterceptor,
      VoidCallback? onAuthenticationError,
      String apiGate = 'https://api.ticatag.com/v2'}) {
    if (_instance != null) {
      throw Exception('TicatagApi instance was already initialized');
    }

    _instance = TicatagApi._internal(
        clientId: clientId,
        clientSecret: clientSecret,
        enableCache: enableCache,
        enableLogging: enableLogging,
        oAuthStorage: oAuthStorage,
        dioInterceptor: dioInterceptor,
        onAuthenticationError: onAuthenticationError,
        enableFirebasePerformance: enableFirebasePerformance);
    if (enableCache) {
      //_instance._cacheOption =
      //  buildCacheOptions(Duration(days: 7), forceRefresh: true);
    }
    return _instance!;
  }

  static TicatagApi get instance {
    if (_instance == null) {
      throw Exception(
          "TicatagApi hasn't been initialized. Please call Firestore.initialize() before using it.");
    }
    return _instance!;
  }

  TicatagApi._internal(
      {required this.clientId,
      required this.clientSecret,
      this.enableFirebasePerformance,
      this.enableCache,
      this.dioInterceptor,
      this.enableLogging,
      this.oAuthStorage,
      this.onAuthenticationError,
      this.apiGate = 'https://api.ticatag.com/v2'}) {
    _dio.options.baseUrl = apiGate;
    _dio.options.headers['Accept'] = 'application/json';
    _dio.options.headers['Content-Type'] = 'application/json';
    _dio.options.connectTimeout = Duration(seconds: 7); //5s
    _dio.options.receiveTimeout = Duration(seconds: 7);

    _oauth = OAuth(
        tokenUrl: 'https://account.ticatag.com/uaa/oauth/token',
        clientId: clientId,
        storage: oAuthStorage ?? OAuthMemoryStorage(),
        clientSecret: clientSecret);

    _dio.interceptors.add(BearerInterceptor(_oauth));

    _dio.interceptors.add(InterceptorsWrapper(onError: (DioException e, handler) {
      if (e.response?.statusCode == 401) {
        if (onAuthenticationError != null) {
          onAuthenticationError?.call();
        }
        _authenticationErrorStreamController.add(null);
      }
      return handler.next(e);
    }));

    if (dioInterceptor != null) {
      _dio.interceptors.add(dioInterceptor);
    }

    if (enableLogging == true) {
      _dio.interceptors
          .add(LogInterceptor(responseBody: true, requestBody: true));
    }
  }

  Future<void> deleteUser() async {
    try {
      await _dio.delete('/uaa/account');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> registerUser(
      {required String email,
      required String password,
      required String firstname,
      required String lastname,
      required String timezoneName,
      String? organizationName,
      String? organizationId,
      required String language}) async {
    final parameters = {
      'email': email,
      'password': password,
      'first_name': firstname,
      'last_name': lastname,
      'lang_key': language,
      'timezone_name': timezoneName,
      if (organizationName != null) 'organization_name': organizationName,
      if (organizationId != null) 'organization_id': organizationId,
    };
    try {
      await _dio.post('/uaa/register', data: parameters);
    } on DioException catch (e) {
      if (e.response?.statusCode == 400) {
        if (e.response?.data != null) {
          if (e.response?.data != null &&
              e.response?.data['message'] ==
                  'error.email_address_already_exists') {
            throw TicatagApiUserAlreadyExistException();
          }
        }
      }
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<String?> login(String email, String password) async {
    OAuthToken token = await _oauth
        .requestToken(PasswordGrant(username: email, password: password));
    _oauth.storage.save(token);
    return token.accessToken;
  }

  Future<void> logout() {
    return _oauth.storage.clear();
  }

  void clearCache() {
    if (enableCache) {
      //_dioCacheManager?.clearAll();
    }
  }

  Future<User?> user() async {
    try {
      final response = await _dio.get(
        '/uaa/account?includes=country,user_id,image,organization,timezone_name',
        options: _cacheOption,
      );
      final user = User.fromMap(response.data);
      return user;
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Geofence?> createGeofence(Geofence geofence) async {
    try {
      final geofenceMap =
          standardSerializers.serializeWith(Geofence.serializer, geofence);
      final response = await _dio.post('/geofences', data: geofenceMap);
      if (response.statusCode == 201) {
        return Geofence.fromMap(response.data);
      }
      throw TicatagApiException('Error creating geofence ($geofence)');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> deleteGeofence({required String geofenceId}) async {
    try {
      await _dio.delete('/geofences/$geofenceId');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Geofence> updateGeofence(Geofence geofence) async {
    try {
      final geofenceMap =
          standardSerializers.serializeWith(Geofence.serializer, geofence);
      final response = await _dio.patch('/geofences/${geofence.identifier}',
          data: geofenceMap);
      if (response.statusCode == 204) {
        return geofence;
      }
      throw TicatagApiException('Error updating geofence ($geofence)');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<List<Geofence>> geofences({required String deviceId}) async {
    try {
      final queryParameters = {
        'includes': 'geofence_id,device_id',
        'device_id': '$deviceId'
      };

      final response = await _dio.get('/geofences',
          queryParameters: queryParameters, options: _cacheOption);
      if (response.data['_embedded'] != null &&
          response.data['_embedded']['geofences'] != null) {
        final geofencesJson = response.data['_embedded']['geofences'];
        List<Geofence> geofences = geofencesJson.map<Geofence>((data) {
          Geofence? geofence = Geofence.fromMap(data);
          return geofence;
        }).toList();
        return geofences;
      } else {
        return [];
      }
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Beacon?> beacon({String? serialNumber, String? deviceId}) async {
    try {
      var requestUrl;
      if (serialNumber != null) {
        requestUrl = '/devices/serial_number/$serialNumber';
      } else if (deviceId != null) {
        requestUrl = '/devices/$deviceId';
      }
      if (requestUrl == null) {
        throw TicatagApiException('Serial number or deviceId is mandatory');
      }

      final queryParameters = {
        'includes':
            'organization,last_humidity,last_temperature,last_co2,last_total_odometer,last_water_leak,last_door,last_motion,last_shock,last_rollover,last_subscription,configuration,name,status,device_id,last_location,ibeacon_identifiers,product,images,last_battery,mac_address,software_version'
      };

      final response = await _dio.get(requestUrl,
          queryParameters: queryParameters, options: _cacheOption);
      return Beacon.fromMap(response.data);
    } on DioException catch (e) {
      if (e.response?.statusCode == 400) {
        if (e.response?.data != null) {
          if (e.response?.data['message'] == 'error.device_already_attached') {
            throw TicatagApiBeaconAlreadyAttachedException(
                deviceId ?? serialNumber ?? '');
          }
        }
      }
      if (e.response?.statusCode == 404) {
        return null;
      }
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> updateBeaconConfiguration(
      BeaconConfiguration beaconConfiguration) async {
    final parameters = beaconConfiguration.toJson();
    try {
      await _dio.patch(
          '/config/devices/${beaconConfiguration.deviceConfigurationId}',
          data: parameters);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Beacon?> createBeacon(String productId, String serialNumber) async {
    try {
      final data = {
        'name': 'name',
        'type': 'unb',
        'serial_number': serialNumber,
        'product_id': productId,
      };
      final response = await _dio.post('/devices', data: data);
      final beacon = Beacon.fromMap(response.data);
      return beacon;
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<PaginatedResponse<Beacon>> beacons(
      {String? type,
      int page = 0,
      int size = 50,
      String? query,
      List<String>? beaconsIds}) async {
    try {
      final queryParameters = {
        'size': size,
        'page': page,
        if (type != null) 'type': type,
        if (query != null) 'q': query,
        if (beaconsIds != null && beaconsIds.length > 0)
          'device_ids': beaconsIds,
        'includes':
            'organization,last_humidity,last_temperature,last_co2,last_total_odometer,last_subscription,configuration,name,status,device_id,last_water_leak,last_door,last_motion,last_shock,last_rollover,last_location,ibeacon_identifiers,product,images,last_battery,mac_address,software_version'
      };

      final response = await _dio.get('/devices',
          queryParameters: queryParameters, options: _cacheOption);
      Map<String, dynamic> pageMap = response.data['page'];
      final pageInfo = PagingInfos(
          size: pageMap['size'],
          totalElements: pageMap['total_elements'],
          totalPages: pageMap['total_pages'],
          number: pageMap['number']);

      if (response.data['_embedded'] != null &&
          response.data['_embedded']['devices'] != null) {
        final devices = response.data['_embedded']['devices'];
        List<Beacon> beacons = [];

        devices.forEach((data) {
          try {
            Beacon? beacon = Beacon.fromMap(data);
            if (beacon.macAddress != null) {
              if (Platform.isAndroid) {
                final updatedBeacon = beacon.rebuild((b) => b
                  ..macAddress =
                      _transformMacAddressToAndroidFormat(b.macAddress!)
                  ..state = BeaconState.unknown);
                beacons.add(updatedBeacon);
              } else {
                final iosPeripheralIdentifier =
                    IosPeripheralIdentifiersManager()
                        .peripheralIdFromMacAddress(beacon.macAddress!);
                final updatedBeacon = beacon.rebuild((b) => b
                  ..iosPeripheralId = iosPeripheralIdentifier
                  ..state = BeaconState.unknown);
                beacons.add(updatedBeacon);
              }
            } else {
              beacons.add(beacon);
            }
          } catch (e) {
            print("===>Error on beacon: $e");
          }
        });
        return PaginatedResponse(beacons, pageInfo);
      } else {
        return PaginatedResponse([], pageInfo);
      }
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Beacon> attachBeacon(
      {required String name,
      required String type,
      String? macAddress,
      String? productName,
      required String organizationId,
      required String serialNumber,
      String iconName = 'key'}) async {
    final parameters = {
      if (productName != null) 'product_name': productName,
      'name': name,
      'type': type,
      'serial_number': serialNumber,
      'organization_id': organizationId,
      if (macAddress != null) 'mac_address': macAddress.toUpperCase(),
      'icon_name': iconName
    };
    try {
      final response = await _dio.post('/devices/attach', data: parameters);
      var beacon = Beacon.fromMap(response.data);
      return beacon;
    } on DioException catch (e) {
      if (e.response?.statusCode == 400) {
        if (e.response?.data != null) {
          if (e.response?.data['message'] == 'error.device_already_attached') {
            throw TicatagApiBeaconAlreadyAttachedException(serialNumber);
          }
        }
      }
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> detachBeacon(String deviceId) async {
    try {
      await _dio.delete('/devices/$deviceId/detach');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> updateBeaconName(String deviceId, String name) async {
    final parameters = {
      'name': name,
    };
    try {
      await _dio.patch('/devices/$deviceId', data: parameters);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> updateBeaconIconName(String beaconId, String name) async {
    final parameters = {
      'icon_name': name,
    };
    try {
      await _dio.patch('/devices/$beaconId', data: parameters);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> activateRecoveryMode(String configId) async {
    final parameters = {
      'recovery_mode_request': DateTime.now().toUtc().toIso8601String(),
    };
    try {
      await _dio.patch('/config/devices/$configId', data: parameters);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<UpdatePictureResponse> _updatePicture(String url,
      {File? file, List<int>? bytes, OnUpdatePictureProgress? progress}) async {
    FormData formData;
    late List<int> uploadedBytes;
    if (bytes != null) {
      uploadedBytes = bytes;
    }
    if (file != null) {
      uploadedBytes = file.readAsBytesSync();
    }
    formData = FormData.fromMap({
      "image": MultipartFile.fromBytes(uploadedBytes,
          filename: "picture.jpg", contentType: MediaType("image", "jpg")),
    });

    try {
      final response =
          await _dio.post(url, data: formData, onSendProgress: (send, total) {
        if (progress != null) progress(send, total);
      });
      return UpdatePictureResponse(
          imageUrl: response.data['image_url'],
          thumbnailUrl: response.data['thumbnail_url']);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<UpdatePictureResponse> updateUserPicture(
      {File? file, List<int>? bytes, OnUpdatePictureProgress? progress}) async {
    return _updatePicture('/uaa/account/image',
        file: file, bytes: bytes, progress: progress);
  }

  Future<void> updateUserTimezone(String timezoneName) async {
    try {
      await _dio.patch('/uaa/account', data: {'timezone_name': timezoneName});
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> updateUserPaymentCustomerId(String paymentCustomerId) async {
    try {
      await _dio.patch('/uaa/account',
          data: {'payment_customer_id': paymentCustomerId});
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> capturePayment(String paymentId) async {
    try {
      await _dio.post('/subscriptions/payments/$paymentId');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> cancelPayment(String paymentId) async {
    try {
      await _dio.delete('/subscriptions/payments/$paymentId');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> cancelSubscription(String subscriptionId) async {
    try {
      await _dio.delete('/subscriptions/$subscriptionId/cancel');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<UpdatePictureResponse> updateBeaconPicture(String beaconId,
      {File? file, List<int>? bytes, OnUpdatePictureProgress? progress}) async {
    //deleteCacheForBeacons();
    return _updatePicture('/devices/$beaconId/image',
        file: file, bytes: bytes, progress: progress);
  }

  Future<void> updatePassword(String password) async {
    final parameters = {
      'password': password,
    };
    try {
      await _dio.post('/uaa/account/change_password', data: parameters);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> passwordLost(String email) async {
    final parameters = {
      'email': email,
    };
    try {
      await _dio.post('/uaa/account/reset_password/init', data: parameters);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<CreditCard?> createCreditCard({required String token}) async {
    try {
      final parameters = {'token': token};
      final response =
          await _dio.post('/subscriptions/fops/cards', data: parameters);
      return CreditCard.fromMap(response.data);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> deleteCreditCard({required String cardId}) async {
    try {
      await _dio.delete('/subscriptions/fops/cards/$cardId');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<List<CreditCard>> creditCards() async {
    final queryParameters = {
      'includes': 'fop_id',
    };
    try {
      final response = await _dio.get('/subscriptions/fops/cards',
          queryParameters: queryParameters, options: _cacheOption);
      if (response.data != null) {
        List<CreditCard> cards = response.data.map<CreditCard>((data) {
          CreditCard? card = CreditCard.fromMap(data);
          return card;
        }).toList();
        return cards;
      } else {
        return [];
      }
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<List<Plan>> availablePlans({Map<String, dynamic>? filter}) async {
    try {
      final response = await _dio.get('/config/plans/device',
          options: _cacheOption, queryParameters: filter);
      if (response.data != null) {
        List<Plan> plans = response.data.map<Plan>((data) {
          Plan plan = Plan.fromMap(data);
          return plan;
        }).toList();
        plans.sort((plan1, plan2) => plan2.amount.compareTo(plan1.amount));
        return plans;
      } else {
        return [];
      }
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Plan?> plan(String id) async {
    try {
      final response = await _dio.get('/config/plans/device/$id');
      if (response.data != null) {
        Plan plan = Plan.fromMap(response.data);
        return plan;
      } else {
        return null;
      }
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<String> checkoutSession(
      {required String planId, required deviceId}) async {
    try {
      final params = {'plan_id': planId, 'device_id': deviceId};
      final response = await _dio.post('/subscriptions/checkout', data: params);
      final sessionUrl = response.data['session_url'];
      return sessionUrl;
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<String> paymentIntent({
    String? planId,
    String? voucherCode,
    required deviceId,
  }) async {
    final params = {
      'device_id': deviceId,
      if (voucherCode != null) 'voucher_code': voucherCode,
      if (planId != null) 'plan_id': planId
    };
    try {
      final response = await _dio.post('/subscriptions/intent', data: params);
      return response.data['client_secret'];
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Subscription> createSubscription(
      {required String deviceId, String? voucherCode, String? planId}) async {
    try {
      final parameters = {
        'device_id': deviceId,
        if (voucherCode != null) 'voucher_code': voucherCode,
        if (planId != null) 'plan_id': planId,
      };
      final response = await _dio.post('/subscriptions', data: parameters);
      if (response.statusCode == 201) {
        final subscription = Subscription.fromMap(response.data);
        if (subscription != null) {
          return subscription;
        } else {
          throw TicatagApiException('Error decoding subscription');
        }
      } else {
        throw TicatagApiException(
            'Error creating voucher subscription ${response.data}');
      }
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<List<Subscription>> subscriptions({required String deviceId}) async {
    final queryParameters = {
      'device_id': deviceId,
      'includes': 'plan,subscription_id',
    };
    try {
      final response = await _dio.get('/subscriptions',
          queryParameters: queryParameters, options: _cacheOption);
      if (response.data['_embedded'] != null &&
          response.data['_embedded']['subscriptions'] != null) {
        final subscriptionsData = response.data['_embedded']['subscriptions'];
        List<Subscription> subscriptions =
            subscriptionsData.map<Subscription>((data) {
          return Subscription.fromMap(data);
        }).toList();
        return subscriptions;
      } else {
        return [];
      }
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> deleteSubscription({required String subscriptionId}) async {
    try {
      await _dio.delete('/subscriptions/$subscriptionId/cancel');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Voucher?> voucherFromCode({required String code}) async {
    try {
      final response = await _dio.get('/subscriptions/vouchers/code/$code');
      if (response.data != null) {
        return Voucher.fromMap(response.data);
      } else {
        return null;
      }
    } on DioException catch (e) {
      if (e.response?.statusCode == 400) {
        if (e.response?.data != null) {
          if (e.response?.data['message']
              .contains('error.voucher_already_activated')) {
            throw TicatagApiVoucherAlreadyActivatedException(code);
          }
        }
      }
      if (e.response?.statusCode == 404) {
        if (e.response?.data != null) {
          if (e.response?.data['message']
              .contains('error.voucher_code_not_found')) {
            throw TicatagApiVoucherNotFoundException(code);
          }
        }
      }
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<List<int>> invoice({required String invoiceId}) async {
    try {
      final response = await _dio.get(
        '/invoices/$invoiceId',
        options: Options(responseType: ResponseType.bytes),
      );
      return response.data;
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<List<Invoice>> invoices() async {
    final queryParameters = {
      'sort': 'id,DESC',
      'includes': 'invoice_id',
    };
    try {
      final response = await _dio.get('/invoices',
          queryParameters: queryParameters, options: _cacheOption);
      if (response.data['_embedded'] != null &&
          response.data['_embedded']['invoices'] != null) {
        final invoicesData = response.data['_embedded']['invoices'];
        List<Invoice> invoices = invoicesData.map<Invoice>((data) {
          return Invoice.fromMap(data);
        }).toList();
        return invoices;
      } else {
        return [];
      }
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<List<Notification>> notifications({required String deviceId}) async {
    final queryParameters = {
      'sort': 'id,DESC',
      'includes': 'notification_id,device_id',
      'device_id': '$deviceId'
    };
    try {
      final response = await _dio.get('/notifications',
          queryParameters: queryParameters, options: _cacheOption);
      if (response.data['_embedded'] != null &&
          response.data['_embedded']['notifications'] != null) {
        final notificationsData = response.data['_embedded']['notifications'];
        List<Notification> notifications =
            notificationsData.map<Notification>((data) {
          return Notification.fromMap(data);
        }).toList();
        return notifications;
      } else {
        return [];
      }
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> deleteNotification({required String notificationId}) async {
    try {
      await _dio.delete('/notifications/$notificationId');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Notification?> updateNotification(
      {required Notification notification}) async {
    try {
      final notificationMap = standardSerializers.serializeWith(
          Notification.serializer, notification);
      final response = await _dio.put('/notifications/${notification.id}',
          data: notificationMap);
      return Notification.fromMap(response.data);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<Notification?> createNotification(
      {required Notification notification}) async {
    try {
      final notificationMap = standardSerializers.serializeWith(
          Notification.serializer, notification);
      final response = await _dio.post('/notifications', data: notificationMap);
      return Notification.fromMap(response.data);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> exportDataHistory(
      {required List<String> deviceIds,
      String data = 'location_history',
      String format = 'csv',
      DateTime? startDate,
      DateTime? endDate}) async {
    final body = {
      'device_ids': deviceIds,
      'data': data,
      'format': format,
      if (startDate != null) 'start_date': startDate.toUtc().toIso8601String(),
      if (endDate != null) 'end_date': endDate.toUtc().toIso8601String()
    };

    try {
      await _dio.post(
        '/batch/events/export',
        data: body,
      );
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<HistoricalInfosResponse> historicalInfosForBeacon(String beaconId,
      {DateTime? startDate,
      DateTime? endDate,
      int? lastDays,
      bool? today = false,
      bool? yesterday = false,
      bool? lastWeek = false,
      bool? lastMonth = false,
      bool? returnAllResults = false,
      String names = 'location',
      int page = 0,
      int? size}) async {
    final now = DateTime.now();

    DateTime? startFilterDate;
    DateTime? endFilterDate = now;
    if (startDate != null && endDate != null) {
      startFilterDate = startDate;
      endFilterDate = endDate;
    } else if (startDate != null) {
      startFilterDate = startDate;
      endFilterDate = now;
    } else if (lastMonth != null && lastMonth == true) {
      startFilterDate = DateUtils.lastMonth();
      endFilterDate = null;
    } else if (lastWeek != null && lastWeek == true) {
      startFilterDate = DateUtils.lastWeek();
      endFilterDate = null;
    } else if (yesterday != null && yesterday == true) {
      startFilterDate = DateUtils.yesterday();
      endFilterDate = DateUtils.today();
    } else if (today != null && today == true) {
      startFilterDate = DateUtils.today();
      endFilterDate = null;
    } else if (lastDays != null && lastDays == true) {
      startFilterDate = DateUtils.lastDays(lastDays);
      endFilterDate = null;
    } else if (startDate == null && endDate == null && size != null) {
      endFilterDate = null;
      startFilterDate = null;
    }

    var startDateStr;
    if (startFilterDate != null) {
      startDateStr = startFilterDate?.toUtc()?.toIso8601String();
    }

    final endDateStr = endFilterDate?.toUtc()?.toIso8601String();

    final queryParameters = {
      'names': names,
      'device_ids': beaconId,
      if (startDateStr != null) 'start_date': startDateStr,
      if (endDateStr != null) 'end_date': endDateStr,
      if (returnAllResults != null && returnAllResults == true)
        'search_result': 'return_all',
      'page': page,
      if (size != null) 'size': size
    };

    try {
      final response = await _dio.get('/events',
          queryParameters: queryParameters, options: _cacheOption);

      Map<String, dynamic> pageMap = response.data['page'];
      List<LocationInfo> locationEvents = [];
      List<ButtonEvent> buttonEvents = [];
      List<BeaconBatteryEvent> batteryEvents = [];
      List<BeaconWaterLeakEvent> waterLeakEvents = [];
      List<BeaconDoorEvent> doorEvents = [];
      List<BeaconEvent> beaconEvents = [];
      List<BeaconTotalOdometerEvent> totalOdometerEvents = [];
      List<BeaconCo2Event> co2Events = [];

      final pageInfo = PagingInfos(
          size: pageMap['size'],
          totalElements: pageMap['total_elements'],
          totalPages: pageMap['total_pages'],
          number: pageMap['number']);

      if (response.data['_embedded'] != null &&
          response.data['_embedded']['events'] != null) {
        final events = response.data['_embedded']['events'];

        events.forEach((event) {
          if (event['name'] == 'location') {
            LocationInfo locationEvent = LocationInfo.fromMap(event)!;
            locationEvents.add(locationEvent);
          } else if (event['name'] == 'button') {
            ButtonEvent buttonEvent = ButtonEvent.fromMap(event)!;
            buttonEvents.add(buttonEvent);
          } else if (event['name'] == 'battery') {
            BeaconBatteryEvent beaconBatteryEvent =
                BeaconBatteryEvent.fromMap(event)!;
            batteryEvents.add(beaconBatteryEvent);
            beaconEvents.add(beaconBatteryEvent);
          } else if (event['name'] == 'water_leak') {
            BeaconWaterLeakEvent waterLeakEvent =
                BeaconWaterLeakEvent.fromMap(event)!;
            beaconEvents.add(waterLeakEvent);
          } else if (event['name'] == 'shock') {
            BeaconShockEvent shockEvent = BeaconShockEvent.fromMap(event)!;
            beaconEvents.add(shockEvent);
          } else if (event['name'] == 'motion') {
            BeaconMotionEvent motionEvent = BeaconMotionEvent.fromMap(event)!;
            beaconEvents.add(motionEvent);
          } else if (event['name'] == 'rollover') {
            BeaconRolloverEvent rolloverEvent =
                BeaconRolloverEvent.fromMap(event)!;
            beaconEvents.add(rolloverEvent);
          } else if (event['name'] == 'door') {
            BeaconDoorEvent doorEvent = BeaconDoorEvent.fromMap(event)!;
            beaconEvents.add(doorEvent);
          } else if (event['name'] == 'temperature') {
            BeaconTemperatureEvent temperatureEvent =
                BeaconTemperatureEvent.fromMap(event)!;
            beaconEvents.add(temperatureEvent);
          } else if (event['name'] == 'humidity') {
            BeaconHumidityEvent humidityEvent =
                BeaconHumidityEvent.fromMap(event)!;
            beaconEvents.add(humidityEvent);
          } else if (event['name'] == 'total_odometer') {
            BeaconTotalOdometerEvent totalOdometerEvent =
                BeaconTotalOdometerEvent.fromMap(event)!;
            beaconEvents.add(totalOdometerEvent);
          } else if (event['name'] == 'co2') {
            BeaconCo2Event co2Event = BeaconCo2Event.fromMap(event)!;
            beaconEvents.add(co2Event);
            co2Events.add(co2Event);
          }
        });
      }

      return HistoricalInfosResponse(
          page: pageInfo,
          events: beaconEvents,
          batteryEvents: batteryEvents,
          locationInfos: locationEvents,
          buttonEvents: buttonEvents,
          waterLeakEvents: waterLeakEvents,
          totalOdometerEvents: totalOdometerEvents,
          co2Events: co2Events,
          doorEvents: doorEvents);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Map<String, dynamic>? beaconEventToTicatagEvent(BeaconEvent event) {
    if (event is BeaconBatteryEvent) {
      return {
        'timestamp': event.timestamp.toUtc().toIso8601String(),
        'mac_address': _normalizeMacAddress(event.macAddress!),
        'name': 'battery',
        'value': event.value,
        'unit': 'percent',
      };
    } else if (event is BeaconLocationEvent) {
      return {
        'timestamp': event.timestamp.toUtc().toIso8601String(),
        'mac_address': _normalizeMacAddress(event.macAddress!),
        'name': 'location',
        'longitude': event.longitude,
        'latitude': event.latitude,
      };
    } else if (event is BeaconButtonEvent) {
      return {
        'timestamp': event.timestamp.toUtc().toIso8601String(),
        'mac_address': _normalizeMacAddress(event.macAddress!),
        'name': 'button',
        'click_type': 'double_click'
      };
    } else if (event is BeaconTemperatureEvent) {
      return {
        'timestamp': event.timestamp.toUtc().toIso8601String(),
        'mac_address': _normalizeMacAddress(event.macAddress!),
        'name': 'temperature',
        'value': event.value,
        'unit': 'celsius',
      };
    } else if (event is BeaconHumidityEvent) {
      return {
        'timestamp': event.timestamp.toUtc().toIso8601String(),
        'mac_address': _normalizeMacAddress(event.macAddress!),
        'name': 'humidity',
        'value': event.value,
        'unit': 'celsius',
      };
    }
    return null;
  }

  Future<void> sendEvents(List<BeaconEvent> events) async {
    if (events.isEmpty) {
      _log.fine("No events to post");
      return;
    }
    final ticatagEvents = events.map((event) {
      return beaconEventToTicatagEvent(event);
    }).toList();

    try {
      await _dio.post(
        '/events',
        data: ticatagEvents,
      );
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  Future<void> registerPushNotificationDeviceToken(
      {required bool isSandbox,
      required String appName,
      required String token,
      required DevicePlatform devicePlatform}) async {
    try {
      final data = {
        'sandbox': isSandbox,
        'app_name': appName,
        'device_token': token,
        'mobile_device':
            (devicePlatform == DevicePlatform.android) ? 'android' : 'ios',
        'channel': 'push',
      };
      await _dio.post('/notifications', data: data);
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  //TODO not available serverSide
  Future<void> unRegisterPushNotificationDeviceToken(
      {required bool isSandbox,
      required String appName,
      required String token,
      required DevicePlatform devicePlatform}) async {
    try {
      await _dio.delete('/notifications');
    } on DioException catch (e) {
      throw TicatagApiException(e.message ?? '');
    } on DeserializationError catch (e) {
      throw TicatagApiException(e.toString());
    } catch (e) {
      throw TicatagApiException(e.toString());
    }
  }

  String _normalizeMacAddress(String macAddress) {
    return macAddress.replaceAll(':', '');
  }

  bool _isValidAndroidMacAddress(String macAddress) {
    if (macAddress.contains(':')) return true;
    return false;
  }

  String _transformMacAddressToAndroidFormat(String macAddress) {
    if (_isValidAndroidMacAddress(macAddress)) return macAddress;
    String androidMacAddress = '';
    for (int i = 0; i < macAddress.length; i++) {
      if (i % 2 == 0 && i > 0) {
        androidMacAddress += ':';
      }
      androidMacAddress += macAddress[i].toUpperCase();
    }
    return androidMacAddress;
  }
}

class TicatagApiException implements Exception {
  final String cause;

  const TicatagApiException(this.cause);

  @override
  String toString() {
    return 'TicatagApiException{cause: $cause}';
  }
}

class TicatagApiAccountNotActivatedException implements Exception {
  TicatagApiAccountNotActivatedException();

  @override
  String toString() {
    return 'TicatagApiAccountNotActivatedException';
  }
}

class TicatagApiBeaconAlreadyAttachedException implements Exception {
  TicatagApiBeaconAlreadyAttachedException(this.macAddress);

  String macAddress;

  @override
  String toString() {
    return 'TicatagApiBeaconAlreadyAttachedException';
  }
}

class TicatagApiBeaconNotFoundException implements Exception {
  TicatagApiBeaconNotFoundException(this.macAddress);

  String macAddress;

  @override
  String toString() {
    return 'TicatagApiBeaconNotFoundException';
  }
}

class TicatagApiVoucherNotFoundException implements Exception {
  TicatagApiVoucherNotFoundException(this.code);

  String code;

  @override
  String toString() {
    return 'TicatagApiVoucherNotFoundException ($code)';
  }
}

class TicatagApiVoucherAlreadyActivatedException implements Exception {
  TicatagApiVoucherAlreadyActivatedException(this.code);

  String code;

  @override
  String toString() {
    return 'TicatagApiVoucherAlreadyActivatedException ($code)';
  }
}

class TicatagApiUserAlreadyExistException implements Exception {
  TicatagApiUserAlreadyExistException();

  @override
  String toString() {
    return 'TicatagApiUserAlreadyExistException';
  }
}

class UpdatePictureResponse {
  final String imageUrl;
  final String? thumbnailUrl;

  UpdatePictureResponse({required this.imageUrl, this.thumbnailUrl});

  @override
  String toString() {
    return 'UpdatePictureResponse{imageUrl: $imageUrl, thumbnailUrl: $thumbnailUrl}';
  }
}

class PaginatedResponse<T> {
  final List<T> result;
  final PagingInfos page;

  const PaginatedResponse(this.result, this.page);

  @override
  String toString() {
    return 'PaginatedResponse{result: $result, page: $page}';
  }
}

class HistoricalInfosResponse {
  final List<LocationInfo> locationInfos;
  final List<ButtonEvent> buttonEvents;
  final List<BeaconBatteryEvent> batteryEvents;
  final List<BeaconWaterLeakEvent> waterLeakEvents;
  final List<BeaconDoorEvent> doorEvents;
  final List<BeaconTotalOdometerEvent> totalOdometerEvents;
  final List<BeaconCo2Event> co2Events;
  final List<BeaconEvent> events;
  final PagingInfos? page;

  HistoricalInfosResponse(
      {this.locationInfos = const [],
      this.buttonEvents = const [],
      this.batteryEvents = const [],
      this.waterLeakEvents = const [],
      this.totalOdometerEvents = const [],
      this.co2Events = const [],
      this.doorEvents = const [],
      this.events = const [],
      this.page});

  @override
  String toString() {
    return 'HistoricalInfosResponse{locationInfos: $locationInfos, buttonEvents: $buttonEvents, batteryEvents: $batteryEvents, waterLeakEvents: $waterLeakEvents, doorEvents: $doorEvents, totalOdometerEvents: $totalOdometerEvents, co2Events: $co2Events, events: $events, page: $page}';
  }
}

class PagingInfos {
  final int number;
  final int size;
  final int totalElements;
  final int totalPages;

  PagingInfos(
      {required this.number,
      required this.size,
      required this.totalElements,
      required this.totalPages});

  @override
  String toString() {
    return 'PagingInfos{number: $number, size: $size, totalElements: $totalElements, totalPages: $totalPages}';
  }
}
