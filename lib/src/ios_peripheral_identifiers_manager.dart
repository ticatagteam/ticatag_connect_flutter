import 'dart:collection';
import 'dart:convert';
import 'dart:core';

import 'package:shared_preferences/shared_preferences.dart';

class IosPeripheralIdentifiersManager {
  static const kTicatagIosPeripheralIdentifiers =
      'tibeconnect_ticatagIosPeripheralIdentifiers';

  Map<String, dynamic>? _identifiersMap;

  static final IosPeripheralIdentifiersManager _singleton =
      IosPeripheralIdentifiersManager._internal();

  factory IosPeripheralIdentifiersManager() {
    return _singleton;
  }

  IosPeripheralIdentifiersManager._internal();

  Future<void> init() async {
    return _load();
  }

  Future<void> clear() async {
    _identifiersMap?.clear();
  }

  Future<void> _load() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final jsonString = prefs.getString(kTicatagIosPeripheralIdentifiers);
    if (jsonString != null) {
      _identifiersMap = _fromJson(jsonString);
    } else {
      _identifiersMap = HashMap<String, String>();
    }
  }

  Future<void> _save() async {
    final jsonString = _toJson();
    if (jsonString != null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(kTicatagIosPeripheralIdentifiers, jsonString);
    }
  }

  Future<void> add(
      {required String macAddress, required String peripheralId}) async {
    if (_identifiersMap == null) {
      _identifiersMap = HashMap<String, String>();
    }
    _identifiersMap![macAddress] = peripheralId;
    await _save();
    return;
  }

  Future<void> remove({required String macAddress}) async {
    if (_identifiersMap == null) {
      return;
    }
    _identifiersMap?.remove(macAddress);
    await _save();
    return;
  }

  String? peripheralIdFromMacAddress(String macAddress) {
    if (_identifiersMap == null) {
      return null;
    }
    return _identifiersMap![macAddress];
  }

  String _toJson() {
    return jsonEncode(_identifiersMap);
  }

  Map<String, dynamic> _fromJson(jsonString) {
    return jsonDecode(jsonString);
  }
}
