import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ticatag_connect/src/user.dart';
import 'package:ticatag_connect/ticatag_connect.dart';

import 'serializers.dart';

part 'beacon.g.dart';

//dart run build_runner build  --delete-conflicting-outputs

abstract class Beacon implements Built<Beacon, BeaconBuilder> {
  static Serializer<Beacon> get serializer => _$beaconSerializer;
  @BuiltValueField(wireName: 'device_id')
  String get identifier;
  @BuiltValueField(wireName: 'serial_number')
  String? get serialNumber;
  @BuiltValueField(wireName: 'mac_address')
  String? get macAddress;
  String? get iosPeripheralId;
  String? get name;
  BeaconStatus? get status;
  @BuiltValueField(wireName: 'icon_name')
  String? get iconName;
  int? get major;
  int? get minor;
  Product get product;
  @BuiltValueField(wireName: 'last_door')
  DoorInfo? get lastDoor;
  @BuiltValueField(wireName: 'last_location')
  LocationInfo? get lastLocation;
  @BuiltValueField(wireName: 'last_water_leak')
  WaterLeakInfo? get lastWaterLeak;
  @BuiltValueField(wireName: 'last_total_odometer')
  TotalOdometerInfo? get lastTotalOdometer;
  @BuiltValueField(wireName: 'last_co2')
  Co2Info? get lastCo2;
  @BuiltValueField(wireName: 'last_temperature')
  TemperatureInfo? get lastTemperature;
  @BuiltValueField(wireName: 'last_humidity')
  HumidityInfo? get lastHumidity;
  @BuiltValueField(wireName: 'last_rollover')
  RolloverInfo? get lastRollover;
  @BuiltValueField(wireName: 'last_motion')
  MotionInfo? get lastMotion;
  @BuiltValueField(wireName: 'last_shock')
  ShockInfo? get lastShock;
  @BuiltValueField(serialize: false)
  BeaconState? get state;
  @BuiltValueField(wireName: 'last_battery')
  LastBattery? get lastBattery;
  @BuiltValueField(wireName: 'image_url')
  String? get imageUrl;
  @BuiltValueField(wireName: 'thumbnail_url')
  String? get thumbnailUrl;
  @BuiltValueField(wireName: 'last_subscription')
  Subscription? get lastSubscription;
  @BuiltValueField(wireName: 'configuration')
  BeaconConfiguration? get configuration;
  @BuiltValueField(wireName: 'software_version')
  String? get softwareVersion;
  Organization? get organization;

  Beacon._();
  factory Beacon([void Function(BeaconBuilder) updates]) = _$Beacon;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Beacon.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Beacon.serializer, this));
  }

  static Beacon fromJson(String jsonString) {
    return serializers.deserializeWith(
        Beacon.serializer, json.decode(jsonString))!;
  }

  static Beacon fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Beacon.serializer, map)!;
  }
}

class BatteryUnit extends EnumClass {
  static Serializer<BatteryUnit> get serializer => _$batteryUnitSerializer;

  static const BatteryUnit millivolt = _$millivolt;
  static const BatteryUnit percent = _$percent;

  const BatteryUnit._(String name) : super(name);

  static BuiltSet<BatteryUnit> get values => _$batteryUnitValues;
  static BatteryUnit valueOf(String name) => _$batteryUnitValueOf(name);
}

class BeaconState extends EnumClass {
  static Serializer<BeaconState> get serializer => _$beaconStateSerializer;

  static const BeaconState connecting = _$connecting;
  static const BeaconState connected = _$connected;
  static const BeaconState disconnecting = _$disconnecting;
  static const BeaconState disconnected = _$disconnected;
  static const BeaconState unknown = _$unknown;

  const BeaconState._(String name) : super(name);

  static BuiltSet<BeaconState> get values => _$stateValues;
  static BeaconState valueOf(String name) => _$stateValueOf(name);
}

class BeaconStatus extends EnumClass {
  static Serializer<BeaconStatus> get serializer => _$beaconStatusSerializer;

  static const BeaconStatus active = _$active;
  static const BeaconStatus inactive = _$inactive;
  static const BeaconStatus alert = _$alert;
  static const BeaconStatus unknown = _$unknownA;
  static const BeaconStatus lost = _$lost;

  const BeaconStatus._(String name) : super(name);

  static BuiltSet<BeaconStatus> get values => _$statusValues;
  static BeaconStatus valueOf(String name) => _$statusValueOf(name);
}

abstract class ButtonEvent implements Built<ButtonEvent, ButtonEventBuilder> {
  static Serializer<ButtonEvent> get serializer => _$buttonEventSerializer;

  DateTime get timestamp;

  ButtonEvent._();
  factory ButtonEvent([void Function(ButtonEventBuilder) updates]) =
      _$ButtonEvent;

  static ButtonEvent? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(ButtonEvent.serializer, map);
  }
}

abstract class MotionInfo implements Built<MotionInfo, MotionInfoBuilder> {
  static Serializer<MotionInfo> get serializer => _$motionInfoSerializer;

  DateTime? get timestamp;
  MotionStatus? get status;

  MotionInfo._();
  factory MotionInfo([void Function(MotionInfoBuilder) updates]) = _$MotionInfo;
}

abstract class ShockInfo implements Built<ShockInfo, ShockInfoBuilder> {
  static Serializer<ShockInfo> get serializer => _$shockInfoSerializer;

  DateTime? get timestamp;
  ShockStatus? get status;

  ShockInfo._();
  factory ShockInfo([void Function(ShockInfoBuilder) updates]) = _$ShockInfo;
}

abstract class RolloverInfo
    implements Built<RolloverInfo, RolloverInfoBuilder> {
  static Serializer<RolloverInfo> get serializer => _$rolloverInfoSerializer;

  DateTime? get timestamp;
  RolloverStatus? get status;

  RolloverInfo._();
  factory RolloverInfo([void Function(RolloverInfoBuilder) updates]) =
      _$RolloverInfo;
}

abstract class DoorInfo implements Built<DoorInfo, DoorInfoBuilder> {
  static Serializer<DoorInfo> get serializer => _$doorInfoSerializer;

  DateTime? get timestamp;
  DoorStatus? get status;

  DoorInfo._();
  factory DoorInfo([void Function(DoorInfoBuilder) updates]) = _$DoorInfo;
}

abstract class Co2Info implements Built<Co2Info, Co2InfoBuilder> {
  static Serializer<Co2Info> get serializer => _$co2InfoSerializer;

  DateTime? get timestamp;
  int? get value;

  Co2Info._();
  factory Co2Info([void Function(Co2InfoBuilder) updates]) = _$Co2Info;
}

abstract class HumidityInfo
    implements Built<HumidityInfo, HumidityInfoBuilder> {
  static Serializer<HumidityInfo> get serializer => _$humidityInfoSerializer;

  DateTime? get timestamp;
  double? get value;

  HumidityInfo._();
  factory HumidityInfo([void Function(HumidityInfoBuilder) updates]) =
      _$HumidityInfo;
}

class TemperatureUnit extends EnumClass {
  static Serializer<TemperatureUnit> get serializer =>
      _$temperatureUnitSerializer;

  static const TemperatureUnit celsius = _$celsius;
  static const TemperatureUnit degrees = _$degrees;

  const TemperatureUnit._(String name) : super(name);

  static BuiltSet<TemperatureUnit> get values => _$temperatureUnitValues;
  static TemperatureUnit valueOf(String name) => _$temperatureUnitValueOf(name);
}

abstract class TemperatureInfo
    implements Built<TemperatureInfo, TemperatureInfoBuilder> {
  static Serializer<TemperatureInfo> get serializer =>
      _$temperatureInfoSerializer;

  DateTime? get timestamp;
  double? get value;
  TemperatureUnit? get unit;

  TemperatureInfo._();
  factory TemperatureInfo([void Function(TemperatureInfoBuilder) updates]) =
      _$TemperatureInfo;
}

abstract class TotalOdometerInfo
    implements Built<TotalOdometerInfo, TotalOdometerInfoBuilder> {
  static Serializer<TotalOdometerInfo> get serializer =>
      _$totalOdometerInfoSerializer;

  DateTime? get timestamp;
  int? get value;

  TotalOdometerInfo._();
  factory TotalOdometerInfo([void Function(TotalOdometerInfoBuilder) updates]) =
      _$TotalOdometerInfo;
}

abstract class WaterLeakInfo
    implements Built<WaterLeakInfo, WaterLeakInfoBuilder> {
  static Serializer<WaterLeakInfo> get serializer => _$waterLeakInfoSerializer;

  DateTime? get timestamp;
  WaterLeakStatus? get status;

  WaterLeakInfo._();
  factory WaterLeakInfo([void Function(WaterLeakInfoBuilder) updates]) =
      _$WaterLeakInfo;
}

abstract class LocationInfo
    implements Built<LocationInfo, LocationInfoBuilder> {
  static Serializer<LocationInfo> get serializer => _$locationInfoSerializer;

  DateTime get timestamp;
  double get latitude;
  double get longitude;
  double? get altitude;
  double? get speed;
  int? get satellites;
  @BuiltValueField(wireName: 'formatted_address')
  String? get formattedAddress;
  bool? get isButtonEventDoubleClick;

  LocationInfo._();
  factory LocationInfo([void Function(LocationInfoBuilder) updates]) =
      _$LocationInfo;

  static LocationInfo? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(LocationInfo.serializer, map);
  }
}

abstract class LastBattery implements Built<LastBattery, LastBatteryBuilder> {
  static Serializer<LastBattery> get serializer => _$lastBatterySerializer;

  DateTime get timestamp;
  double? get value;
  BatteryUnit? get unit;
  BatteryStatus? get status;

  LastBattery._();
  factory LastBattery([void Function(LastBatteryBuilder) updates]) =
      _$LastBattery;
}

abstract class Product implements Built<Product, ProductBuilder> {
  static Serializer<Product> get serializer => _$productSerializer;

  String get name;
  @BuiltValueField(wireName: 'image_url')
  String? get imageUrl;
  @BuiltValueField(wireName: 'marketing_name')
  String get marketingName;

  Product._();
  factory Product([void Function(ProductBuilder) updates]) = _$Product;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Product.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Product.serializer, this));
  }

  static Product? fromJson(String jsonString) {
    return serializers.deserializeWith(
        Product.serializer, json.decode(jsonString));
  }
}

abstract class Subscription
    implements Built<Subscription, SubscriptionBuilder> {
  static Serializer<Subscription> get serializer => _$subscriptionSerializer;

  @BuiltValueField(wireName: 'subscription_id')
  String? get subscriptionId;
  @BuiltValueField(wireName: 'device_id')
  String? get deviceId;
  @BuiltValueField(wireName: 'voucher_id')
  String? get voucherId;
  @BuiltValueField(wireName: 'start_date')
  DateTime get startDate;
  @BuiltValueField(wireName: 'end_date')
  DateTime get endDate;
  SubscriptionStatus get status;
  Plan? get plan;

  Subscription._();
  factory Subscription([void Function(SubscriptionBuilder) updates]) =
      _$Subscription;

  String toJson() {
    return json.encode(
        standardSerializers.serializeWith(Subscription.serializer, this));
  }

  String toString() {
    return json.encode(
        standardSerializers.serializeWith(Subscription.serializer, this));
  }

  static Subscription fromJson(String jsonString) {
    return serializers.deserializeWith(
        Subscription.serializer, json.decode(jsonString))!;
  }

  static Subscription fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Subscription.serializer, map)!;
  }
}

class SubscriptionStatus extends EnumClass {
  static Serializer<SubscriptionStatus> get serializer =>
      _$subscriptionStatusSerializer;

  static const SubscriptionStatus trialing = _$subscriptionStatusTrialing;
  static const SubscriptionStatus active = _$subscriptionStatusActive;
  static const SubscriptionStatus canceled = _$subscriptionStatusCanceled;
  static const SubscriptionStatus unpaid = _$subscriptionStatusUnpaid;
  static const SubscriptionStatus past = _$subscriptionStatusPast;
  static const SubscriptionStatus unknown = _$subscriptionStatusUnknown;

  const SubscriptionStatus._(String name) : super(name);

  static BuiltSet<SubscriptionStatus> get values => _$subscriptionStatusValues;
  static SubscriptionStatus valueOf(String name) =>
      _$subscriptionStatusValueOf(name);
}

abstract class Plan implements Built<Plan, PlanBuilder> {
  static Serializer<Plan> get serializer => _$planSerializer;

  @BuiltValueField(wireName: 'plan_id')
  String get planId;
  String get title;
  String? get description;
  double get amount;
  @BuiltValueField(wireName: 'tax_percent')
  double get taxPercent;
  String get currency;
  PlanType get type;
  String get frequency;
  @BuiltValueField(wireName: 'frequency_interval')
  int get frequencyInterval;
  String? get operator;
  @BuiltValueField(wireName: 'trial_period_days')
  int? get trialPeriodDays;

  Plan._();
  factory Plan([void Function(PlanBuilder) updates]) = _$Plan;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Plan.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Plan.serializer, this));
  }

  static Plan fromJson(String jsonString) {
    return serializers.deserializeWith(
        Plan.serializer, json.decode(jsonString))!;
  }

  static Plan fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Plan.serializer, map)!;
  }
}

abstract class CreditCard implements Built<CreditCard, CreditCardBuilder> {
  static Serializer<CreditCard> get serializer => _$creditCardSerializer;

  @BuiltValueField(wireName: 'fop_id')
  String get fopId;
  @BuiltValueField(wireName: 'exp_month')
  int get expirationMonth;
  @BuiltValueField(wireName: 'exp_year')
  int get expirationYear;
  @BuiltValueField(wireName: 'last4')
  String get last4Digits;
  String get brand;
  @BuiltValueField(wireName: 'default_card')
  bool get isDefaultCard;

  CreditCard._();
  factory CreditCard([void Function(CreditCardBuilder) updates]) = _$CreditCard;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(CreditCard.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(CreditCard.serializer, this));
  }

  Object toMap() {
    return standardSerializers.serializeWith(CreditCard.serializer, this)!;
  }

  static CreditCard fromJson(String jsonString) {
    return serializers.deserializeWith(
        CreditCard.serializer, json.decode(jsonString))!;
  }

  static CreditCard fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(CreditCard.serializer, map)!;
  }
}

abstract class Voucher implements Built<Voucher, VoucherBuilder> {
  static Serializer<Voucher> get serializer => _$voucherSerializer;

  @BuiltValueField(wireName: 'voucher_id')
  String get voucherId;
  String get code;
  @BuiltValueField(wireName: 'serial_number')
  String get serialNumber;
  @BuiltValueField(wireName: 'activated')
  bool get isActivated;
  Plan get plan;

  Voucher._();
  factory Voucher([void Function(VoucherBuilder) updates]) = _$Voucher;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Voucher.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Voucher.serializer, this));
  }

  static Voucher fromJson(String jsonString) {
    return serializers.deserializeWith(
        Voucher.serializer, json.decode(jsonString))!;
  }

  static Voucher fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Voucher.serializer, map)!;
  }
}

abstract class Invoice implements Built<Invoice, InvoiceBuilder> {
  static Serializer<Invoice> get serializer => _$invoiceSerializer;

  @BuiltValueField(wireName: 'invoice_id')
  String get invoiceId;
  @BuiltValueField(wireName: 'period_start')
  DateTime get startDate;
  @BuiltValueField(wireName: 'period_end')
  DateTime get endDate;
  String get currency;
  String get description;
  String get number;
  DateTime get created;
  //@BuiltValueField(wireName: 'paid_date')
  //DateTime get paidDate;
  @BuiltValueField(wireName: 'tax_amount')
  int get taxAmount;
  @BuiltValueField(wireName: 'total_amount')
  int get totalAmount;
  @BuiltValueField(wireName: 'owner_id')
  String get ownerId;
  @BuiltValueField(wireName: 'owner_stripe_id')
  String get stripeId;
  @BuiltValueField(wireName: 'pdf')
  String get pdfUrl;

  Invoice._();
  factory Invoice([void Function(InvoiceBuilder) updates]) = _$Invoice;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Invoice.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Invoice.serializer, this));
  }

  static Invoice fromJson(String jsonString) {
    return serializers.deserializeWith(
        Invoice.serializer, json.decode(jsonString))!;
  }

  static Invoice fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Invoice.serializer, map)!;
  }
}

abstract class Notification
    implements Built<Notification, NotificationBuilder> {
  static Serializer<Notification> get serializer => _$notificationSerializer;

  String? get channel;
  EventType? get event;
  BuiltList<String>? get options;
  @BuiltValueField(wireName: 'active')
  bool? get isActive;
  BuiltList<String> get to;
  @BuiltValueField(wireName: 'notification_id')
  String? get id;
  @BuiltValueField(wireName: 'device_id')
  String get deviceId;
  BuiltList<Email>? get emails;
  BuiltList<Phone>? get phones;

  Notification._();
  factory Notification([void Function(NotificationBuilder) updates]) =
      _$Notification;

  String toJson() {
    return json.encode(
        standardSerializers.serializeWith(Notification.serializer, this));
  }

  String toString() {
    return json.encode(
        standardSerializers.serializeWith(Notification.serializer, this));
  }

  static Notification fromJson(String jsonString) {
    return serializers.deserializeWith(
        Notification.serializer, json.decode(jsonString))!;
  }

  static Notification fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Notification.serializer, map)!;

  }
}

abstract class BeaconConfiguration
    implements Built<BeaconConfiguration, BeaconConfigurationBuilder> {
  static Serializer<BeaconConfiguration> get serializer =>
      _$beaconConfigurationSerializer;

  @BuiltValueField(wireName: 'device_configuration_id')
  String? get deviceConfigurationId;

  @BuiltValueField(wireName: 'geolocation_period')
  int? get geolocationPeriod;

  @BuiltValueField(wireName: 'send_message_in_motion')
  bool? get sendMessageInMotion;

  @BuiltValueField(wireName: 'retry_period')
  int? get retryPeriod;

  @BuiltValueField(wireName: 'wifi_sniffing')
  bool? get isWifiSniffing;

  @BuiltValueField(wireName: 'co2_send_statistics')
  bool? get isCo2SendStatistics;

  @BuiltValueField(wireName: 'co2_send_interval')
  int? get co2SendInterval;

  @BuiltValueField(wireName: 'co2_extra_measurement_count')
  int? get co2ExtraMeasurementCount;

  @BuiltValueField(wireName: 'co2_offset')
  int? get co2Offset;

  @BuiltValueField(wireName: 'co2_alert_threshold')
  int? get co2AlertThreshold;

  @BuiltValueField(wireName: 'recovery_mode_request')
  DateTime? get recoveryModeRequest;

  @BuiltValueField(wireName: 'recovery_mode_exit')
  DateTime? get recoveryModeExit;

  @BuiltValueField(wireName: 'yabby_configuration_plan_id')
  String? get configurationPlanId;

  @BuiltValueField(wireName: 'subscription_plan_id')
  String? get subscriptionPlanId;



  @BuiltValueField(wireName: 'recovery_mode_status')
  RecoveryModeStatus? get recoveryModeStatus;



  BeaconConfiguration._();
  factory BeaconConfiguration(
          [void Function(BeaconConfigurationBuilder) updates]) =
      _$BeaconConfiguration;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(
        BeaconConfiguration.serializer, this));
  }

  String toString() {
    return json.encode(standardSerializers.serializeWith(
        BeaconConfiguration.serializer, this));
  }

  static BeaconConfiguration fromJson(String jsonString) {
    return serializers.deserializeWith(
        BeaconConfiguration.serializer, json.decode(jsonString))!;
  }

  static BeaconConfiguration fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(BeaconConfiguration.serializer, map)!;
  }
}

abstract class Email implements Built<Email, EmailBuilder> {
  static Serializer<Email> get serializer => _$emailSerializer;

  String get email;

  Email._();
  factory Email([void Function(EmailBuilder) updates]) = _$Email;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Email.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Email.serializer, this));
  }

  static Email fromJson(String jsonString) {
    return serializers.deserializeWith(
        Email.serializer, json.decode(jsonString))!;
  }

  static Email fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Email.serializer, map)!;
  }
}

abstract class Phone implements Built<Phone, PhoneBuilder> {
  static Serializer<Phone> get serializer => _$phoneSerializer;

  String get phone;

  Phone._();
  factory Phone([void Function(PhoneBuilder) updates]) = _$Phone;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Phone.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Phone.serializer, this));
  }

  static Phone fromJson(String jsonString) {
    return serializers.deserializeWith(
        Phone.serializer, json.decode(jsonString))!;
  }

  static Phone fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Phone.serializer, map)!;
  }
}

class EventType extends EnumClass {
  static Serializer<EventType> get serializer => _$eventTypeSerializer;

  @BuiltValueEnumConst(wireName: 'button_press')
  static const EventType buttonPressed = _$buttonPressed;

  @BuiltValueEnumConst(wireName: 'button_pressed')
  static const EventType buttonPressedSingleClick = _$buttonPressedSingleClick;

  @BuiltValueEnumConst(wireName: 'button_double_pressed')
  static const EventType buttonPressedDoubleClick = _$buttonPressedDoubleClick;

  @BuiltValueEnumConst(wireName: 'button_held')
  static const EventType buttonPressedLongClick = _$buttonPressedLongClick;

  @BuiltValueEnumConst(wireName: 'geofence_crossed')
  static const EventType geofenceCrossed = _$geofenceCrossed;

  @BuiltValueEnumConst(wireName: 'battery_low')
  static const EventType batteryLow = _$batteryLow;

  @BuiltValueEnumConst(wireName: 'battery_status_bad')
  static const EventType batteryBadStatus = _$batteryBadStatus;

  @BuiltValueEnumConst(wireName: 'door_opened')
  static const EventType doorOpened = _$doorOpened;

  @BuiltValueEnumConst(wireName: 'water_leak_detected')
  static const EventType waterLeakDetected = _$waterLeakDetected;

  @BuiltValueEnumConst(wireName: 'shock_detected')
  static const EventType shockDetected = _$shockDetected;

  @BuiltValueEnumConst(wireName: 'rollover_detected')
  static const EventType rolloverDetected = _$rolloverDetected;

  @BuiltValueEnumConst(wireName: 'temperature_threshold_reached')
  static const EventType temperatureThresholdReached =
      _$temperatureThresholdReached;

  @BuiltValueEnumConst(wireName: 'co2_threshold_reached')
  static const EventType co2ThresholdReached = _$co2ThresholdReached;

  @BuiltValueEnumConst(wireName: 'humidity_threshold_reached')
  static const EventType humidityThresholdReached = _$humidityThresholdReached;

  @BuiltValueEnumConst(wireName: 'address_country_changed')
  static const EventType addressCountryChanged = _$addressCountryChanged;

  @BuiltValueEnumConst(wireName: 'address_state_changed')
  static const EventType addressStateChanged = _$addressStateChanged;

  @BuiltValueEnumConst(wireName: 'address_city_changed')
  static const EventType addressCityChanged = _$addressCityChanged;

  @BuiltValueEnumConst(wireName: 'motion_detected')
  static const EventType motionDetected = _$motionDetected;

  const EventType._(String name) : super(name);

  static BuiltSet<EventType> get values => _$eventTypeValues;
  static EventType valueOf(String name) => _$eventTypeValueOf(name);
}

class RecoveryModeStatus extends EnumClass {
  static Serializer<RecoveryModeStatus> get serializer => _$recoveryModeStatusSerializer;

  static const RecoveryModeStatus inactive = _$recoveryModeInactive;
  static const RecoveryModeStatus active = _$recoveryModeactive;
  static const RecoveryModeStatus pending = _$recoveryModepending;

  const RecoveryModeStatus._(String name) : super(name);

  static BuiltSet<RecoveryModeStatus> get values => _$recoveryModeStatusValues;
  static RecoveryModeStatus valueOf(String name) => _$recoveryModeStatusValueOf(name);
}


class PlanType extends EnumClass {
  static Serializer<PlanType> get serializer => _$planTypeSerializer;

  static const PlanType prepaid = _$prepaid;
  static const PlanType postpaid = _$postpaid;
  static const PlanType voucher = _$voucher;

  const PlanType._(String name) : super(name);

  static BuiltSet<PlanType> get values => _$typeValues;
  static PlanType valueOf(String name) => _$typeValueOf(name);
}

class BatteryStatus extends EnumClass {
  static Serializer<BatteryStatus> get serializer => _$batteryStatusSerializer;

  static const BatteryStatus good = _$good;
  static const BatteryStatus medium = _$medium;
  static const BatteryStatus bad = _$bad;
  static const BatteryStatus unknown = _$unknownB;

  const BatteryStatus._(String name) : super(name);

  static BuiltSet<BatteryStatus> get values => _$batteryStatusValues;
  static BatteryStatus valueOf(String name) => _$batteryStatusValueOf(name);
}

class WaterLeakStatus extends EnumClass {
  static Serializer<WaterLeakStatus> get serializer =>
      _$waterLeakStatusSerializer;

  static const WaterLeakStatus on = _$on;
  static const WaterLeakStatus off = _$off;

  const WaterLeakStatus._(String name) : super(name);

  static BuiltSet<WaterLeakStatus> get values => _$waterLeakStatusValues;
  static WaterLeakStatus valueOf(String name) => _$waterLeakStatusValueOf(name);
}

class DoorStatus extends EnumClass {
  static Serializer<DoorStatus> get serializer => _$doorStatusSerializer;

  static const DoorStatus open = _$open;
  static const DoorStatus closed = _$closed;

  const DoorStatus._(String name) : super(name);

  static BuiltSet<DoorStatus> get values => _$doorStatusValues;
  static DoorStatus valueOf(String name) => _$doorStatusValueOf(name);
}

class ShockStatus extends EnumClass {
  static Serializer<ShockStatus> get serializer => _$shockStatusSerializer;

  static const ShockStatus clear = _$clear;
  static const ShockStatus detected = _$detected;

  const ShockStatus._(String name) : super(name);

  static BuiltSet<ShockStatus> get values => _$shockStatusValues;
  static ShockStatus valueOf(String name) => _$shockStatusValueOf(name);
}

class MotionStatus extends EnumClass {
  static Serializer<MotionStatus> get serializer => _$motionStatusSerializer;

  static const MotionStatus still = _$still;
  static const MotionStatus moving = _$moving;

  const MotionStatus._(String name) : super(name);

  static BuiltSet<MotionStatus> get values => _$motionStatusValues;
  static MotionStatus valueOf(String name) => _$motionStatusValueOf(name);
}

class RolloverStatus extends EnumClass {
  static Serializer<RolloverStatus> get serializer =>
      _$rolloverStatusSerializer;

  @BuiltValueEnumConst(wireName: 'clear')
  static const RolloverStatus clear = _$rolloverStatusClear;
  @BuiltValueEnumConst(wireName: 'detected')
  static const RolloverStatus detected = _$rolloverStatusDetected;

  const RolloverStatus._(String name) : super(name);

  static BuiltSet<RolloverStatus> get values => _$rolloverStatusValues;
  static RolloverStatus valueOf(String name) => _$rolloverStatusValueOf(name);
}
