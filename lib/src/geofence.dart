import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'geofence.g.dart';

//flutter packages pub run build_runner build  --delete-conflicting-outputs

abstract class Geofence implements Built<Geofence, GeofenceBuilder> {
  static Serializer<Geofence> get serializer => _$geofenceSerializer;
  @BuiltValueField(wireName: 'geofence_id')
  String? get identifier;
  @BuiltValueField(wireName: 'device_id')
  String? get deviceId;
  double get longitude;
  double get latitude;
  int get radius;
  @BuiltValueField(wireName: 'active')
  bool get isActive;
  @BuiltValueField(wireName: 'on_entry')
  bool get isOnEntry;
  @BuiltValueField(wireName: 'on_exit')
  bool get isOnExit;
  @BuiltValueField(wireName: 'formatted_address')
  String? get formattedAddress;

  Geofence._();
  factory Geofence([void Function(GeofenceBuilder) updates]) = _$Geofence;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(Geofence.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(Geofence.serializer, this));
  }

  static Geofence fromJson(String jsonString) {
    return serializers.deserializeWith(
        Geofence.serializer, json.decode(jsonString))!;
  }

  static Geofence fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Geofence.serializer, map)!;
  }
}
