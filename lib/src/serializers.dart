library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:ticatag_connect/src/beacon.dart';
import 'package:ticatag_connect/src/beacon_event.dart';
import 'package:ticatag_connect/src/geofence.dart';
import 'package:ticatag_connect/src/product_metadata.dart';
import 'package:ticatag_connect/src/user.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  User,
  Organization,
  Beacon,
  LocationInfo,
  BeaconState,
  Geofence,
  BeaconDoorEvent,
  BeaconShockEvent,
  BeaconRolloverEvent,
  BeaconMotionEvent,
  BeaconWaterLeakEvent,
  BeaconBatteryEvent,
  BeaconTotalOdometerEvent,
  BeaconButtonEvent,
  BeaconLocationEvent,
  BeaconTemperatureEvent,
  BeaconHumidityEvent,
  BeaconCo2Event,
  ButtonEvent,
  Subscription,
  Plan,
  CreditCard,
  Voucher,
  Invoice,
  Notification,
  BeaconConfiguration,
  Email,
  Phone,
  WaterLeak,
  Co2,
  Co2Info,
  WaterLeakInfo,
  ProductMetadata,
  Sensor,
  Vendor,
])
final Serializers serializers = _$serializers;

final standardSerializers = (serializers.toBuilder()
      ..add(Iso8601DateTimeSerializer())
      ..addPlugin(StandardJsonPlugin(discriminator: 'type')))
    .build();

var builtListProductMetadata = const FullType(BuiltList, [
  FullType(BuiltList, [FullType(ProductMetadata)])
]);

/*
T? deserialize<T>(dynamic value) => standardSerializers.deserializeWith<T>(
    standardSerializers.serializerForType(T), value);

BuiltList<T> deserializeListOf<T>(dynamic value) => BuiltList.from(
    value.map((value) => deserialize<T>(value)).toList(growable: false));
*/
