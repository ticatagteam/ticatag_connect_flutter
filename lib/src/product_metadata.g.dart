// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_metadata.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const SensorId _$button = const SensorId._('button');
const SensorId _$door = const SensorId._('door');
const SensorId _$rollover = const SensorId._('rollover');
const SensorId _$location = const SensorId._('location');
const SensorId _$waterleak = const SensorId._('waterleak');
const SensorId _$shock = const SensorId._('shock');
const SensorId _$motion = const SensorId._('motion');
const SensorId _$temperature = const SensorId._('temperature');
const SensorId _$humidity = const SensorId._('humidity');
const SensorId _$battery = const SensorId._('battery');
const SensorId _$totalOdometer = const SensorId._('totalOdometer');
const SensorId _$co2 = const SensorId._('co2');

SensorId _$sensorIdValueOf(String name) {
  switch (name) {
    case 'button':
      return _$button;
    case 'door':
      return _$door;
    case 'rollover':
      return _$rollover;
    case 'location':
      return _$location;
    case 'waterleak':
      return _$waterleak;
    case 'shock':
      return _$shock;
    case 'motion':
      return _$motion;
    case 'temperature':
      return _$temperature;
    case 'humidity':
      return _$humidity;
    case 'battery':
      return _$battery;
    case 'totalOdometer':
      return _$totalOdometer;
    case 'co2':
      return _$co2;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<SensorId> _$sensorIdValues =
    new BuiltSet<SensorId>(const <SensorId>[
  _$button,
  _$door,
  _$rollover,
  _$location,
  _$waterleak,
  _$shock,
  _$motion,
  _$temperature,
  _$humidity,
  _$battery,
  _$totalOdometer,
  _$co2,
]);

const Operator _$sigfox = const Operator._('sigfox');
const Operator _$digitalmatter = const Operator._('digitalmatter');
const Operator _$truphone = const Operator._('truphone');
const Operator _$smartphone = const Operator._('smartphone');

Operator _$operatorValueOf(String name) {
  switch (name) {
    case 'sigfox':
      return _$sigfox;
    case 'digitalmatter':
      return _$digitalmatter;
    case 'truphone':
      return _$truphone;
    case 'smartphone':
      return _$smartphone;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<Operator> _$operatorValues =
    new BuiltSet<Operator>(const <Operator>[
  _$sigfox,
  _$digitalmatter,
  _$truphone,
  _$smartphone,
]);

Serializer<SensorId> _$sensorIdSerializer = new _$SensorIdSerializer();
Serializer<Operator> _$operatorSerializer = new _$OperatorSerializer();
Serializer<Sensor> _$sensorSerializer = new _$SensorSerializer();
Serializer<ProductMetadata> _$productMetadataSerializer =
    new _$ProductMetadataSerializer();
Serializer<Vendor> _$vendorSerializer = new _$VendorSerializer();

class _$SensorIdSerializer implements PrimitiveSerializer<SensorId> {
  @override
  final Iterable<Type> types = const <Type>[SensorId];
  @override
  final String wireName = 'SensorId';

  @override
  Object serialize(Serializers serializers, SensorId object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  SensorId deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      SensorId.valueOf(serialized as String);
}

class _$OperatorSerializer implements PrimitiveSerializer<Operator> {
  @override
  final Iterable<Type> types = const <Type>[Operator];
  @override
  final String wireName = 'Operator';

  @override
  Object serialize(Serializers serializers, Operator object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  Operator deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      Operator.valueOf(serialized as String);
}

class _$SensorSerializer implements StructuredSerializer<Sensor> {
  @override
  final Iterable<Type> types = const [Sensor, _$Sensor];
  @override
  final String wireName = 'Sensor';

  @override
  Iterable<Object?> serialize(Serializers serializers, Sensor object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'apiEventName',
      serializers.serialize(object.apiEventName,
          specifiedType: const FullType(String)),
      'imageAsset',
      serializers.serialize(object.imageAsset,
          specifiedType: const FullType(String)),
      'sensorId',
      serializers.serialize(object.sensorId,
          specifiedType: const FullType(SensorId)),
      'descriptions',
      serializers.serialize(object.descriptions,
          specifiedType: const FullType(
              Map, const [const FullType(String), const FullType(String)])),
      'names',
      serializers.serialize(object.names,
          specifiedType: const FullType(
              Map, const [const FullType(String), const FullType(String)])),
    ];
    Object? value;
    value = object.imageUrl;
    if (value != null) {
      result
        ..add('imageUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Sensor deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SensorBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'apiEventName':
          result.apiEventName = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'imageUrl':
          result.imageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'imageAsset':
          result.imageAsset = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'sensorId':
          result.sensorId = serializers.deserialize(value,
              specifiedType: const FullType(SensorId))! as SensorId;
          break;
        case 'descriptions':
          result.descriptions = serializers.deserialize(value,
              specifiedType: const FullType(Map, const [
                const FullType(String),
                const FullType(String)
              ]))! as Map<String, String>;
          break;
        case 'names':
          result.names = serializers.deserialize(value,
              specifiedType: const FullType(Map, const [
                const FullType(String),
                const FullType(String)
              ]))! as Map<String, String>;
          break;
      }
    }

    return result.build();
  }
}

class _$ProductMetadataSerializer
    implements StructuredSerializer<ProductMetadata> {
  @override
  final Iterable<Type> types = const [ProductMetadata, _$ProductMetadata];
  @override
  final String wireName = 'ProductMetadata';

  @override
  Iterable<Object?> serialize(Serializers serializers, ProductMetadata object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'productId',
      serializers.serialize(object.productId,
          specifiedType: const FullType(String)),
      'vendor',
      serializers.serialize(object.vendor,
          specifiedType: const FullType(Vendor)),
      'operator',
      serializers.serialize(object.operator,
          specifiedType: const FullType(Operator)),
      'names',
      serializers.serialize(object.names,
          specifiedType: const FullType(BuiltMap,
              const [const FullType(String), const FullType(String)])),
      'descriptions',
      serializers.serialize(object.descriptions,
          specifiedType: const FullType(BuiltMap,
              const [const FullType(String), const FullType(String)])),
      'instructionImageAsset',
      serializers.serialize(object.instructionImageAsset,
          specifiedType: const FullType(String)),
      'isBluetoothConfig',
      serializers.serialize(object.isBluetoothConfig,
          specifiedType: const FullType(bool)),
      'isScanQrcode',
      serializers.serialize(object.isScanQrcode,
          specifiedType: const FullType(bool)),
      'isAskForLocation',
      serializers.serialize(object.isAskForLocation,
          specifiedType: const FullType(bool)),
      'isPrepaidPlan',
      serializers.serialize(object.isPrepaidPlan,
          specifiedType: const FullType(bool)),
      'isPostpaidPlan',
      serializers.serialize(object.isPostpaidPlan,
          specifiedType: const FullType(bool)),
      'isVoucherPlan',
      serializers.serialize(object.isVoucherPlan,
          specifiedType: const FullType(bool)),
      'isAllowPlanUpdate',
      serializers.serialize(object.isAllowPlanUpdate,
          specifiedType: const FullType(bool)),
      'eventTypes',
      serializers.serialize(object.eventTypes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(EventType)])),
      'sensorsIds',
      serializers.serialize(object.sensorsIds,
          specifiedType:
              const FullType(BuiltList, const [const FullType(SensorId)])),
    ];
    Object? value;
    value = object.imageAsset;
    if (value != null) {
      result
        ..add('imageAsset')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.imageUrl;
    if (value != null) {
      result
        ..add('imageUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.instruction;
    if (value != null) {
      result
        ..add('instruction')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isOnlineSubscription;
    if (value != null) {
      result
        ..add('isOnlineSubscription')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  ProductMetadata deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProductMetadataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'productId':
          result.productId = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'vendor':
          result.vendor.replace(serializers.deserialize(value,
              specifiedType: const FullType(Vendor))! as Vendor);
          break;
        case 'operator':
          result.operator = serializers.deserialize(value,
              specifiedType: const FullType(Operator))! as Operator;
          break;
        case 'names':
          result.names.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap,
                  const [const FullType(String), const FullType(String)]))!);
          break;
        case 'descriptions':
          result.descriptions.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap,
                  const [const FullType(String), const FullType(String)]))!);
          break;
        case 'imageAsset':
          result.imageAsset = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'imageUrl':
          result.imageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'instruction':
          result.instruction = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'instructionImageAsset':
          result.instructionImageAsset = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'isBluetoothConfig':
          result.isBluetoothConfig = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'isScanQrcode':
          result.isScanQrcode = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'isAskForLocation':
          result.isAskForLocation = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'isOnlineSubscription':
          result.isOnlineSubscription = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'isPrepaidPlan':
          result.isPrepaidPlan = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'isPostpaidPlan':
          result.isPostpaidPlan = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'isVoucherPlan':
          result.isVoucherPlan = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'isAllowPlanUpdate':
          result.isAllowPlanUpdate = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'eventTypes':
          result.eventTypes.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(EventType)]))!
              as BuiltList<Object?>);
          break;
        case 'sensorsIds':
          result.sensorsIds.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SensorId)]))!
              as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$VendorSerializer implements StructuredSerializer<Vendor> {
  @override
  final Iterable<Type> types = const [Vendor, _$Vendor];
  @override
  final String wireName = 'Vendor';

  @override
  Iterable<Object?> serialize(Serializers serializers, Vendor object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
    ];
    Object? value;
    value = object.logoAsset;
    if (value != null) {
      result
        ..add('logoAsset')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Vendor deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new VendorBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'logoAsset':
          result.logoAsset = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Sensor extends Sensor {
  @override
  final String apiEventName;
  @override
  final String? imageUrl;
  @override
  final String imageAsset;
  @override
  final SensorId sensorId;
  @override
  final Map<String, String> descriptions;
  @override
  final Map<String, String> names;

  factory _$Sensor([void Function(SensorBuilder)? updates]) =>
      (new SensorBuilder()..update(updates))._build();

  _$Sensor._(
      {required this.apiEventName,
      this.imageUrl,
      required this.imageAsset,
      required this.sensorId,
      required this.descriptions,
      required this.names})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        apiEventName, r'Sensor', 'apiEventName');
    BuiltValueNullFieldError.checkNotNull(imageAsset, r'Sensor', 'imageAsset');
    BuiltValueNullFieldError.checkNotNull(sensorId, r'Sensor', 'sensorId');
    BuiltValueNullFieldError.checkNotNull(
        descriptions, r'Sensor', 'descriptions');
    BuiltValueNullFieldError.checkNotNull(names, r'Sensor', 'names');
  }

  @override
  Sensor rebuild(void Function(SensorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SensorBuilder toBuilder() => new SensorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Sensor &&
        apiEventName == other.apiEventName &&
        imageUrl == other.imageUrl &&
        imageAsset == other.imageAsset &&
        sensorId == other.sensorId &&
        descriptions == other.descriptions &&
        names == other.names;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, apiEventName.hashCode);
    _$hash = $jc(_$hash, imageUrl.hashCode);
    _$hash = $jc(_$hash, imageAsset.hashCode);
    _$hash = $jc(_$hash, sensorId.hashCode);
    _$hash = $jc(_$hash, descriptions.hashCode);
    _$hash = $jc(_$hash, names.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class SensorBuilder implements Builder<Sensor, SensorBuilder> {
  _$Sensor? _$v;

  String? _apiEventName;
  String? get apiEventName => _$this._apiEventName;
  set apiEventName(String? apiEventName) => _$this._apiEventName = apiEventName;

  String? _imageUrl;
  String? get imageUrl => _$this._imageUrl;
  set imageUrl(String? imageUrl) => _$this._imageUrl = imageUrl;

  String? _imageAsset;
  String? get imageAsset => _$this._imageAsset;
  set imageAsset(String? imageAsset) => _$this._imageAsset = imageAsset;

  SensorId? _sensorId;
  SensorId? get sensorId => _$this._sensorId;
  set sensorId(SensorId? sensorId) => _$this._sensorId = sensorId;

  Map<String, String>? _descriptions;
  Map<String, String>? get descriptions => _$this._descriptions;
  set descriptions(Map<String, String>? descriptions) =>
      _$this._descriptions = descriptions;

  Map<String, String>? _names;
  Map<String, String>? get names => _$this._names;
  set names(Map<String, String>? names) => _$this._names = names;

  SensorBuilder();

  SensorBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _apiEventName = $v.apiEventName;
      _imageUrl = $v.imageUrl;
      _imageAsset = $v.imageAsset;
      _sensorId = $v.sensorId;
      _descriptions = $v.descriptions;
      _names = $v.names;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Sensor other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Sensor;
  }

  @override
  void update(void Function(SensorBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Sensor build() => _build();

  _$Sensor _build() {
    final _$result = _$v ??
        new _$Sensor._(
            apiEventName: BuiltValueNullFieldError.checkNotNull(
                apiEventName, r'Sensor', 'apiEventName'),
            imageUrl: imageUrl,
            imageAsset: BuiltValueNullFieldError.checkNotNull(
                imageAsset, r'Sensor', 'imageAsset'),
            sensorId: BuiltValueNullFieldError.checkNotNull(
                sensorId, r'Sensor', 'sensorId'),
            descriptions: BuiltValueNullFieldError.checkNotNull(
                descriptions, r'Sensor', 'descriptions'),
            names: BuiltValueNullFieldError.checkNotNull(
                names, r'Sensor', 'names'));
    replace(_$result);
    return _$result;
  }
}

class _$ProductMetadata extends ProductMetadata {
  @override
  final String productId;
  @override
  final Vendor vendor;
  @override
  final Operator operator;
  @override
  final BuiltMap<String, String> names;
  @override
  final BuiltMap<String, String> descriptions;
  @override
  final String? imageAsset;
  @override
  final String? imageUrl;
  @override
  final String? instruction;
  @override
  final String instructionImageAsset;
  @override
  final bool isBluetoothConfig;
  @override
  final bool isScanQrcode;
  @override
  final bool isAskForLocation;
  @override
  final bool? isOnlineSubscription;
  @override
  final bool isPrepaidPlan;
  @override
  final bool isPostpaidPlan;
  @override
  final bool isVoucherPlan;
  @override
  final bool isAllowPlanUpdate;
  @override
  final BuiltList<EventType> eventTypes;
  @override
  final BuiltList<SensorId> sensorsIds;

  factory _$ProductMetadata([void Function(ProductMetadataBuilder)? updates]) =>
      (new ProductMetadataBuilder()..update(updates))._build();

  _$ProductMetadata._(
      {required this.productId,
      required this.vendor,
      required this.operator,
      required this.names,
      required this.descriptions,
      this.imageAsset,
      this.imageUrl,
      this.instruction,
      required this.instructionImageAsset,
      required this.isBluetoothConfig,
      required this.isScanQrcode,
      required this.isAskForLocation,
      this.isOnlineSubscription,
      required this.isPrepaidPlan,
      required this.isPostpaidPlan,
      required this.isVoucherPlan,
      required this.isAllowPlanUpdate,
      required this.eventTypes,
      required this.sensorsIds})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        productId, r'ProductMetadata', 'productId');
    BuiltValueNullFieldError.checkNotNull(vendor, r'ProductMetadata', 'vendor');
    BuiltValueNullFieldError.checkNotNull(
        operator, r'ProductMetadata', 'operator');
    BuiltValueNullFieldError.checkNotNull(names, r'ProductMetadata', 'names');
    BuiltValueNullFieldError.checkNotNull(
        descriptions, r'ProductMetadata', 'descriptions');
    BuiltValueNullFieldError.checkNotNull(
        instructionImageAsset, r'ProductMetadata', 'instructionImageAsset');
    BuiltValueNullFieldError.checkNotNull(
        isBluetoothConfig, r'ProductMetadata', 'isBluetoothConfig');
    BuiltValueNullFieldError.checkNotNull(
        isScanQrcode, r'ProductMetadata', 'isScanQrcode');
    BuiltValueNullFieldError.checkNotNull(
        isAskForLocation, r'ProductMetadata', 'isAskForLocation');
    BuiltValueNullFieldError.checkNotNull(
        isPrepaidPlan, r'ProductMetadata', 'isPrepaidPlan');
    BuiltValueNullFieldError.checkNotNull(
        isPostpaidPlan, r'ProductMetadata', 'isPostpaidPlan');
    BuiltValueNullFieldError.checkNotNull(
        isVoucherPlan, r'ProductMetadata', 'isVoucherPlan');
    BuiltValueNullFieldError.checkNotNull(
        isAllowPlanUpdate, r'ProductMetadata', 'isAllowPlanUpdate');
    BuiltValueNullFieldError.checkNotNull(
        eventTypes, r'ProductMetadata', 'eventTypes');
    BuiltValueNullFieldError.checkNotNull(
        sensorsIds, r'ProductMetadata', 'sensorsIds');
  }

  @override
  ProductMetadata rebuild(void Function(ProductMetadataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductMetadataBuilder toBuilder() =>
      new ProductMetadataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProductMetadata &&
        productId == other.productId &&
        vendor == other.vendor &&
        operator == other.operator &&
        names == other.names &&
        descriptions == other.descriptions &&
        imageAsset == other.imageAsset &&
        imageUrl == other.imageUrl &&
        instruction == other.instruction &&
        instructionImageAsset == other.instructionImageAsset &&
        isBluetoothConfig == other.isBluetoothConfig &&
        isScanQrcode == other.isScanQrcode &&
        isAskForLocation == other.isAskForLocation &&
        isOnlineSubscription == other.isOnlineSubscription &&
        isPrepaidPlan == other.isPrepaidPlan &&
        isPostpaidPlan == other.isPostpaidPlan &&
        isVoucherPlan == other.isVoucherPlan &&
        isAllowPlanUpdate == other.isAllowPlanUpdate &&
        eventTypes == other.eventTypes &&
        sensorsIds == other.sensorsIds;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, productId.hashCode);
    _$hash = $jc(_$hash, vendor.hashCode);
    _$hash = $jc(_$hash, operator.hashCode);
    _$hash = $jc(_$hash, names.hashCode);
    _$hash = $jc(_$hash, descriptions.hashCode);
    _$hash = $jc(_$hash, imageAsset.hashCode);
    _$hash = $jc(_$hash, imageUrl.hashCode);
    _$hash = $jc(_$hash, instruction.hashCode);
    _$hash = $jc(_$hash, instructionImageAsset.hashCode);
    _$hash = $jc(_$hash, isBluetoothConfig.hashCode);
    _$hash = $jc(_$hash, isScanQrcode.hashCode);
    _$hash = $jc(_$hash, isAskForLocation.hashCode);
    _$hash = $jc(_$hash, isOnlineSubscription.hashCode);
    _$hash = $jc(_$hash, isPrepaidPlan.hashCode);
    _$hash = $jc(_$hash, isPostpaidPlan.hashCode);
    _$hash = $jc(_$hash, isVoucherPlan.hashCode);
    _$hash = $jc(_$hash, isAllowPlanUpdate.hashCode);
    _$hash = $jc(_$hash, eventTypes.hashCode);
    _$hash = $jc(_$hash, sensorsIds.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class ProductMetadataBuilder
    implements Builder<ProductMetadata, ProductMetadataBuilder> {
  _$ProductMetadata? _$v;

  String? _productId;
  String? get productId => _$this._productId;
  set productId(String? productId) => _$this._productId = productId;

  VendorBuilder? _vendor;
  VendorBuilder get vendor => _$this._vendor ??= new VendorBuilder();
  set vendor(VendorBuilder? vendor) => _$this._vendor = vendor;

  Operator? _operator;
  Operator? get operator => _$this._operator;
  set operator(Operator? operator) => _$this._operator = operator;

  MapBuilder<String, String>? _names;
  MapBuilder<String, String> get names =>
      _$this._names ??= new MapBuilder<String, String>();
  set names(MapBuilder<String, String>? names) => _$this._names = names;

  MapBuilder<String, String>? _descriptions;
  MapBuilder<String, String> get descriptions =>
      _$this._descriptions ??= new MapBuilder<String, String>();
  set descriptions(MapBuilder<String, String>? descriptions) =>
      _$this._descriptions = descriptions;

  String? _imageAsset;
  String? get imageAsset => _$this._imageAsset;
  set imageAsset(String? imageAsset) => _$this._imageAsset = imageAsset;

  String? _imageUrl;
  String? get imageUrl => _$this._imageUrl;
  set imageUrl(String? imageUrl) => _$this._imageUrl = imageUrl;

  String? _instruction;
  String? get instruction => _$this._instruction;
  set instruction(String? instruction) => _$this._instruction = instruction;

  String? _instructionImageAsset;
  String? get instructionImageAsset => _$this._instructionImageAsset;
  set instructionImageAsset(String? instructionImageAsset) =>
      _$this._instructionImageAsset = instructionImageAsset;

  bool? _isBluetoothConfig;
  bool? get isBluetoothConfig => _$this._isBluetoothConfig;
  set isBluetoothConfig(bool? isBluetoothConfig) =>
      _$this._isBluetoothConfig = isBluetoothConfig;

  bool? _isScanQrcode;
  bool? get isScanQrcode => _$this._isScanQrcode;
  set isScanQrcode(bool? isScanQrcode) => _$this._isScanQrcode = isScanQrcode;

  bool? _isAskForLocation;
  bool? get isAskForLocation => _$this._isAskForLocation;
  set isAskForLocation(bool? isAskForLocation) =>
      _$this._isAskForLocation = isAskForLocation;

  bool? _isOnlineSubscription;
  bool? get isOnlineSubscription => _$this._isOnlineSubscription;
  set isOnlineSubscription(bool? isOnlineSubscription) =>
      _$this._isOnlineSubscription = isOnlineSubscription;

  bool? _isPrepaidPlan;
  bool? get isPrepaidPlan => _$this._isPrepaidPlan;
  set isPrepaidPlan(bool? isPrepaidPlan) =>
      _$this._isPrepaidPlan = isPrepaidPlan;

  bool? _isPostpaidPlan;
  bool? get isPostpaidPlan => _$this._isPostpaidPlan;
  set isPostpaidPlan(bool? isPostpaidPlan) =>
      _$this._isPostpaidPlan = isPostpaidPlan;

  bool? _isVoucherPlan;
  bool? get isVoucherPlan => _$this._isVoucherPlan;
  set isVoucherPlan(bool? isVoucherPlan) =>
      _$this._isVoucherPlan = isVoucherPlan;

  bool? _isAllowPlanUpdate;
  bool? get isAllowPlanUpdate => _$this._isAllowPlanUpdate;
  set isAllowPlanUpdate(bool? isAllowPlanUpdate) =>
      _$this._isAllowPlanUpdate = isAllowPlanUpdate;

  ListBuilder<EventType>? _eventTypes;
  ListBuilder<EventType> get eventTypes =>
      _$this._eventTypes ??= new ListBuilder<EventType>();
  set eventTypes(ListBuilder<EventType>? eventTypes) =>
      _$this._eventTypes = eventTypes;

  ListBuilder<SensorId>? _sensorsIds;
  ListBuilder<SensorId> get sensorsIds =>
      _$this._sensorsIds ??= new ListBuilder<SensorId>();
  set sensorsIds(ListBuilder<SensorId>? sensorsIds) =>
      _$this._sensorsIds = sensorsIds;

  ProductMetadataBuilder() {
    ProductMetadata._setDefaults(this);
  }

  ProductMetadataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _productId = $v.productId;
      _vendor = $v.vendor.toBuilder();
      _operator = $v.operator;
      _names = $v.names.toBuilder();
      _descriptions = $v.descriptions.toBuilder();
      _imageAsset = $v.imageAsset;
      _imageUrl = $v.imageUrl;
      _instruction = $v.instruction;
      _instructionImageAsset = $v.instructionImageAsset;
      _isBluetoothConfig = $v.isBluetoothConfig;
      _isScanQrcode = $v.isScanQrcode;
      _isAskForLocation = $v.isAskForLocation;
      _isOnlineSubscription = $v.isOnlineSubscription;
      _isPrepaidPlan = $v.isPrepaidPlan;
      _isPostpaidPlan = $v.isPostpaidPlan;
      _isVoucherPlan = $v.isVoucherPlan;
      _isAllowPlanUpdate = $v.isAllowPlanUpdate;
      _eventTypes = $v.eventTypes.toBuilder();
      _sensorsIds = $v.sensorsIds.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProductMetadata other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ProductMetadata;
  }

  @override
  void update(void Function(ProductMetadataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ProductMetadata build() => _build();

  _$ProductMetadata _build() {
    _$ProductMetadata _$result;
    try {
      _$result = _$v ??
          new _$ProductMetadata._(
              productId: BuiltValueNullFieldError.checkNotNull(
                  productId, r'ProductMetadata', 'productId'),
              vendor: vendor.build(),
              operator: BuiltValueNullFieldError.checkNotNull(
                  operator, r'ProductMetadata', 'operator'),
              names: names.build(),
              descriptions: descriptions.build(),
              imageAsset: imageAsset,
              imageUrl: imageUrl,
              instruction: instruction,
              instructionImageAsset: BuiltValueNullFieldError.checkNotNull(
                  instructionImageAsset, r'ProductMetadata', 'instructionImageAsset'),
              isBluetoothConfig: BuiltValueNullFieldError.checkNotNull(
                  isBluetoothConfig, r'ProductMetadata', 'isBluetoothConfig'),
              isScanQrcode: BuiltValueNullFieldError.checkNotNull(
                  isScanQrcode, r'ProductMetadata', 'isScanQrcode'),
              isAskForLocation: BuiltValueNullFieldError.checkNotNull(
                  isAskForLocation, r'ProductMetadata', 'isAskForLocation'),
              isOnlineSubscription: isOnlineSubscription,
              isPrepaidPlan: BuiltValueNullFieldError.checkNotNull(
                  isPrepaidPlan, r'ProductMetadata', 'isPrepaidPlan'),
              isPostpaidPlan:
                  BuiltValueNullFieldError.checkNotNull(isPostpaidPlan, r'ProductMetadata', 'isPostpaidPlan'),
              isVoucherPlan: BuiltValueNullFieldError.checkNotNull(isVoucherPlan, r'ProductMetadata', 'isVoucherPlan'),
              isAllowPlanUpdate: BuiltValueNullFieldError.checkNotNull(isAllowPlanUpdate, r'ProductMetadata', 'isAllowPlanUpdate'),
              eventTypes: eventTypes.build(),
              sensorsIds: sensorsIds.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'vendor';
        vendor.build();

        _$failedField = 'names';
        names.build();
        _$failedField = 'descriptions';
        descriptions.build();

        _$failedField = 'eventTypes';
        eventTypes.build();
        _$failedField = 'sensorsIds';
        sensorsIds.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ProductMetadata', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Vendor extends Vendor {
  @override
  final String name;
  @override
  final String? logoAsset;
  @override
  final String description;

  factory _$Vendor([void Function(VendorBuilder)? updates]) =>
      (new VendorBuilder()..update(updates))._build();

  _$Vendor._({required this.name, this.logoAsset, required this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'Vendor', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, r'Vendor', 'description');
  }

  @override
  Vendor rebuild(void Function(VendorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  VendorBuilder toBuilder() => new VendorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Vendor &&
        name == other.name &&
        logoAsset == other.logoAsset &&
        description == other.description;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, logoAsset.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class VendorBuilder implements Builder<Vendor, VendorBuilder> {
  _$Vendor? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _logoAsset;
  String? get logoAsset => _$this._logoAsset;
  set logoAsset(String? logoAsset) => _$this._logoAsset = logoAsset;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  VendorBuilder();

  VendorBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _logoAsset = $v.logoAsset;
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Vendor other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Vendor;
  }

  @override
  void update(void Function(VendorBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Vendor build() => _build();

  _$Vendor _build() {
    final _$result = _$v ??
        new _$Vendor._(
            name:
                BuiltValueNullFieldError.checkNotNull(name, r'Vendor', 'name'),
            logoAsset: logoAsset,
            description: BuiltValueNullFieldError.checkNotNull(
                description, r'Vendor', 'description'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
