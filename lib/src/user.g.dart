// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const RoleType _$user = const RoleType._('user');
const RoleType _$admin = const RoleType._('admin');
const RoleType _$organizationUser = const RoleType._('organizationUser');
const RoleType _$superAdmin = const RoleType._('superAdmin');

RoleType _$typeValueOf(String name) {
  switch (name) {
    case 'user':
      return _$user;
    case 'admin':
      return _$admin;
    case 'organizationUser':
      return _$organizationUser;
    case 'superAdmin':
      return _$superAdmin;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<RoleType> _$typeValues = new BuiltSet<RoleType>(const <RoleType>[
  _$user,
  _$admin,
  _$organizationUser,
  _$superAdmin,
]);

Serializer<User> _$userSerializer = new _$UserSerializer();
Serializer<Organization> _$organizationSerializer =
    new _$OrganizationSerializer();
Serializer<RoleType> _$roleTypeSerializer = new _$RoleTypeSerializer();

class _$UserSerializer implements StructuredSerializer<User> {
  @override
  final Iterable<Type> types = const [User, _$User];
  @override
  final String wireName = 'User';

  @override
  Iterable<Object?> serialize(Serializers serializers, User object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'user_id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'organization',
      serializers.serialize(object.organization,
          specifiedType: const FullType(Organization)),
      'role',
      serializers.serialize(object.role,
          specifiedType: const FullType(RoleType)),
    ];
    Object? value;
    value = object.email;
    if (value != null) {
      result
        ..add('email')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.country;
    if (value != null) {
      result
        ..add('country')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.lastName;
    if (value != null) {
      result
        ..add('last_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.firstName;
    if (value != null) {
      result
        ..add('first_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.language;
    if (value != null) {
      result
        ..add('lang_key')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.timestamp;
    if (value != null) {
      result
        ..add('birth_date')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.imageUrl;
    if (value != null) {
      result
        ..add('image_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.timezoneName;
    if (value != null) {
      result
        ..add('timezone_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  User deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'country':
          result.country = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'last_name':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'first_name':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'lang_key':
          result.language = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'user_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'birth_date':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'image_url':
          result.imageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'organization':
          result.organization.replace(serializers.deserialize(value,
              specifiedType: const FullType(Organization))! as Organization);
          break;
        case 'role':
          result.role = serializers.deserialize(value,
              specifiedType: const FullType(RoleType))! as RoleType;
          break;
        case 'timezone_name':
          result.timezoneName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$OrganizationSerializer implements StructuredSerializer<Organization> {
  @override
  final Iterable<Type> types = const [Organization, _$Organization];
  @override
  final String wireName = 'Organization';

  @override
  Iterable<Object?> serialize(Serializers serializers, Organization object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'organization_id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Organization deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new OrganizationBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
        case 'organization_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String))! as String;
          break;
      }
    }

    return result.build();
  }
}

class _$RoleTypeSerializer implements PrimitiveSerializer<RoleType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'user': 'role_user',
    'admin': 'role_orga_admin',
    'organizationUser': 'role_orga_user',
    'superAdmin': 'role_super_admin',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'role_user': 'user',
    'role_orga_admin': 'admin',
    'role_orga_user': 'organizationUser',
    'role_super_admin': 'superAdmin',
  };

  @override
  final Iterable<Type> types = const <Type>[RoleType];
  @override
  final String wireName = 'RoleType';

  @override
  Object serialize(Serializers serializers, RoleType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  RoleType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      RoleType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

class _$User extends User {
  @override
  final String? email;
  @override
  final String? country;
  @override
  final String? lastName;
  @override
  final String? firstName;
  @override
  final String? language;
  @override
  final String id;
  @override
  final DateTime? timestamp;
  @override
  final String? imageUrl;
  @override
  final Organization organization;
  @override
  final RoleType role;
  @override
  final String? timezoneName;

  factory _$User([void Function(UserBuilder)? updates]) =>
      (new UserBuilder()..update(updates))._build();

  _$User._(
      {this.email,
      this.country,
      this.lastName,
      this.firstName,
      this.language,
      required this.id,
      this.timestamp,
      this.imageUrl,
      required this.organization,
      required this.role,
      this.timezoneName})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'User', 'id');
    BuiltValueNullFieldError.checkNotNull(
        organization, r'User', 'organization');
    BuiltValueNullFieldError.checkNotNull(role, r'User', 'role');
  }

  @override
  User rebuild(void Function(UserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserBuilder toBuilder() => new UserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is User &&
        email == other.email &&
        country == other.country &&
        lastName == other.lastName &&
        firstName == other.firstName &&
        language == other.language &&
        id == other.id &&
        timestamp == other.timestamp &&
        imageUrl == other.imageUrl &&
        organization == other.organization &&
        role == other.role &&
        timezoneName == other.timezoneName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, email.hashCode);
    _$hash = $jc(_$hash, country.hashCode);
    _$hash = $jc(_$hash, lastName.hashCode);
    _$hash = $jc(_$hash, firstName.hashCode);
    _$hash = $jc(_$hash, language.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, timestamp.hashCode);
    _$hash = $jc(_$hash, imageUrl.hashCode);
    _$hash = $jc(_$hash, organization.hashCode);
    _$hash = $jc(_$hash, role.hashCode);
    _$hash = $jc(_$hash, timezoneName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class UserBuilder implements Builder<User, UserBuilder> {
  _$User? _$v;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _country;
  String? get country => _$this._country;
  set country(String? country) => _$this._country = country;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _language;
  String? get language => _$this._language;
  set language(String? language) => _$this._language = language;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  DateTime? _timestamp;
  DateTime? get timestamp => _$this._timestamp;
  set timestamp(DateTime? timestamp) => _$this._timestamp = timestamp;

  String? _imageUrl;
  String? get imageUrl => _$this._imageUrl;
  set imageUrl(String? imageUrl) => _$this._imageUrl = imageUrl;

  OrganizationBuilder? _organization;
  OrganizationBuilder get organization =>
      _$this._organization ??= new OrganizationBuilder();
  set organization(OrganizationBuilder? organization) =>
      _$this._organization = organization;

  RoleType? _role;
  RoleType? get role => _$this._role;
  set role(RoleType? role) => _$this._role = role;

  String? _timezoneName;
  String? get timezoneName => _$this._timezoneName;
  set timezoneName(String? timezoneName) => _$this._timezoneName = timezoneName;

  UserBuilder();

  UserBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _email = $v.email;
      _country = $v.country;
      _lastName = $v.lastName;
      _firstName = $v.firstName;
      _language = $v.language;
      _id = $v.id;
      _timestamp = $v.timestamp;
      _imageUrl = $v.imageUrl;
      _organization = $v.organization.toBuilder();
      _role = $v.role;
      _timezoneName = $v.timezoneName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(User other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$User;
  }

  @override
  void update(void Function(UserBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  User build() => _build();

  _$User _build() {
    _$User _$result;
    try {
      _$result = _$v ??
          new _$User._(
              email: email,
              country: country,
              lastName: lastName,
              firstName: firstName,
              language: language,
              id: BuiltValueNullFieldError.checkNotNull(id, r'User', 'id'),
              timestamp: timestamp,
              imageUrl: imageUrl,
              organization: organization.build(),
              role:
                  BuiltValueNullFieldError.checkNotNull(role, r'User', 'role'),
              timezoneName: timezoneName);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'organization';
        organization.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'User', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Organization extends Organization {
  @override
  final String name;
  @override
  final String id;

  factory _$Organization([void Function(OrganizationBuilder)? updates]) =>
      (new OrganizationBuilder()..update(updates))._build();

  _$Organization._({required this.name, required this.id}) : super._() {
    BuiltValueNullFieldError.checkNotNull(name, r'Organization', 'name');
    BuiltValueNullFieldError.checkNotNull(id, r'Organization', 'id');
  }

  @override
  Organization rebuild(void Function(OrganizationBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  OrganizationBuilder toBuilder() => new OrganizationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Organization && name == other.name && id == other.id;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class OrganizationBuilder
    implements Builder<Organization, OrganizationBuilder> {
  _$Organization? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  OrganizationBuilder();

  OrganizationBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _id = $v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Organization other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Organization;
  }

  @override
  void update(void Function(OrganizationBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Organization build() => _build();

  _$Organization _build() {
    final _$result = _$v ??
        new _$Organization._(
            name: BuiltValueNullFieldError.checkNotNull(
                name, r'Organization', 'name'),
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'Organization', 'id'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
