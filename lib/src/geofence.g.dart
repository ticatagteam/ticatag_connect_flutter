// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geofence.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Geofence> _$geofenceSerializer = new _$GeofenceSerializer();

class _$GeofenceSerializer implements StructuredSerializer<Geofence> {
  @override
  final Iterable<Type> types = const [Geofence, _$Geofence];
  @override
  final String wireName = 'Geofence';

  @override
  Iterable<Object?> serialize(Serializers serializers, Geofence object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'longitude',
      serializers.serialize(object.longitude,
          specifiedType: const FullType(double)),
      'latitude',
      serializers.serialize(object.latitude,
          specifiedType: const FullType(double)),
      'radius',
      serializers.serialize(object.radius, specifiedType: const FullType(int)),
      'active',
      serializers.serialize(object.isActive,
          specifiedType: const FullType(bool)),
      'on_entry',
      serializers.serialize(object.isOnEntry,
          specifiedType: const FullType(bool)),
      'on_exit',
      serializers.serialize(object.isOnExit,
          specifiedType: const FullType(bool)),
    ];
    Object? value;
    value = object.identifier;
    if (value != null) {
      result
        ..add('geofence_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.deviceId;
    if (value != null) {
      result
        ..add('device_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.formattedAddress;
    if (value != null) {
      result
        ..add('formatted_address')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Geofence deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GeofenceBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'geofence_id':
          result.identifier = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'device_id':
          result.deviceId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double))! as double;
          break;
        case 'radius':
          result.radius = serializers.deserialize(value,
              specifiedType: const FullType(int))! as int;
          break;
        case 'active':
          result.isActive = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'on_entry':
          result.isOnEntry = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'on_exit':
          result.isOnExit = serializers.deserialize(value,
              specifiedType: const FullType(bool))! as bool;
          break;
        case 'formatted_address':
          result.formattedAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$Geofence extends Geofence {
  @override
  final String? identifier;
  @override
  final String? deviceId;
  @override
  final double longitude;
  @override
  final double latitude;
  @override
  final int radius;
  @override
  final bool isActive;
  @override
  final bool isOnEntry;
  @override
  final bool isOnExit;
  @override
  final String? formattedAddress;

  factory _$Geofence([void Function(GeofenceBuilder)? updates]) =>
      (new GeofenceBuilder()..update(updates))._build();

  _$Geofence._(
      {this.identifier,
      this.deviceId,
      required this.longitude,
      required this.latitude,
      required this.radius,
      required this.isActive,
      required this.isOnEntry,
      required this.isOnExit,
      this.formattedAddress})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(longitude, r'Geofence', 'longitude');
    BuiltValueNullFieldError.checkNotNull(latitude, r'Geofence', 'latitude');
    BuiltValueNullFieldError.checkNotNull(radius, r'Geofence', 'radius');
    BuiltValueNullFieldError.checkNotNull(isActive, r'Geofence', 'isActive');
    BuiltValueNullFieldError.checkNotNull(isOnEntry, r'Geofence', 'isOnEntry');
    BuiltValueNullFieldError.checkNotNull(isOnExit, r'Geofence', 'isOnExit');
  }

  @override
  Geofence rebuild(void Function(GeofenceBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GeofenceBuilder toBuilder() => new GeofenceBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Geofence &&
        identifier == other.identifier &&
        deviceId == other.deviceId &&
        longitude == other.longitude &&
        latitude == other.latitude &&
        radius == other.radius &&
        isActive == other.isActive &&
        isOnEntry == other.isOnEntry &&
        isOnExit == other.isOnExit &&
        formattedAddress == other.formattedAddress;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, identifier.hashCode);
    _$hash = $jc(_$hash, deviceId.hashCode);
    _$hash = $jc(_$hash, longitude.hashCode);
    _$hash = $jc(_$hash, latitude.hashCode);
    _$hash = $jc(_$hash, radius.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, isOnEntry.hashCode);
    _$hash = $jc(_$hash, isOnExit.hashCode);
    _$hash = $jc(_$hash, formattedAddress.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class GeofenceBuilder implements Builder<Geofence, GeofenceBuilder> {
  _$Geofence? _$v;

  String? _identifier;
  String? get identifier => _$this._identifier;
  set identifier(String? identifier) => _$this._identifier = identifier;

  String? _deviceId;
  String? get deviceId => _$this._deviceId;
  set deviceId(String? deviceId) => _$this._deviceId = deviceId;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(double? longitude) => _$this._longitude = longitude;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(double? latitude) => _$this._latitude = latitude;

  int? _radius;
  int? get radius => _$this._radius;
  set radius(int? radius) => _$this._radius = radius;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  bool? _isOnEntry;
  bool? get isOnEntry => _$this._isOnEntry;
  set isOnEntry(bool? isOnEntry) => _$this._isOnEntry = isOnEntry;

  bool? _isOnExit;
  bool? get isOnExit => _$this._isOnExit;
  set isOnExit(bool? isOnExit) => _$this._isOnExit = isOnExit;

  String? _formattedAddress;
  String? get formattedAddress => _$this._formattedAddress;
  set formattedAddress(String? formattedAddress) =>
      _$this._formattedAddress = formattedAddress;

  GeofenceBuilder();

  GeofenceBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _identifier = $v.identifier;
      _deviceId = $v.deviceId;
      _longitude = $v.longitude;
      _latitude = $v.latitude;
      _radius = $v.radius;
      _isActive = $v.isActive;
      _isOnEntry = $v.isOnEntry;
      _isOnExit = $v.isOnExit;
      _formattedAddress = $v.formattedAddress;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Geofence other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Geofence;
  }

  @override
  void update(void Function(GeofenceBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Geofence build() => _build();

  _$Geofence _build() {
    final _$result = _$v ??
        new _$Geofence._(
            identifier: identifier,
            deviceId: deviceId,
            longitude: BuiltValueNullFieldError.checkNotNull(
                longitude, r'Geofence', 'longitude'),
            latitude: BuiltValueNullFieldError.checkNotNull(
                latitude, r'Geofence', 'latitude'),
            radius: BuiltValueNullFieldError.checkNotNull(
                radius, r'Geofence', 'radius'),
            isActive: BuiltValueNullFieldError.checkNotNull(
                isActive, r'Geofence', 'isActive'),
            isOnEntry: BuiltValueNullFieldError.checkNotNull(
                isOnEntry, r'Geofence', 'isOnEntry'),
            isOnExit: BuiltValueNullFieldError.checkNotNull(
                isOnExit, r'Geofence', 'isOnExit'),
            formattedAddress: formattedAddress);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
