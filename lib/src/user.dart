import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'user.g.dart';

//flutter packages pub run build_runner build  --delete-conflicting-outputs

abstract class User implements Built<User, UserBuilder> {
  static Serializer<User> get serializer => _$userSerializer;
  String? get email;
  String? get country;
  @BuiltValueField(wireName: 'last_name')
  String? get lastName;
  @BuiltValueField(wireName: 'first_name')
  String? get firstName;
  @BuiltValueField(wireName: 'lang_key')
  String? get language;
  @BuiltValueField(wireName: 'user_id')
  String get id;
  @BuiltValueField(wireName: 'birth_date')
  DateTime? get timestamp;
  @BuiltValueField(wireName: 'image_url')
  String? get imageUrl;
  Organization get organization;
  RoleType get role;
  @BuiltValueField(wireName: 'timezone_name')
  String? get timezoneName;

  User._();
  factory User([void Function(UserBuilder) updates]) = _$User;

  String toJson() {
    return json
        .encode(standardSerializers.serializeWith(User.serializer, this));
  }

  String toString() {
    return json
        .encode(standardSerializers.serializeWith(User.serializer, this));
  }

  static User? fromJson(String jsonString) {
    return serializers.deserializeWith(
        User.serializer, json.decode(jsonString));
  }

  static User? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(User.serializer, map);
  }
}

abstract class Organization
    implements Built<Organization, OrganizationBuilder> {
  static Serializer<Organization> get serializer => _$organizationSerializer;
  String get name;
  @BuiltValueField(wireName: 'organization_id')
  String get id;

  Organization._();
  factory Organization([void Function(OrganizationBuilder) updates]) =
      _$Organization;

  String toJson() {
    return json.encode(
        standardSerializers.serializeWith(Organization.serializer, this));
  }

  String toString() {
    return json.encode(
        standardSerializers.serializeWith(Organization.serializer, this));
  }

  static Organization? fromJson(String jsonString) {
    return serializers.deserializeWith(
        Organization.serializer, json.decode(jsonString));
  }

  static Organization? fromMap(Map<dynamic, dynamic> map) {
    return standardSerializers.deserializeWith(Organization.serializer, map);
  }
}

class RoleType extends EnumClass {
  static Serializer<RoleType> get serializer => _$roleTypeSerializer;

  @BuiltValueEnumConst(wireName: 'role_user')
  static const RoleType user = _$user;
  @BuiltValueEnumConst(wireName: 'role_orga_admin')
  static const RoleType admin = _$admin;
  @BuiltValueEnumConst(wireName: 'role_orga_user')
  static const RoleType organizationUser = _$organizationUser;
  @BuiltValueEnumConst(wireName: 'role_super_admin')
  static const RoleType superAdmin = _$superAdmin;

  const RoleType._(String name) : super(name);

  static BuiltSet<RoleType> get values => _$typeValues;
  static RoleType valueOf(String name) => _$typeValueOf(name);
}
