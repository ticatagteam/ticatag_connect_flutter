class DateUtils {

  static DateTime today() {
    final now = DateTime.now();
    final lastMidnight = now.subtract(Duration(
      hours: now.hour,
      minutes: now.minute,
      seconds: now.second,
      milliseconds: now.millisecond,
      microseconds: now.microsecond,
    ));
    return lastMidnight;
  }

  static DateTime yesterday() {
   return DateUtils.today().subtract(Duration(days: 1));
 }

 static DateTime lastWeek() {
   return DateUtils.today().subtract(Duration(days: 7));
 }

 static DateTime lastMonth() {
   return DateUtils.today().subtract(Duration(days: 30));
 }

 static DateTime lastDays(int days) {
   return DateUtils.today().subtract(Duration(days: days));
 }
}
