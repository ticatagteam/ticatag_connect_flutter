library ticatag_connect;

export 'src/beacon.dart';
export 'src/beacon_event.dart';
export 'src/geofence.dart';
export 'src/ios_peripheral_identifiers_manager.dart';
export 'src/product_metadata.dart';
export 'src/serializers.dart';
export 'src/ticatag_api.dart';
export 'src/user.dart';
